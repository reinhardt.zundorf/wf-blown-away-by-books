--
-- Table structure for table `article`
--
USE f3894401_babb;

--
-- Table structure for table `article`
--
-- DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` 
(
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `publicationDate` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` mediumtext NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp  NULL,
  `dateRemoved` timestamp  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Insert data for table `article`
--
INSERT INTO `article` VALUES (1,'2018-04-03', 'Sample Article', 'Summary for this article', 'Some content for the testing purposes of this blog', CURRENT_TIMESTAMP, NULL, NULL);
INSERT INTO `article` VALUES (2, now(), 'New Website in Development', 'The Friends of the Fish Hoek Library have opted to use local company Web Folders to create a new website for our organization.', 'New website in development', CURRENT_TIMESTAMP, NULL, NULL);

--
-- Table structure for table `view`
--
-- DROP TABLE IF EXISTS `view`;
CREATE TABLE `view`
(
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `viewTypeId` smallint(5) unsigned,
  `dateAdded` timestamp NOT NULL,
  `dateModified` timestamp  NULL,
  `dateRemoved` timestamp  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Insert data for table `view`
--
INSERT INTO `view` VALUES ('2018-04-03', 'Test', 'Summary for this article', 'Some content for the testing purposes of this blog', CURRENT_TIMESTAMP, NULL, NULL);

--
-- Insert data for table `view_type`
--
-- DROP TABLE IF EXISTS `view_type`;
CREATE TABLE `view_type`
(
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NOT NULL,
  `notes` text NOT NULL,
  `dateAdded` timestamp NOT NULL,
  `dateModified` timestamp  NULL,
  `dateRemoved` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Table structure for table `book`
--
-- DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` 
(
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `altTitle` varchar(255),
  `authorId` INT NOT NULL,
  `publisherId` INT DEFAULT NULL,
  `coverUrl` varchar(255) DEFAULT NULL,
  `text` TEXT DEFAULT NULL,
  `genreId` varchar(255) DEFAULT NULL,
  `datePublished` timestamp,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL,
  `dateRemoved` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `book` VALUES (1, 'Lolita', 'Lolita: A Novel', 1, 1, 'res/books/lolita.jpg', 'Summary for this book', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL); 
INSERT INTO `book` VALUES (2, 'Pale Fire', '', 1, 2, 'res/books/pale_fire.jpg', 'Summary for this book', 2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL, NULL); 

--
-- Table structure for table `event`
--
DROP TABLE IF EXISTS `event`;
CREATE TABLE `event`
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `title` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `author`;
CREATE TABLE `author`
(

  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL,
  `dateRemoved` timestamp NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
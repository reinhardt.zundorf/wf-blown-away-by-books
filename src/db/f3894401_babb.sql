-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 23, 2018 at 11:20 AM
-- Server version: 5.5.34-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `f3894401_babb`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `publicationDate` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` mediumtext NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `publicationDate`, `title`, `summary`, `content`, `dateAdded`, `dateModified`, `dateRemoved`) VALUES
(1, '2018-04-03', 'Sample Article', 'Summary for this article', 'Some content for the testing purposes of this blog', '2018-07-25 11:50:29', NULL, NULL),
(2, '2018-07-25', 'New Website in Development', 'The Friends of the Fish Hoek Library have opted to use local company Web Folders to create a new website for our organization.', 'New website in development', '2018-07-25 11:50:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `profilePic` varchar(255) DEFAULT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `name`, `surname`, `text`, `profilePic`, `dateAdded`, `dateModified`, `dateRemoved`) VALUES
(1, 'Andrew', 'Brown', 'Andrew Brown practises as an advocate in Cape Town, and is a reservist sergeant in the South African Police Service. He has published five novels, the most recent of which, Devil’s Harvest, came out in 2014. Coldsleep Lullaby won the 2006 Sunday Times Literary Prize and Refuge was shortlisted for the 2009 Commonwealth Writer’s Prize for the Africa region. Street Blues, an account of his experiences as a police reservist was shortlisted for the 2009 Alan Paton Award. Good Cop Bad Cop was published in 2016.', 'res/authors/andrew-brown/profile.jpg', '2018-07-25 18:27:11', '2018-07-25 18:27:11', NULL),
(2, 'Diane ', 'Awerbuck', 'Diane Awerbuck wrote Gardening at Night (2003), which was awarded the Commonwealth Best First Book Award (Africa and the Caribbean) and was shortlisted for the International Dublin IMPAC Award. She also reviews fiction for the South African Sunday Times, and writes for Mail &amp; Guardian’s thought leader. \r\n\r\nAwerbuck’s latest collection of short stories is Cabin Fever; her last novel was Home Remedies. She also writes as Frank Owen (with Alex Latimer), and their latest book is South. They are currently writing the sequel, North. Visit http://southvsnorth.com/.', 'res/authors/diane-awerbuck/profile.jpg', '2018-07-25 18:28:10', '2018-07-25 18:28:10', NULL),
(3, 'Helen', 'Zille', 'Helen Zille serves as Premier of the Western Cape Province in South Africa. She was also the Leader of the Democratic Alliance until May 2015. Zille previously served as Mayor of Cape Town. She was also the Director of Communications at the University of Cape Town.', 'res/authors/helen-zille/profile.jpg', '2018-07-25 18:21:21', NULL, NULL),
(4, 'Don', 'Pinnock', 'Don Pinnock is an associate of Southern Write, a group of top travel and natural history writers and photographers in Africa. He’ s a former editor of Getaway magazine in Cape Town, South Africa He has been an electronic engineer, lecturer in journalism and criminology, consultant to the Mandela government, a professional yachtsman, explorer, travel writer, photographer and a cable-car operator on the Rock of Gibraltar. His present passion is the impact of humans on planetary processes.', 'res/authors/don-pinnock/profile.jpg', '2018-07-25 18:22:17', NULL, NULL),
(5, 'John', 'Fredericks', 'I was born in 1945 in Kewtown, Athlone one of the many townships scattered across the Cape Flats of the Western Cape. My Father was a Dustman and my Mother worked in the abattoirs.\r\n\r\nI have this distant memory of my Dad coming home from work with comic books, magazines and novels that he had salvaged from other people’s rubbish bins. We were poor and one of my adventures then was to ride upfront with my dad on the horse cart that were used to haul the refuse.\r\n\r\nI would scavenge with the other human scavengers on the dump looking for book to read and anything with resale value. I hold a vivid recollection of horse carts, huge piles of rubbish and people enveloped in dust. The squawk of seagulls, mangy dogs and the banter of dustmen over the noise of the wagons.\r\n\r\nAt night my buddies and I would huddle around a fire brazier for warmth. We would talk about our adventures of the day which always led to me telling a story that I had read.\r\n\r\nThree of those buddies later died on the gallows and many others died violent deaths before they reached manhood. In our township there were no heroes. Our idols were old street fighters who settled their scores in bloody bare fisted brawls on the streets of our town. The dynamics were crime and gangsterism and a flickering knife became part of everyday life. We felt privileged when these old gangsters sent us to the Merchant to go buy their marijuana, it meant that we could hang around them and listen to their stories of gangs and prison life.\r\n\r\nMany of us aspired to become like them. Some succeeded to rise above these childhood fantasies, for others it became their destiny. Short lived and violent, leaving behind a legacy of heartache and shattered dreams. Our make believe world of gangsters became our rites of passage.\r\n\r\nI changed my life around and took my story telling abilities to a higher level. I persevered and honed my craft as a writer and filmmaker, running on the perimeter of the film industry for more than twenty years. I do not have much education but I do have revelation. I have worked with people from all cultures and strived to reach my goal. For many years I was told that I was a good writer but I should not give up my day job. At the age of fifty and tired of doing nothing, I gave up my day job to write full time. To date I have numerous documentaries, film scripts and published stories to my credit. I have written stage and radio plays. I also wrote the screen play of the epic local film Noem My Skollie.\r\n\r\nI am currently writing the book of Skollie and this will be my first major book. I dream on because I believe that dreams never die.\r\n“I grow in the shadows, I glow in the dark. I am the storyteller. The weaver of dreams”.', 'res/authors/john-fredericks/profile.jpg', '2018-07-25 18:25:57', NULL, NULL),
(6, 'Jolyn', 'Phillips', 'Jolyn Phillips works as a word-vagrant in Cape Town. She sometimes stays in Gansbaai where she was born and sometimes in Bellville and Cape Town with a big backpack with stuff in. Her family calls her a professional student because she has not left university after graduating so many times. They also believe she doesn’t ‘really’ work but mostly they are proud that she is now a millionaire because she published a book called Tjieng Tjang Tjerries and other stories published by Modjaji Books. If she is not scratching around for words she performs as a jazz vocalist, theater performer, educator and poet.', 'res/authors/jolyn-phillips/profile.jpg', '2018-07-25 18:33:46', NULL, NULL),
(7, 'Mark', 'Winkler', 'Mark grew up in what is now Mpumalanga, and was educated at St Alban’s College in Pretoria and Rhodes University, Grahamstown. He has spent most of his working life in the advertising industry in Cape Town, where he lives with his wife and two daughters. He is currently creative director at a leading Cape Town advertising agency.', 'res/authors/mark-winkler/profile.jpg', '2018-07-26 19:11:40', NULL, NULL),
(8, 'Marianne', 'Thamm', 'Marianne Thamm is a top-selling author, comedian and commentator-at-large. She has written several successful books, including Alison Botha’s story I Have Life. Marianne’s online following is huge, she appears live and on TV, and is known for her in-depth, off-beat journalism. She is assistant editor at The Daily Maverick and also very active on Facebook, Twitter and other social media.\r\nMarianne’s multiple awards include two as Outstanding Humorous Columnist (South African Comedy Awards); several Excellence Awards as Journalist of the Year and Best Columnist (Media 24); a Mondi Award for Best Columnist; and three South African Film and Television awards for ZA News (as head writer).\r\n\r\nShe lives in Newlands, Cape Town, with her partner of many years and their two daughters.', 'res/authors/marianne-thamm/profile.jpg', '2018-07-26 19:12:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `altTitle` varchar(255) DEFAULT NULL,
  `authorId` int(11) NOT NULL,
  `publisherId` int(11) DEFAULT NULL,
  `coverUrl` varchar(255) DEFAULT NULL,
  `text` text,
  `genreId` varchar(255) DEFAULT NULL,
  `datePublished` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateAdded` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `altTitle`, `authorId`, `publisherId`, `coverUrl`, `text`, `genreId`, `datePublished`, `dateAdded`, `dateModified`, `dateRemoved`) VALUES
(1, 'Lolita', 'Lolita: A Novel', 1, 1, 'res/books/lolita.jpg', 'Summary for this book', '1', '2018-07-25 11:50:30', '2018-07-25 11:50:30', NULL, NULL),
(2, 'Pale Fire', '', 1, 2, 'res/books/pale_fire.jpg', 'Summary for this book', '2', '2018-07-25 11:50:30', '2018-07-25 11:50:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `title` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE `view` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `viewTypeId` smallint(5) UNSIGNED DEFAULT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view`
--

INSERT INTO `view` (`id`, `title`, `content`, `viewTypeId`, `dateAdded`, `dateModified`, `dateRemoved`) VALUES
(2018, 'Test', 'Summary for this article', 0, '2018-07-25 11:50:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `view_type`
--

CREATE TABLE `view_type` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `view`
--
ALTER TABLE `view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `view_type`
--
ALTER TABLE `view_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `view`
--
ALTER TABLE `view`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2019;

--
-- AUTO_INCREMENT for table `view_type`
--
ALTER TABLE `view_type`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

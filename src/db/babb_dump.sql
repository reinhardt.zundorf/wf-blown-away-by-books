-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: babb
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `publicationDate` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` mediumtext NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'2018-04-03','Sample Article','Summary for this article','Some content for the testing purposes of this blog','2018-07-18 00:24:30',NULL,NULL),(2,'2017-07-13','Fish Hoek Valley Museum','The Fish Hoek Valley Museum','<article id=\"post-471\" class=\"content-single post-471 post type-post status-publish format-standard has-post-thumbnail hentry category-museum tag-fish-hoek-valley-museum\">\r\n	<header class=\"entry-header\">\r\n		<h1 class=\"entry-title\">The Fish Hoek Valley Museum</h1>\r\n		<div class=\"post-meta\"><!--Post Meta-->\r\n			 <!--We close PHP and open the way for HTML code -->\r\n		<ul>\r\n			<li><span class=\"posted-on\"><i class=\"space fa fa-calendar\"></i> <time class=\"entry-date published\" datetime=\"2017-07-13T20:30:02+00:00\">13th July 2017</time><time class=\"updated\" datetime=\"2017-07-13T21:07:02+00:00\">13th July 2017</time> </span></li>\r\n			<li><span class=\"theauthor\"> by <span class=\"author vcard\"><a class=\"url fn n\" href=\"http://www.blownawaybybooks.co.za/author/mahodges/\">MaryAnn Hodges</a></span></span></li>\r\n			<li><div class=\"thecategory\"><a href=\"http://www.blownawaybybooks.co.za/category/museum/\" rel=\"category tag\">Museum</a> </div></li>\r\n			<li><span class=\"comments\"><i class=\"fa fa-comments\"></i> No Comments </span></li>\r\n		</ul>\r\n		</div>\r\n	</header><!-- .entry-header -->\r\n	\r\n	<div class=\"post-image\"><!--Featured Image-->\r\n					<img width=\"505\" height=\"350\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum.jpg\" class=\"attachment-stiff_big size-stiff_big wp-post-image\" alt=\"\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum-300x208.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum-768x533.jpg 768w\" sizes=\"(max-width: 505px) 100vw, 505px\" />			</div>	\r\n\r\n	<div class=\"entry-content\">\r\n		<div id=\"pl-471\"  class=\"panel-layout\" ><div id=\"pg-471-0\"  class=\"panel-grid panel-no-style\" ><div id=\"pgc-471-0-0\"  class=\"panel-grid-cell\" ><div id=\"panel-471-0-0-0\" class=\"so-panel widget widget_media_image panel-first-child\" data-index=\"0\" ><h3 class=\"widget-title\">Fish Hoek Valley Museum</h3><img width=\"960\" height=\"666\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum.jpg\" class=\"image wp-image-473  attachment-full size-full\" alt=\"\" style=\"max-width: 100%; height: auto;\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum-300x208.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/fish-hoek-museum-768x533.jpg 768w\" sizes=\"(max-width: 960px) 100vw, 960px\" /></div><div id=\"panel-471-0-0-1\" class=\"so-panel widget widget_media_image\" data-index=\"1\" ><h3 class=\"widget-title\">Sewing Machine</h3><img width=\"960\" height=\"960\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/sewing-machine.jpg\" class=\"image wp-image-480  attachment-full size-full\" alt=\"\" style=\"max-width: 100%; height: auto;\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/sewing-machine.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/sewing-machine-150x150.jpg 150w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/sewing-machine-300x300.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/sewing-machine-768x768.jpg 768w\" sizes=\"(max-width: 960px) 100vw, 960px\" /></div><div id=\"panel-471-0-0-2\" class=\"so-panel widget widget_media_image\" data-index=\"2\" ><h3 class=\"widget-title\">Peach Peeler</h3><img width=\"960\" height=\"960\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/Peach-peeler.jpg\" class=\"image wp-image-478  attachment-full size-full\" alt=\"\" style=\"max-width: 100%; height: auto;\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/Peach-peeler.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/Peach-peeler-150x150.jpg 150w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/Peach-peeler-300x300.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/Peach-peeler-768x768.jpg 768w\" sizes=\"(max-width: 960px) 100vw, 960px\" /></div><div id=\"panel-471-0-0-3\" class=\"so-panel widget widget_media_image\" data-index=\"3\" ><h3 class=\"widget-title\">Lamp</h3><img width=\"960\" height=\"960\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/lamp2.jpg\" class=\"image wp-image-476  attachment-full size-full\" alt=\"\" style=\"max-width: 100%; height: auto;\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/lamp2.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/lamp2-150x150.jpg 150w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/lamp2-300x300.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/lamp2-768x768.jpg 768w\" sizes=\"(max-width: 960px) 100vw, 960px\" /></div><div id=\"panel-471-0-0-4\" class=\"so-panel widget widget_media_image\" data-index=\"4\" ><h3 class=\"widget-title\">Foot Warmer</h3><img width=\"960\" height=\"720\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/foot-warmer.jpg\" class=\"image wp-image-475  attachment-full size-full\" alt=\"\" style=\"max-width: 100%; height: auto;\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/foot-warmer.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/foot-warmer-300x225.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/foot-warmer-768x576.jpg 768w\" sizes=\"(max-width: 960px) 100vw, 960px\" /></div><div id=\"panel-471-0-0-5\" class=\"so-panel widget widget_media_image panel-last-child\" data-index=\"5\" ><h3 class=\"widget-title\">Icecream Maker</h3><img width=\"960\" height=\"960\" src=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/icecream-maker.jpg\" class=\"image wp-image-474  attachment-full size-full\" alt=\"\" style=\"max-width: 100%; height: auto;\" srcset=\"http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/icecream-maker.jpg 960w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/icecream-maker-150x150.jpg 150w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/icecream-maker-300x300.jpg 300w, http://www.blownawaybybooks.co.za/wp-content/uploads/2017/07/icecream-maker-768x768.jpg 768w\" sizes=\"(max-width: 960px) 100vw, 960px\" /></div></div></div></div><div class=\'apss-social-share apss-theme-1 clearfix\' >\r\n\r\n\r\n				<div class=\'apss-facebook apss-single-icon\'>\r\n					<a rel=\'nofollow\'  title=\"Share on Facebook\" target=\'_blank\' href=\'https://www.facebook.com/sharer/sharer.php?u=http://www.blownawaybybooks.co.za/2017/07/13/the-fish-hoek-valley-museum/\'>\r\n						<div class=\'apss-icon-block clearfix\'>\r\n							<i class=\'fa fa-facebook\'></i>\r\n							<span class=\'apss-social-text\'>Share on Facebook</span>\r\n							<span class=\'apss-share\'>Share</span>\r\n						</div>\r\n											</a>\r\n				</div>\r\n								<div class=\'apss-twitter apss-single-icon\'>\r\n					<a rel=\'nofollow\'  href=\"https://twitter.com/intent/tweet?text=The%20Fish%20Hoek%20Valley%20Museum&amp;url=http%3A%2F%2Fwww.blownawaybybooks.co.za%2F2017%2F07%2F13%2Fthe-fish-hoek-valley-museum%2F&amp;\"  title=\"Share on Twitter\" target=\'_blank\'>\r\n						<div class=\'apss-icon-block clearfix\'>\r\n							<i class=\'fa fa-twitter\'></i>\r\n							<span class=\'apss-social-text\'>Share on Twitter</span><span class=\'apss-share\'>Tweet</span>\r\n						</div>\r\n											</a>\r\n				</div>\r\n								<div class=\'apss-google-plus apss-single-icon\'>\r\n					<a rel=\'nofollow\'  title=\"Share on Google Plus\" target=\'_blank\' href=\'https://plus.google.com/share?url=http://www.blownawaybybooks.co.za/2017/07/13/the-fish-hoek-valley-museum/\'>\r\n						<div class=\'apss-icon-block clearfix\'>\r\n							<i class=\'fa fa-google-plus\'></i>\r\n							<span class=\'apss-social-text\'>Share on Google Plus</span>\r\n							<span class=\'apss-share\'>Share</span>\r\n						</div>\r\n											</a>\r\n				</div>\r\n				\r\n				<div class=\'apss-pinterest apss-single-icon\'>\r\n					<a rel=\'nofollow\' title=\"Share on Pinterest\" href=\'javascript:pinIt();\'>\r\n						<div class=\'apss-icon-block clearfix\'>\r\n							<i class=\'fa fa-pinterest\'></i>\r\n							<span class=\'apss-social-text\'>Share on Pinterest</span>\r\n							<span class=\'apss-share\'>Share</span>\r\n						</div>\r\n						\r\n					</a>\r\n				</div>\r\n				\r\n				<div class=\'apss-linkedin apss-single-icon\'>\r\n					<a rel=\'nofollow\'  title=\"Share on LinkedIn\" target=\'_blank\' href=\'http://www.linkedin.com/shareArticle?mini=true&amp;title=The%20Fish%20Hoek%20Valley%20Museum&amp;url=http://www.blownawaybybooks.co.za/2017/07/13/the-fish-hoek-valley-museum/&amp;summary=Fish+Hoek+Valley+MuseumSewing+MachinePeach+PeelerLampFoot+WarmerIcecream+Maker\'>\r\n						<div class=\'apss-icon-block clearfix\'><i class=\'fa fa-linkedin\'></i>\r\n							<span class=\'apss-social-text\'>Share on LinkedIn</span>\r\n							<span class=\'apss-share\'>Share</span>\r\n						</div>\r\n											</a>\r\n				</div>\r\n								<div class=\'apss-digg apss-single-icon\'>\r\n					<a rel=\'nofollow\'  title=\"Share on Digg\" target=\'_blank\' href=\'http://digg.com/submit?phase=2%20&amp;url=http://www.blownawaybybooks.co.za/2017/07/13/the-fish-hoek-valley-museum/&amp;title=The%20Fish%20Hoek%20Valley%20Museum\'>\r\n						<div class=\'apss-icon-block clearfix\'>\r\n							<i class=\'fa fa-digg\'></i>\r\n							<span class=\'apss-social-text\'>Share on Digg</span>\r\n							<span class=\'apss-share\'>Share</span>\r\n						</div>\r\n					</a>\r\n				</div>\r\n\r\n				</div>	</div><!-- .entry-content -->\r\n	<div class=\"links-pages\">\r\n			</div><!-- .entry-content -->\r\n\r\n	<div class=\"post-tags\">\r\n		<span class=\"tags-links\"><i class=\"fa fa-tags\"></i> <a href=\"http://www.blownawaybybooks.co.za/tag/fish-hoek-valley-museum/\" rel=\"tag\">Fish Hoek Valley Museum</a> </span>	</div>\r\n	\r\n	<nav class=\"navigation post-navigation\" role=\"navigation\">\r\n		 <!--We close PHP and open the way for HTML code -->\r\n	<div class=\"next_prev_post\">\r\n		<div class=\"nav-next\"><a href=\"http://www.blownawaybybooks.co.za/2017/07/13/games-day/\" rel=\"next\">Next Post <i class=\"fa fa-chevron-right\"></i></a></div>	</div><!-- .next_prev_post -->\r\n	</nav><!-- .navigation -->\r\n</article><!-- #post-## -->','2018-07-18 09:57:59','2018-07-18 09:57:59',NULL),(3,'2018-07-19','New Website in Development','The Friends of the Fish Hoek Library have opted to use local company Web Folders to create a new website for our organization.','New website in development','2018-07-19 11:50:56',NULL,NULL);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `altTitle` varchar(255) DEFAULT NULL,
  `authorId` int(11) NOT NULL,
  `publisherId` int(11) DEFAULT NULL,
  `coverUrl` varchar(255) DEFAULT NULL,
  `text` text,
  `genreId` varchar(255) DEFAULT NULL,
  `datePublished` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Lolita','Lolita: A Novel',1,1,'res/books/lolita.jpg','Summary for this book','1','2018-07-18 00:24:31','2018-07-18 00:24:31',NULL,NULL),(2,'Pale Fire','',1,2,'res/books/pale_fire.jpg','Summary for this book','2','2018-07-18 00:24:31','2018-07-18 00:24:31',NULL,NULL);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `title` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'2018-07-10 00:26:39','2018-07-10 00:26:39','Test Event #1');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view`
--

DROP TABLE IF EXISTS `view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `viewTypeId` smallint(5) unsigned DEFAULT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view`
--

LOCK TABLES `view` WRITE;
/*!40000 ALTER TABLE `view` DISABLE KEYS */;
/*!40000 ALTER TABLE `view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_type`
--

DROP TABLE IF EXISTS `view_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_type` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateModified` timestamp NULL DEFAULT NULL,
  `dateRemoved` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_type`
--

LOCK TABLES `view_type` WRITE;
/*!40000 ALTER TABLE `view_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `view_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-21 13:37:39

/* 
 * The MIT License
 *
 * Copyright 2018 Web Folders (Pty) Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


$.notify(
{
    icon:           'http://www.webfolders.co.za/favicon.ico',
    message:        'FBT: Landing Page (ALPHA)'
},
{
    type:           'minimalist',
    delay:          5000,
    icon_type:      'image',
    template:       '<div data-notify="container" class="col-xs-7 col-sm-3 alert alert-{0}" role="alert">' +
                    '   <img data-notify="icon" class="img-circle pull-left">' +
                    '   <span data-notify="title">{1}</span>' +
                    '   <span data-notify="message">{2}</span>' +
                    '   <span data-notify="close">{3}</span>' +
                    '</div>'
});

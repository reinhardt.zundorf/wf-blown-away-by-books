<!--<div id="calendar"></div>-->

<!--<script src="js/moment.min.js"></script>-->
<!--<script src="js/fullcalendar-min.js"></script>-->
<!--<script src="js/fullcalendar-min.js"></script>-->
<!--<script src="js/fullcalendar-print.min.js"></script>-->

<!--<script async src="//code.jquery.com/jquery.js"></script>-->
<!-- <script async src="js/jquery.js"></script> -->
<!-- <script async src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script> -->
<!-- <script async src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.0/fullcalendar.min.js"></script>  -->

<script>
	$(document).ready(function() 
	{

        $('#calendar').fullCalendar(
        {
          header: 
          {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
          },

          defaultDate: '2018-03-12',
          weekNumbers: true,
          navLinks: true, 
          editable: false,
          eventLimit: true, 
          
          events: 
          [
            {
              title: 'All Day Event',
              start: '2018-03-01'
            },
            {
              title: 'Long Event',
              start: '2018-03-07',
              end: '2018-03-10'
            },
            {
              id: 999,
              title: 'Repeating Event',
              start: '2018-03-09T16:00:00'
            },
            {
              id: 999,
              title: 'Repeating Event',
              start: '2018-03-16T16:00:00'
            },
            {
              title: 'Conference',
              start: '2018-03-11',
              end: '2018-03-13'
            },
            {
              title: 'Meeting',
              start: '2018-03-12T10:30:00',
              end: '2018-03-12T12:30:00'
            },
            {
              title: 'Lunch',
              start: '2018-03-12T12:00:00'
            },
            {
              title: 'Meeting',
              start: '2018-03-12T14:30:00'
            },
            {
              title: 'Happy Hour',
              start: '2018-03-12T17:30:00'
            },
            {
              title: 'Dinner',
              start: '2018-03-12T20:00:00'
            },
            {
              title: 'Birthday Party',
              start: '2018-03-13T07:00:00'
            },
            {
              title: 'Click for Google',
              url: 'http://google.com/',
              start: '2018-03-28'
            }
          ]
        });
	  });
</script>

<div class="container-fluid" style="background-color: rgba(255, 255, 255, 0.7);" id="calendar"></div>




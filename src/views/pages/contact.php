<!-- LATEST BOOKS -->
<article class="section">
    <header class="section-header text-center">
        <h1 class="h1">Contact Us</h1>
        <hr>
    </header>
</article>

<!-- FORM -->
<article class="container-default text-center contact-form">
    <div class="container" class="text-center">
        <header class="section-header text-center">
            <h2 class="h2">Send Us A Message</h2>
            <p class="p">Complete the contact form below to send us a message - we would like to hear from you. </p>
        </header>
        <section>
            <?php include "./views/forms/contact_form.php"; ?>
        </section>
    </div>
</article>

<!-- MAP -->
<article class="contact-map" style="padding:0px;">
    <div id="map" style="width:100%; height:450px"></div>
</article>

<script>
    function myMap()
    {
        var myCenter = new google.maps.LatLng(-34.136708, 18.426808);
        var mapCanvas = document.getElementById("map");
        var mapOptions = {center: myCenter, zoom: 18};
        var map = new google.maps.Map(mapCanvas, mapOptions);
        var marker = new google.maps.Marker({position: myCenter});

        marker.setMap(map);

        var infowindow = new google.maps.InfoWindow({content: "We are based at the Fish Hoek Public Library\n Fish Hoek Civic Centre\n Recreation Road\n Fish Hoek\n 7975"});

        infowindow.open(map, marker);
    }
</script>



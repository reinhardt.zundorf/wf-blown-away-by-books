<!-- MAIN CONTENT -->
<div class="container">
    <div class="row">
        <div class="col-lg-9">            

            <!--<br>-->
            <div class="container">
                <div class="row">

                    <!-- FESTIVAL PROGRAMME -->
                    <div class="col-lg-6 col-md-6 col-sm-10">
                        <article class="tile">
                            <a href="index.php?action=festival&type=programme">
                                <div class="tile-inner">
                                    <img src="res/home/carousel-2.jpg" width="400" height="275" alt="Festival Programme 2019"/>
                                    <div class="text-block">
                                        <h2 class="h2">Programme 2019</h2>
                                    </div>
                                </div>
                            </a>
                        </article>
                    </div>

                    <!-- FESTIVAL: FEATURED AUTHORS -->
                    <div class="col-lg-6 col-md-6 col-sm-10">
                        <article class="tile">
                            <a href="index.php?action=festival&type=info">
                                <div class="tile-inner">
                                    <img src="res/home/carousel-3.jpg" width="400" height="275" alt="Festival Authors 2019"/>
                                    <div class="text-block">
                                        <h2 class="h2">Guest Authors 2019</h2>
                                    </div>
                                </div>
                            </a>
                        </article>
                    </div>

                </div>

                <div class="row">

                    <!-- FESTIVAL: PROGRAMME -->
                    <div class="col-lg-6 col-md-6 col-sm-10">
                        <article class="tile">
                            <a href="index.php?action=events">
                                <div class="tile-inner">
                                    <img src="res/home/library-outside-2.jpg" width="400" height="275" alt="Programme"/>
                                    <div class="text-block">
                                        <h2 class="h2">Library Events</h2>
                                    </div>
                                </div>
                            </a>
                        </article>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-10">
                        <article class="tile">
                            <a href="index.php?action=festival&type=info">
                                <div class="tile-inner">
                                    <img src="res/home/carousel-3.jpg" width="400" height="275" alt="Programme"/>
                                    <div class="text-block">
                                        <h2 class="h2">Past Festivals</h2>
                                    </div>
                                </div>
                            </a>
                        </article>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-10">
                    <article class="home-newsletter">
                        <div class="home-newsletter-inner">
                            <!--<div class="thumbnail center well well-sm text-center">-->
                            <h2>JOIN OUR NEWSLETTER</h2>
                            <p>Get the latest updates about the festival and more literature related news.</p>
                            <form action="" method="post" role="form">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input class="form-control" type="text" id="newsletter-email" name="Email" placeholder="your@email.com">
                                </div>
                                <input type="submit" value="Subscribe" class="btn btn-large btn-primary" />
                            </form>
                        </div>
                    </article>
                </div>
            </div>

            <!--            <div class="home-posts">
                            <div class="row">
            
                                <div class="card post" style="width:400px">
                                    <img class="card-img-top" src="img_avatar1.png" alt="Post image">
                                    <div class="card-body">
                                        <h4 class="card-title">Blog Post</h4>
                                        <p class="card-text">Some example text.</p>
                                        <a href="#" class="btn btn-primary">Read more</a>
                                    </div>
                                </div>
                                <div class="card post" style="width:400px">
                                    <img class="card-img-top" src="img_avatar1.png" alt="Post image">
                                    <div class="card-body">
                                        <h4 class="card-title">Blog Post</h4>
                                        <p class="card-text">Some example text.</p>
                                        <a href="#" class="btn btn-primary">Read more</a>
                                    </div>
                                </div>
                                <div class="card post" style="width:400px">
                                    <img class="card-img-top" src="img_avatar1.png" alt="Post image">
                                    <div class="card-body">
                                        <h4 class="card-title">Blog Post</h4>
                                        <p class="card-text">Some example text.</p>
                                        <a href="#" class="btn btn-primary">Read more</a>
                                    </div>
                                </div>
            
                            </div>-->
            <!--</div>-->


            <!-- FESTIVAL -->
            <!--            <header class="section-header home-library">
                            <h1 class="h1">Fish Hoek Library</h1>
                            <hr style="padding-left: 0px; margin-left:0px; width:25%;">
                        </header>
            
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-3">
                                    
                                </div>
                                <div class="col-lg-5 offset-3">
                                    <div class="row">
                                        <article class="col tile" style="background-image: url('res/film/breathless-1.jpg');">
                                            <section class="tile-inner">
                                                <h1 class="h1">Film Night</h1>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="col tile" style="background-image: url('res/home/fish-hoek-library.jpg');">
                                            <section class="tile-inner">
                                                <h1 class="h1">All events</h1>
                                            </section>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>-->
        </div>

        <!--    <div class="row">
               <article class="col tile" style="background-image: url('res/home/fish-hoek-library.jpg');">
                   <section class="tile-inner">
                       <h1 class="h1">Book Readings</h1>
                   </section>
               </article>
           </div> -->

        <?php include "./views/templates/sidebar.php"; ?>

    </div>
</div>
<!--</div>-->


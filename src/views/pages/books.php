<!-- LATEST BOOKS -->
<article class="section">
    <header class="section-header text-center">
        <h1 class="h1">Latest Books</h1>
        <hr>
    </header>
</article>

<article class="section" style="background-color:#ededed;">
    <div class="card-group" style="background-color:#ededed;">
        <div class="row">
            <?php

            $count = 0;

            $books = [0 => "res/books/book-lolita.jpg",
                	  1 => "res/books/book-gardens.jpg",
                	  2 => "res/books/book-michele.jpg",
                	  3 => "res/books/book-darrell.jpg",
                	  4 => "res/books/book-paige.jpg",
                	  5 => "res/books/book-pens.jpg"];

            /* ============================================ *
             * Output sample books                          *
             * -------------------------------------------- */
            while($count != 6)
            {
                $path = $books[$count];

                echo "<div class='card'>";
                echo "  <div class='card-body'>";
                echo "      <img class='card-img-top' src='{$path}'/>";
                echo "  </div>";
                echo "  <div class='card-footer'>";
                echo "      <h4 class='h4'>Lolita</h4>";
                echo "      <p>Vladimir Nabokov</p>";
                echo "  </div>";
                echo "</div>";

                if($count === 7)
                {
                    echo "</div>";
                    echo "<div class='row'>";
                }

                $count++;
            }
            ?>
        </div>
    </div>
</article>
<!-- LATEST BOOKS -->
<article class="section">
    <header class="section-header text-center">
        <h1 class="h1">Blog</h1>
        <hr>
    </header>
</article>
    
<!-- BLOG -->
<article class="">
    <div class="container">

        <div class="row">
            <div class="col-lg-10">

                    <?php 

                    $articles = $view["articles"];
                    $feature  = reset($articles["results"]);

                    echo "<div class='jumbotron p-3 p-md-5 text-white rounded' > \n
                            <div class='col-md-6 px-0'> \n
                                <h1 class='display-4 font-italic'>{$feature->title}</h1> \n
                                <p class='lead my-3'>{$feature->summary}</p> \n
                                <p class='lead mb-0'><a href='index.php?action=blog&id={$feature->id}' class='text-white font-weight-bold'>Continue reading...</a></p> \n
                            </div> \n
                            <div class='col-md-3 offset-3' style='background-color:#555555;'> \n
                            </div> \n
                          </div> \n";

                    ?>
                    
                    <!-- BLOG POSTS -->
                    <div class="row mb-2">

                        <?php
                        /* ================================================= *
                         * Get posts from view structure                     *
                         * ------------------------------------------------- */
                        $posts   = $view["articles"];
                        $counter = 1;
                        $size    = sizeof($posts["results"]);

                        /* =================================================== *
                         * Iterate through $posts array and output each post   *
                         * --------------------------------------------------- */
                        foreach ($posts["results"] as $post)
                        {

                            echo "<div class='col-md-6'> \n
                                    <div class='card flex-lg-row mb-4 box-shadow h-md-250'> \n
                                        <div class='card-body d-flex flex-column align-items-start'> \n
                                            <strong class='d-inline-block mb-2 text-primary'>General News</strong> \n
                                            <h3 class='mb-0'><a class='text-dark' href='#'>{$post->title}</a></h3> \n
                                            <div class='mb-1 text-muted'>{$post->publicationDate}<br></div> \n
                                            <p class='card-text mb-auto'>{$post->summary}</p> \n
                                            <a href='index.php?action=blog&id={$post->id}'>Continue reading</a> \n
                                        </div> \n
                                        <img class='card-img-right flex-auto d-none d-md-block' data-src='holder.js/200x250?theme=thumb' alt='{$post->title}' style='width: 200px; height: 185px;' src='res/blog/{$post->id}.jpg' data-holder-rendered='true'/> \n
                                    </div> \n
                                  </div> \n";


                            if((($counter %2) === 0) && $size >= 2)
                            {
                                echo "</div>\n";
                                echo "<div class='row mb-2'>\n";
                            }

                            $counter++;
                        }

                        ?>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="">
                        <h2>About</h2>
                        <p>
                            <?php ?>
                        </p>
                    </div>
                    <div class="">
                        <h2>Archives</h2>
                        <ol class="list-unstyled">
                            <li><a href="#">March 2018</a></li>
                            <li><a href="#">February 2018</a></li>
                            <li><a href="#">January 2018</a></li>
                            <li><a href="#">December 2017</a></li>
                        </ol>
                    </div>
                    <div class="sidebar-module">
                        <h2>Elsewhere</h2>
                        <div class="row">
                            <div class="col-lg-1">
                                <ol class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-official"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ol>
                            </div>
                            <div class="col-lg-8">
                                <ol class="list-unstyled">
                                    <li><a href="http://www.facebook.com/FalseBayTours">Facebook</a></li>
                                    <li><a href="#">Twitter</a></li>
                                    <li><a href="#">Google</a></li>
                                    <li><a href="#">Instagram</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->

        <br><br>
    </section>
</article>


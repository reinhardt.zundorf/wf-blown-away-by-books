<!-- PAGE TITLE -->
<article class="section">
    <header class="section-header text-center">
        <h1 class="h1"><?php echo ucwords($view["heading"]); ?></h1>
        <hr>
    </header>
</article>

<!-- MISSION -->
<article class="section" style="background-color: #ededed;;">
    <div class="container text-center">
        <p class="p">
            <blockquote>
                We are <i>The Friends Of The Fish Hoek Library</i> and we are responsible 
                for hosting and coordinating the yearly events that make up the Blown Away By Books 
                festival.
            </blockquote>
        </p>
    </div>
</article>

<!-- PAGE TITLE -->
<article class="section">
    <header class="section-header text-center">
        <h1 class="h1">What we do..</h1>
        <hr>
    </header>
</article>

<!-- MISSION -->
<article class="section">
    <div class="container text-center">
        <div class="card-columns border-0">

          <div class="card border-0">
            <img class="img-fluid" src="res/about/festival.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Book Festival</h5>
              <p class="card-text">Quis occaecat dolor labore deserunt nulla esse est veniam incididunt reprehenderit.</p>
              <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
          </div>

          <div class="card border-0">
            <img class="" src="res/film/breathless-cover.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Arthouse Film Club</h5>
              <p class="card-text">The group meets the first and third Friday of every month to view a screening of classic film. Admission is free to all library members.</p>
              <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
          </div>

          <div class="card border-0">
            <img class="rounded-circle" src="res/home/carousel-2.jpg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
              <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
            </div>
          </div>
          
        </div>
    </div>
</article>


<!-- Who we are -->
<article class="section">
    <header class="section-header text-center">
        <h1 class="h1">Who We Are</h1>
        <hr>
    </header>
</article>

<!-- MISSION -->
<article class="section" style="background-color: rgba(0, 0, 0, 0);">
    <div class="container text-center">
        <p class="p">
            Velit cillum dolor do esse sunt enim dolor aliquip aliqua dolore officia proident velit duis commodo officia ea in in cupidatat ea aliquip tempor eu enim excepteur irure aute veniam incididunt dolore consectetur eu ad labore do ut id minim magna minim fugiat fugiat id officia ad nulla dolore nisi dolore ex sit ut nisi pariatur dolor consequat ut et id anim culpa et quis eiusmod dolore esse do in eiusmod laboris fugiat qui do mollit sunt et consequat dolor qui reprehenderit sed deserunt ad qui sed et dolor velit sit enim nostrud est eiusmod dolore in aliqua eiusmod reprehenderit do magna nisi do proident magna ea do officia adipisicing in excepteur do labore amet dolor occaecat irure officia cupidatat ut id dolor non excepteur sint enim tempor nulla laborum consectetur cillum ex elit labore incididunt et amet voluptate in culpa velit quis est ullamco ullamco exercitation laboris cillum magna nisi in eiusmod qui veniam anim reprehenderit sunt consequat nostrud sunt mollit dolore exercitation commodo.
        </p>
    </div>
</article>

<!--<section class="container-fluid" id="bg-white">
    <div class="page-content">
        <p>
            Planning your ideal getaway or first time visit to South Africa starts by browsing our catalogue of Leisure Activities and Adventure Trips scattered about the <a href="index.php?action=map">False Bay</a> region of 
            the Southern Peninsula of the Western Cape of South Africa.
            As part of our A variety of Leisure Activities and Adventure Trips ranging from Hiking, Snorkel Diving and Wine Tasting, to Helicopter Tours and a visit to Cape Point as a few examples, are available to be selected as components to create an unforgettable touring experience. 
        </p>
    </div>
    <section class="container-fluid" id="bg-white">
        <div class="page-content text-justify">
            <p>For more information and updates on Leisure Activities and Adventure Trips available for selection, visitors to this website can refer to our online store and catalogue for details. </p>
            <p>We are passionate about the beauty and diversity of our heritage on the southern peninsula and are excited to be part of promoting and showcasing this jewel at the Southern Point of our Africa.</p>
            <p>With Service Excellence setting the tone of our operation at all times, we propose to partner with our visitors and assist them with the planning and administration of their upcoming Western Cape holiday trip, leaving them relaxed and free to totally savour the wholesomeness of all the elements offered False Bay and the greater Cape Town area.</p>
            <p>Some service features such as an Integrated Reservation System, including Accommodation and Dining Out services additionally, propose to care for the convenience and peace of mind of visitors exploring False Bay.</p>
            <p>Tourism suppliers subscribed to our website will be listed, rated and recommended. This provides guests with a network frame of associated, established and credible professionals who are communally geared for the care and ultimate comfort of our visitors.</p>
            <p>By following our blog a first-hand account of news from our communities, upcoming events, reviews from other travellers and promotional deals on offer by our subscribed suppliers, is available. This will complete the picture of what to expect as a potential visitor, and what to look out for when reaching your holiday destination.</p>
        </div>
    </section>-->

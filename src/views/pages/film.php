<div class="container">
    <div class="row">
        <div class="col-lg-9">
        	<!-- <div class="row"> -->

                <!-- BOOKS: Latest  -->
				<article class="section">
	                <header class="section-header text-center">
	                    <h1 class="h1">Latest Books</h1>
                    	<hr>
                	</header>
            	</article>

    		<!-- </div> -->
        	
        	<!-- <div class="row"> -->

        		<div class="section">

        		<!-- FILM -->
    			<article class="tile" style="background-image: url('res/film/breathless-1.jpg');">
    				<section class="tile-inner">
    					<h1 class="h1">Film Night</h1>
    				</section>
    			</article>

    			<!-- BOOKS -->
    			<article class="col-lg-6 tile" style="background-image: url('res/film/breathless-2.jpg');">
					<section class="tile-inner">
    					<h1 class="h1">Featured Books</h1>
    				</section>
				</article>
        		<!-- </div> -->
        	</div>
    	</div> 

        
        <!-- SIDEBAR -->
        <?php include "./views/templates/sidebar_default.php"; ?>

	</div>
</div>
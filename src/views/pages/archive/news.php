<!--<style>
    .fancyTab {
        text-align: center;
        padding:15px 0;
        background-color: #eee;
        box-shadow: 0 0 0 1px #ddd;
        top:15px;	
        transition: top .2s;
    }

    .fancyTab.active {
        top:0;
        transition:top .2s;
    }

    .whiteBlock {
        display:none;
    }

    .fancyTab.active .whiteBlock {
        display:block;
        height:2px;
        bottom:-2px;
        background-color:#fff;
        width:99%;
        position:absolute;
        z-index:1;
    }

    .fancyTab a {
        font-family: 'Source Sans Pro';
        font-size:1.25em;
        font-weight:300;
        transition:.2s;
        color:#333;
        padding:20px;
    }

    /*.fancyTab .hidden-xs {
      white-space:nowrap;
    }*/

    .fancyTabs {
        border-bottom:2px solid #ddd;
        margin: 15px 0 0;
    }

    li.fancyTab a {
        padding-top: 15px;
        top:-15px;
        padding-bottom:0;
    }

    li.fancyTab.active a {
        padding-top: inherit;
    }

    .fancyTab .fa {
        font-size: 40px;
        width:100%;
        padding: 15px 0 5px;
        color:#666;
    }

    .fancyTab.active .fa {
        color: #cfb87c;
    }

    .fancyTab a:focus {
        outline:none;
    }

    .fancyTabContent {
        border-color: transparent;
        box-shadow: 0 -2px 0 -1px #fff, 0 0 0 1px #ddd;
        padding: 30px 15px 15px;
        position:relative;
        background-color:#fff;
    }

    .nav-tabs > li.fancyTab.active > a, 
    .nav-tabs > li.fancyTab.active > a:focus,
    .nav-tabs > li.fancyTab.active > a:hover {
        border-width:0;
    }

    .nav-tabs > li.fancyTab:hover {
        background-color:#f9f9f9;
        box-shadow: 0 0 0 1px #ddd;
    }

    .nav-tabs > li.fancyTab.active:hover {
        background-color:#fff;
        box-shadow: 1px 1px 0 1px #fff, 0 0px 0 1px #ddd, -1px 1px 0 0px #ddd inset;
    }

    .nav-tabs > li.fancyTab:hover a {
        border-color:transparent;
    }

    .nav.nav-tabs .fancyTab a[data-toggle="tab"] {
        background-color:transparent;
        border-bottom:0;
    }

    .nav-tabs > li.fancyTab:hover a {
        border-right: 1px solid transparent;
    }

    .nav-tabs > li.fancyTab > a {
        margin-right:0;
        border-top:0;
        padding-bottom: 30px;
        margin-bottom: -30px;
    }

    .nav-tabs > li.fancyTab {
        margin-right:0;
        margin-bottom:0;
    }

    .nav-tabs > li.fancyTab:last-child a {
        border-right: 1px solid transparent;
    }

    .nav-tabs > li.fancyTab.active:last-child {
        border-right: 0px solid #ddd;
        box-shadow: 0px 2px 0 0px #fff, 0px 0px 0 1px #ddd;
    }

    .fancyTab:last-child {
        box-shadow: 0 0 0 1px #ddd;
    }

    .tabs .nav-tabs li.fancyTab.active a {
        box-shadow:none;
        top:0;
    }


    .fancyTab.active {
        background: #fff;
        box-shadow: 1px 1px 0 1px #fff, 0 0px 0 1px #ddd, -1px 1px 0 0px #ddd inset;
        padding-bottom:30px;
    }

    .arrow-down {
        display:none;
        width: 0;
        height: 0;
        border-left: 20px solid transparent;
        border-right: 20px solid transparent;
        border-top: 22px solid #ddd;
        position: absolute;
        top: -1px;
        left: calc(50% - 20px);
    }

    .arrow-down-inner {
        width: 0;
        height: 0;
        border-left: 18px solid transparent;
        border-right: 18px solid transparent;
        border-top: 12px solid #fff;
        position: absolute;
        top: -22px;
        left: -18px;
    }

    .fancyTab.active .arrow-down {
        display: block;
    }

    @media (max-width: 1200px) {

        .fancyTab .fa {
            font-size: 36px;
        }

        .fancyTab .hidden-xs {
            font-size:22px;
        }

    }


    @media (max-width: 992px) {

        .fancyTab .fa {
            font-size: 33px;
        }

        .fancyTab .hidden-xs {
            font-size:18px;
            font-weight:normal;
        }

    }


    @media (max-width: 768px) {

        .fancyTab > a {
            font-size:18px;
        }

        .nav > li.fancyTab > a {
            padding:15px 0;
            margin-bottom:inherit;
        }

        .fancyTab .fa {
            font-size:30px;
        }

        .nav-tabs > li.fancyTab > a {
            border-right:1px solid transparent;
            padding-bottom:0;
        }

        .fancyTab.active .fa {
            color: #333;
        }

    }
</style>-->

<!-- CSS -->
<link href="css/sections/facebook-1.0.css" rel="stylesheet">

<!-- SECTION: About Header -->
<section class="page-header">
    <div class="container text-center">
        <h1>News</h1>
        <!--<hr class="badge">-->
    </div>
</section>

<!-- NEWS: Wrapper -->
<div class="news-wrapper">

<!--    <section class="page-content container">

        <div class="text-center">

            <div class="row text-center">
                 Swimlab - Competitive 
                <div class="col-lg-4">
                    <img class="news-badge" src="res/brand/academy/brand-badge-black.png" alt="Swimlab"/>
                    <h4>Competitive 2017</h4>
                    <hr>
                    <ul class="list-unstyled">
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">December</a></li>
                        <li class="list-group-item-text"><a href="#competitive-nov-2017">November</a></li>
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">October</a></li>
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">Older (archived)</a></li>
                    </ul>
                </div>

                 Swimlab - Competitive 
                <div class="col-lg-4">
                    <img class="news-badge" src="res/brand/academy/brand-badge-black.png" alt="Swimlab"/>
                    <h4>Aquatic Academy 2017</h4><hr>
                    <ul class="list-unstyled">
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">December</a></li>
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">November</a></li>
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">October</a></li>
                        <li class="list-group-item-text"><a href="#competitive-dec-2017">Older (archived)</a></li>
                    </ul>
                    </ul>
                </div>

                 National Swimming 
                <div class="col-lg-4">
                    <img class="news-badge" src="res/brand/academy/brand-badge-black.png" alt="Swimlab"/>
                    <h4>International Swimming</h4><hr>
                    <ul class="list-unstyled">
                        <li class="list-group-item-text"><a target="_blank" href="http://swimsa.org/">Swimming South Africa</a></li>
                        <li class="list-group-item-text"><a target="_blank" href="http://ctmaquatics.co.za/">Cape Town Metro Aquatics</a></li>
                        <li class="list-group-item-text"><a target="_blank" href="http://www.fina.org/news">FINA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>-->

    <!--<section id="fancyTabWidget" class="tabs t-tabs">-->
<!--        <div class="container">

            <ul class="nav nav-tabs fancyTabs" role="tablist">
                <li class="tab fancyTab active">
                    <div class="arrow-down"><div class="arrow-down-inner"></div></div>	
                    <a data-toggle="tab" href="#2018">2018</a>
                </li>
                <li class="tab fancyTab">
                    <div class="arrow-down"><div class="arrow-down-inner"></div></div>	
                    <a data-toggle="tab" href="#2017">2017</a>
                </li>
            </ul>
        </div>-->

        <!--<div class="tab-content">-->

            <!-- NEWS: 2018 -->
            <div id="2018" class="tab-pane fade in active show">
                <article class="news-content page-content" style="background-color: #31b0d5;" id="2018">
                    <div class="container">
                        <h1 class="news-heading text-center">2018</h1>

<!--                        <div class="row">
                            <div class="col-lg-4">
                                <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fswimlabsa%2Fposts%2F1540287172720727&width=500" width="300" height="373" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            </div>
                            <div class="col-lg-4">
                                <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fswimlabsa%2Fposts%2F1537299323019512&width=500" width="300" height="664" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            </div>
                            <div class="col-lg-4">
                                <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fswimlabsa%2Fposts%2F1538130092936435&width=500" width="300" height="632" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            </div>
                        </div>-->

                        <div class="row">
                            <div class="col-4">
                                <div class="card" style="background-color: #ffffff;">
                                    <div class="fb-post" data-href="https://www.facebook.com/swimlabsa/posts/1522890724460372" data-show-text="true"><blockquote cite="https://www.facebook.com/swimlabsa/posts/1522890724460372" class="fb-xfbml-parse-ignore"><p>Congratulations to swimlab for picking up 4 medals for 50 fly at WC Champs
                                                Rachel Groepes 11&amp;u gold
                                                Olivia Nel 14-15 silver
                                                Jeanri Buys 16&amp;o bronze
                                                Simon Hadden 16&amp;o bronze</p>Posted by <a href="https://www.facebook.com/swimlabsa/">Swimlab</a> on&nbsp;<a href="https://www.facebook.com/swimlabsa/posts/1522890724460372">Sunday, 3 December 2017</a></blockquote></div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card" style="background-color:#ffffff;">
                                    <div class="fb-post" data-href="https://www.facebook.com/swimlabsa/posts/1521923667890411" data-width="" data-show-text="true"><blockquote cite="https://www.facebook.com/swimlabsa/posts/1521923667890411" class="fb-xfbml-parse-ignore"><p>Congrats to the Swimlab girls last night picking up medals for 400 freestyle to open up Swimlab&#x2019;s medal count for...</p>Posted by <a href="https://www.facebook.com/swimlabsa/">Swimlab</a> on&nbsp;<a href="https://www.facebook.com/swimlabsa/posts/1521923667890411">Saturday, 2 December 2017</a></blockquote></div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card" style="background-color: #ffffff;">
                                    <div class="fb-post" data-href="https://www.facebook.com/swimlabsa/posts/1522074164542028" data-show-text="true"><blockquote cite="https://www.facebook.com/swimlabsa/posts/1522074164542028" class="fb-xfbml-parse-ignore"><p>Congrats to Jeremy Labuschagne for bronze in 11&amp;u boys 100 back at WC Championships</p>Posted by <a href="https://www.facebook.com/swimlabsa/">Swimlab</a> on&nbsp;<a href="https://www.facebook.com/swimlabsa/posts/1522074164542028">Saturday, 2 December 2017</a></blockquote></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card" style="background-color:#ffffff;">
                                    <div class="fb-post" data-href="https://www.facebook.com/swimlabsa/posts/1500835926665852" data-width="" data-show-text="true"><blockquote cite="https://www.facebook.com/swimlabsa/posts/1500835926665852" class="fb-xfbml-parse-ignore"><p>Our very own Jeanri Buys at World Cup a few weeks ago where she smashed some PBs and seen her with world champion, Emily Seebohm, Australian 200 backstroke gold medalist.</p>Posted by <a href="https://www.facebook.com/swimlabsa/">Swimlab</a> on&nbsp;<a href="https://www.facebook.com/swimlabsa/posts/1500835926665852">Thursday, 9 November 2017</a></blockquote></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card" style="background-color:#ffffff;">
                                    <div class="fb-post" data-href="https://www.facebook.com/swimlabsa/posts/1498396690243109" data-width="" data-show-text="true"><blockquote cite="https://www.facebook.com/swimlabsa/posts/1498396690243109" class="fb-xfbml-parse-ignore">Posted by <a href="https://www.facebook.com/swimlabsa/">Swimlab</a> on&nbsp;<a href="https://www.facebook.com/swimlabsa/posts/1498396690243109">Monday, 6 November 2017</a></blockquote></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card" style="background-color:#ffffff;">
                                    <div class="fb-post" data-href="https://www.facebook.com/swimlabsa/posts/1498866216862823" data-width="" data-show-text="true"><blockquote cite="https://www.facebook.com/swimlabsa/posts/1498866216862823" class="fb-xfbml-parse-ignore"><p>We are very proud announce that Swimlab&#039;s Toni Pluke  has been voted onto the Western Province Aquatics Exco as Vice...</p>Posted by <a href="https://www.facebook.com/swimlabsa/">Swimlab</a> on&nbsp;<a href="https://www.facebook.com/swimlabsa/posts/1498866216862823">Tuesday, 7 November 2017</a></blockquote></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </article>
            </div>
        </div>




         <!--NEWS: 2017--> 
        <div id="2017" class="tab-pane fade in active">
            <article class="news-content page-content" style="background-color:rgba(79,210,255, 0.7);" id="2017">
                <div class="container">
                    <h1 class="news-heading text-center">2017</h1>

                </div>
            </article>

        </div>
    <!--</section>-->
<!--</div>-->


<div class="page-content container">
    <h1 class="text-center">Follow Us to Read More Swimming News</h1>

    <div class="news-follow container text-center">
        <!--<div class="row text-center">-->

        <!--            <div class="col-sm-3 news-facebook">-->
        <i class="fa fa-5x fa-facebook-official"></i>
        <!--</div>-->

        <!--<div class="col-sm-3 news-twitter">-->
        <i class="fa fa-5x fa-twitter"></i>
        <!--</div>-->

        <!--<div class="col-sm-3 news-instagram">-->
        <i class="fa fa-5x fa-instagram"></i>
        <!--</div>-->

        <!--<div class="col-sm-3 news-google">-->
        <i class="fa fa-5x fa-google-plus"></i>
        <!--</div>-->
    </div>
</div>


<div id="fb-root"></div>

<script>
    (function (d, s, id)
    {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11&appId=128339154547426';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
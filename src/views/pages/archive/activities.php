<!DOCTYPE html>
<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<main id="bg-neutral-1">

    <section class="default-section" id="bg-neutral-1">
        <div class="container page-header" >
            <div class="text-center">
                <h1 class="h1">Activities<span style="font-weight:500;"> | Categories</span></h1>
            </div>
        </div>

        <section class="container-fluid">
            <div class="container">
                <p class="text-center lead">Browse our activities on offer by category. To read about and book activities for your ideal holiday itinerary, please visit our <a href="index.php?action=catalogue">Catalogue</a></p>
                <div class="card-group">

                    <?php
                    /* ======================================= *
                     * Check whether data is present           *
                     * --------------------------------------- */
                    if (isset($data["results"]))
                    {
                        /* ============================================ *
                         * Iterate through results and output activity  *
                         * -------------------------------------------- */
                        $activities = $data["results"];

                        foreach ($activities as $activity)
                        {
                            echo "<a href='index.php?action=catalogue&type=$activity' class='card-wrapper'>";
                            echo "<div class='card card-activity'>";
                            echo "<div class='card-body' id='ico-activity-sailing'></div>";
                            echo "<div class='card-footer'><h1 class='h1'>Sailing</h1></div>";
                            echo "</div>";
                            echo "</a>";
                        }
                    }
                    else
                    {
                        ?>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-sailing"></div>
                                <div class="card-footer"><h1 class="h1">Sailing</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-horse-riding"></div>
                                <div class="card-footer"><h1 class="h1">Horse Riding</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-scuba-diving"></div>
                                <div class="card-footer"><h1 class="h1">Diving</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-fishing-trips"></div>
                                <div class="card-footer"><h1 class="h1">Fishing</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-paragliding"></div>
                                <div class="card-footer"><h1 class="h1">Paragliding</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-hiking"></div>
                                <div class="card-footer"><h1 class="h1">Hiking</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-hiking"></div>
                                <div class="card-footer"><h1 class="h1">Helicopter Trips</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-boat-cruises"></div>
                                <div class="card-footer"><h1 class="h1">Boat Cruises</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-camping"></div>
                                <div class="card-footer"><h1 class="h1">Camping</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-scuba-diving"></div>
                                <div class="card-footer"><h1 class="h1">Shark Cage Diving</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-quad-biking"></div>
                                <div class="card-footer"><h1 class="h1">Quad Biking</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-wine-tasting"></div>
                                <div class="card-footer"><h1 class="h1">Wine Tasting</h1></div>
                            </div>
                        </a>

                        <a href="index.php?action=catalogue" class="card-wrapper">
                            <div class="card card-activity">
                                <div class="card-body" id="ico-activity-kite-surfing"></div>
                                <div class="card-footer"><h1 class="h1">Kite Surfing</h1></div>
                            </div>
                        </a>

                    <?php } ?>

                    <br>
                    <br>
                    <br>

                </div>
            </div>
        </section>
    </section>
</section>

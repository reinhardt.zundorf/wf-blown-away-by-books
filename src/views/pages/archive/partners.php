<main>

    <section class="default-section" style="margin-top:0px; margin-bottom:0px;">

        <!-- Heading -->
        <section class="activity-header" style="margin-bottom:15px;">
            <div class="row">
                <div class="container">
                    <h1 class="h1">About | <text class="h1" style="margin-left:7px;font-weight:500;">Partners & Subscriptions</text></h1>
                </div>
            </div>
        </section>

        <!-- Content -->
        <section class="page-content" id="bg-white">
            <div class="row">
                <div class="container">
                    <p>
                        It is with great pleasure that we present you with our new online one-stop shop for tourist activities and
                        destinations in the False Bay and greater Western Cape areas. We cater for the online selection and purchase of
                        Leisure Activities and Adventure Trips, with access to value-added Services, as provided by different travel service
                        providers in our area.
                    </p>
                    <p>
                        Our website makes provision for prospective visitors to explore the potential of tourist destinations available to visit
                        and with a wide variety of activities to engage in, when planning their next holiday.
                    </p>
                    <p>
                        By making their selections from the available options listed in the catalogue of our online store, visitors are
                        afforded the opportunity to custom-make their own holiday itinerary subject to their personal preferences and
                        particular to their individual fields of interest!
                    </p>
                    <div class="text-center">
                        <a href="?action=catalogue" class="btn btn-lg btn-catalogue">Browse Our Catalogue</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-content">
            <div class="row">
                <div class="container text-center">
                    <br>
                    <h1 style="font-size:20px;" id="text-primary">Become a partner in destination planning in the False Bay Area.</h1>
                    <p>Partnering with False Bay Tours is easy - subscribe now through our online portal.</p>
                    <a class="btn btn-destinations">Subscribe online</a>
                </div>
            </div>
        </section>

        <section class="page-content" id="bg-white">
            <div class="row">
                <div class="container">
                    <p>
                        Our logistical management systems and platforms run on the back of some of the latest technologies and are
                        designed to facilitate and support an integrated central reservation system. By taking care of the administrative
                        management of reservations, we gear towards providing visitors with the convenience of minimized admin and
                        increased peace of mind, giving them more time to uninterruptedly savor the wholesomeness of False Bay and
                        surrounds.
                    </p>
                    <p>
                        The additional listings of service providers such as that of Accommodation Services, Dining Outlets, Vehicle
                        Rentals-or Services and qualified Tour Guides, provide travelers with a framework of associated, established and
                        credible service providers who are subscribed to our site, rated and recommended as such.
                    </p>
                    <p>
                        False Bay Tours employs extensive Search Engine Optimization applications and network our operations over a
                        variety of social media platforms - maximizing the various avenues of digital exposure, with specifically False Bay
                        and associated service and destination providers as the target.
                    </p>
                    <p>
                        Our blog is regularly updated by our team with community news, upcoming events, reviews and special offers or
                        promotions. Feature articles are alternately published to introduce and showcase our subscribed service providers
                        and advertisers - giving priority to their specialties and highlighting key aspects of their operation.
                    </p>
                    <p>
                        If you are interested to receive more details on subscribing and be given preference in the listing of associate
                        service providers for False Bay Tours, kindly contact us at info@falsebaytours.com or alternatively, for
                        information to register as an advertiser, at marketing@falsebaytours.com.
                    </p>
                </div>
            </div>
        </section>

        <section class="container-fluid" id="">
            <section class="default-heading"">
                <div class="container">
                    <div class="text-center justify-content-center">
                        <h1 class="h1" >Interested in becoming a partner?</h1>
                        <p class="lead">We offer three subscription types:</p>
                    </div>
                </div>
            </section>
        </section>

        <section class="page-content" style="margin-bottom:30px;">
            <div class="container text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-3">
                        <i class="fa fa-5x fa-globe"></i>
                        <h3>Website Ads</h3>
                        <p class="lead">Want to place an advert on our website? Find out more <a href="index.php?action=subscribe?type=advert">here</a></p>
                    </div>
                    <div class="col-lg-3">
                        <i class="fa fa-5x fa-bicycle"></i>
                        <h3>Activities & Destinations</h3>
                        <p class="lead">Add your Destinations and Activities to our Catalogue and let us help you boost your sales <a href="index.php?action=subscribe&type=partner">here</a></p>
                    </div>
                    <div class="col-lg-3">
                        <i class="fa fa-5x fa-suitcase"></i>
                        <h3>Service Listings</h3>
                        <p class="lead">Own a business in the hospitalities or accomodation industry? Apply to be added on our Travel Services lists <a href="index.php?action=subscribe&type=listing">here</a></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="container-fluid" id="bg-white">
            <section class="default-heading text-center">
                <div class="container">
                    <div class="text-center justify-content-center">
                        <h1 class="h1">Or download and complete our Partners Application Form</h1>
                    </div>
                </div>

                <ol class="list-unstyled">
                    <li>1. Download & complete a subscription form. </li>
                    <li>2. Email the completed form to <a href="mailto:partners@falsebaytours.com">partners@falsebaytours.com</a></li>
                    <li>3. As soon as we have processed your form, we will contact you.</li>
                </ol>
                <a href="./partners/2018-Partners_Form.pdf" class="btn btn-lg btn-info">Download Form</a>
            </section>
        </section>


        <section class="page-content" style="background-color:#ededed;">
            <div class="container text-center">
                <h1 class="h1">Still not clear?</h1>
                <p class="lead">For more information or any queries please email us at <a href="mailto:partners@falsebaytours.com">partners@falsebaytours.com</a></p>

                <!--<div class="col-md-9 offset-2">-->
                    <form id="contact-form" method="post" action="?action=email" role="form">
                        <div class="controls">
                            <div class="row text-center">
                                <div class="col-md-3 offset-3">
                                    <div class="form-group">
                                        <label>Enter your first name:</label>
                                        <input id="form_firstname" type="text" name="name" class="form-control" placeholder="" required="required" data-error="First name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Enter your last name:</label>
                                        <input id="form_lastname" type="text" name="name" class="form-control" placeholder="" required="required" data-error="Last name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-3 offset-3">
                                    <div class="form-group">
                                        <label>Enter your email address:</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Enter your phone number:</label>
                                        <input type="number" name="mobile" class="form-control" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-4 offset-4">
                                    <div class="form-group">
                                        <label>Please explain how we can help you:</label>
                                        <textarea style="height:150px;" id="form_message" name="message" class="form-control" placeholder="" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-4 offset-4">
                                    <input type="submit" class="btn btn-success btn-send" value="Send message">
                                </div>
                            </div>
                        </div>
                        <div class="messages"></div>
                    </form>
                </div>

                <!--                <div class="col-lg-4 offset-4">
                                    <div class="form">
                                        <form class="form-horizontal" method="post">
                                            <div class="form-group">
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon "><i class="fa fa-group"></i></div>
                                                        <input type="text" class="form-control" id="inputFirstname" placeholder="First name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="inputLastname" placeholder="Last name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email address">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" id="inputQuery" placeholder="Query"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="inputOrganisation" placeholder="Name of your organisation">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-danger">Send</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>-->
            </div>
        </section>
    </section>
</main>

<!DOCTYPE html>
<html>

    <!-- HEAD -->
    <head>

        <!-- META -->
        <meta charset="UTF-8">

        <!-- TITLE -->
        <title>False Bay Tours | Portal</title>

        <!-- CSS -->
        <link href="./css/tether.min.css"                  rel="stylesheet">
        <link href="./css/bootstrap/bootstrap.css"         rel="stylesheet">
        <link href="./css/bootstrap/bootstrap-grid.css"    rel="stylesheet">
        <link href="./css/bootstrap/bootstrap-reboot.css"  rel="stylesheet">
        <link href="./css/font-awesome.css"                rel="stylesheet">

    </head>

    <!-- BODY -->
    <body>

        <!-- HEADER -->
        <img src="alpha/res/Landing/header.png" alt="False Bay Tours Logo" />

        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header" id="bg-base-2">

                        </div>
                        <div class="card-body">

                        </div>
                        <div class="card-footer">

                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header"  id="bg-base-3">

                        </div>
                        <div class="card-body">
                            Landing ALPHA
                        </div>
                        <div class="card-footer">
                            <a href="./alpha/index.php">View</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header" id="bg-base-1">

                        </div>
                        <div class="card-body">
                            Blog Demo
                        </div>
                        <div class="card-footer">
                            <a href="./alpha/index.php?action=blog">View</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>

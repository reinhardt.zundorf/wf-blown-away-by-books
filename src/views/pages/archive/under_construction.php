<!DOCTYPE html>
<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->

<link rel="stylesheet" href="css/sections/construction.css"/>

<section class="container-fluid" id="bg-neutral-1">
    <div class="container" >
        <div >
            
            <!--<i class="fa fa-5x fa-warning"></i>-->
            <br>
            <div class="text-center"><img src="res/graphics/construction-icon.png" style="width:325px;" alt="Construction"/></div>
            
            <br>
            <p>
                <br><img style="width:175px;margin-bottom:0px;padding-bottom:5px;" src="res/brand/brand-text-xsm.png" alt="False Bay Tours"/>  will afford prospective visitors to the Western Cape the opportunity to design their own holiday itinerary - tailored to their own personal requirements and catering for individual tastes and interests. <br><br>
                A variety of Leisure Activities and Adventure Trips ranging from Hiking, Snorkel Diving and Wine Tasting, to Helicopter Tours and a visit to Cape Point as example, will be available to be selected as components to create an unforgettable touring experience. <br>
            <br>Some service features such as an Integrated Reservation System for Accommodation, Vehicle Rentals and Dining Out services, propose to care for the convenience and peace of mind of visitors exploring False Bay.
            <br><br>
            Subscribed service providers to this site will be listed, rated and recommended. This provides guests with a network frame of associated, established and credible professionals who are in turn commonly geared for the care and comfort of our visitors.</p>
            <p><br>For more information and updates, <a href="index.php?action=blog">follow our blog </a> or contact us by mail at <a href="mailto:info@falsebaytours.com">info@falsebaytours.com</a> <br>or directly on (+27) 073 792 0736</p><br>



            <!-- MODAL: Notify -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #00cc66; color: #ffffff;">
                            <h5 class="modal-title" id="exampleModalLabel">Notify Me</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Please enter your email address below and we will notify you when we are up and running!</p>
                            <form class="form-default" id="subscribeForm" novalidate="novalidate">
                                <div class="form-group required bg-light">
                                    <label class="form-label" for="subscribeEmail">Email address</label>
                                    <input class="form-control" id="subscribeEmail" type="text" name="email">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                <div class="form-notify"></div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <!-- MODAL: Contact -->
            <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #4e53a7; color: #ffffff;">
                            <h5 class="modal-title" id="exampleModalLabel">Contact Us</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h3>False Bay Tours</h3>
                            <p>TEL: 073 792 0736</p>
                            <p>EMAIL: info@falsebaytours.com</p>
                        </div>
                    </div>
                </div>
            </div>
 
            
            
            </section>
            <!-- FOOTER -->
            <footer class="footer">
                <div class="container">
                    <div class="row" style="min-height:120px;">
                        
                    </div>
                </div>
            </footer>

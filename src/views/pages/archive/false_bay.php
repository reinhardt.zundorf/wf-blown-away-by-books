<!-- ABOUT US -->
<!--    <aside>
        <ul class="list-group">
            <li><h3>False Bay</h3></li>
            <l<a href=""</li>
            <li><h3>False Bay</h3></li>
        </ul>
    </aside>-->

<main>

<!-- FALSE BAY -->
<section class="default-section">
    <div class="container">
        <div class="row">

            <div class="col-md-10">
                <!-- ACTIVITY: Content -->
                <article class="activity-main">

                    <!-- ACTIVITY: Header -->
                    <section class="activity-header">
                        <div class="row">
                            <h1 class="h1">Destinations | </h1><h1 class="h1" style="margin-left:7px;font-weight:500;">False Bay</h1>
                        </div>
                        <br>
                    </section>
                    
                    

                    <section class="page-content" id="bg-white">

                        <h2 class="h2">Description and Location</h2>
                        <p>
                            The Northern shore is defined by a very long, curving, sandy beach. This sandy, Northern perimeter of the bay is the Southern edge of the area known as the Cape Flats. The bay is 30 kilometres wide at its widest point. 
                            The False Bay coastline spans from Muizenberg through St James, Kalk Bay and Fish Hoek to Simon's Town - for many years the site of a major British naval base.
                            The Eastern and Western shores of False Bay are very rocky and mountainous. In places large cliffs plunge into deep water - Koeëlberg (1289m / 4229 feet), 
                            which rises from the water itself forming the highest point of the Kogelberg, as well as the Somerset Sneeukop (1590m / 5217 feet) and Wemmershoek Peak (1788m / 5866 feet).
                        </p>

                        <h2 class="h2">Beaches along False Bay</h2>
                        <p>
                            This coast was Cape Town's first fashionable bathing area, particularly as the water here is 5-7 degrees warmer than on the Atlantic Coastline. 
                            There are many good beaches here, with the best being Muizenberg, and St. James, with its brightly coloured bathing booths and tidal pool, Fish Hoek and Boulder's Beach just beyond Simon's Town. 
                        </p>

                        <h2 class="h2">Climate</h2>
                        <p>
                            Lying between the Indian and Atlantic Oceans, Cape Town enjoys mild winters and pleasant summers. Summer temperatures in December to February range from around 15 to 30 degrees Celsius (60 to 80 degrees Fahrenheit), whilst in the winter months of June to August average temperatures are between 5 to 20 degrees Celsius (45 to 70 degrees Fahrenheit). 
                            Rainfall is moderate and there are refreshing sea breezes which can sometimes turn a little bracing during the winter.
                            Summer (November to March) brings long, hot days with spectacular late sunsets. Lots of outdoors daytime activities can be seen around with people exploring the coastal villages and their markets, or enjoying the various beaches and hiking trials.
                            South African summer holidays (usually from the first week of December to mid - January) and Easter, are peak periods for visiting Cape Town, so planning trips should  be done reasonably in advance, and be prepared for lots of company! Cape Town restaurants, hotels and guest houses are full and many visitors book weeks in advance of arriving in Cape Town. 
                            Local wisdom tells us that Spring (September and October) and Autumn (April and May) are the smart times to visit Cape Town. They are usually balmy in-between seasons, with little wind, when nature is at its showiest .Whether you are into flowers, whale-watching, or seeing the vineyards at their best, these are great seasons to be in the Cape.
                            Although Cape Town winters have a chilly reputation for rain and wind, they often produce unexpected sunny days. Some climatic change seems to have reduced winter rainfall, and when it’s not raining, winter becomes a perfect green season, with temperatures reaching a summery 26C (80F) or higher. This time is known in Cape Town as the "Secret Season" - because of all the secret pleasures to be experienced!
                            Whatever season you have chosen to visit Cape Town, be warned that Cape Town has four seasons - sometimes in the same day…  This is especially true if you plan to go hiking or be outdoors. Check the weather forecasts in local papers or on radio. Or phone 40881 for detailed short-term forecasts.
                            Water Temperature
                        </p>
                        <p>
                            Average summer surface temperature of the Atlantic off the Cape Peninsula is in the range 10° to 13°C. The bottom temperature may be a few degrees colder. Minimum temperature is about 8°C and maximum about 17°C. Average winter surface temperature of the Atlantic off the Cape Peninsula is in the range 13° to 15°C. The bottom temperature inshore is much the same.
                            Average winter surface temperature of False Bay is approximately 15°C, and the bottom temperature much the same. Average summer surface temperature of False Bay is approximately 19°C. The bottom temperature is 1° to 3°C lower than it is in winter.
                        </p>

                        <h2 class="h2">History</h2>
                        <p>
                            Bartolomeu Dias once in 1488 first referred to the bay as "the gulf between the mountains". 
                            The name "False Bay" was applied early on (at least three hundred years ago) by sailors who confused the bay with Table Bay to the north. 
                            The confusion arose because of sailors returning from the east, initially confused Cape Point and Cape Hangklip, which are somewhat similar in form.
                            Hangklip was known to the early Portuguese seafarers as “Cabo Falso”, or “False Cape”, deriving in the name of the bay as it today stands.
                        </p>
                        <p>
                            False Bay was not used until 1671 when bad weather forced the Dutch ship, Isselsteijn, to call in at Simon’s Bay and reported anchorage in False Bay to be superior to that of Table Bay. In 1687 Simon van der Stel became the first Dutch official to extensively explore False Bay and the Cape Peninsula. The Cape lion and the Quagga (both extinct), elephants, hippos, eland, hartebeest, rhebok and other game were plentiful - Simon van der Stel habitually lead hunting parties from the Silvermine Valley and Oliphantsbospunt, which was named after a part of the elephant’s migration route.
                            In 1741, the directors of the Dutch East India Company decreed that between May and August all ships would anchor in Simon’s Bay. Today Simon’s Town is the principal harbour of the South African Navy, and has many historic buildings. The other two major harbours are Kalk Bay and Hout Bay, also difficult to reach by land for hundreds of years
                            The Dutch rule at the Cape ended in September 1795 after the Battle of Muizenberg, against invading British forces that landed at Simon’s Town. They used the wind to their advantage, bringing their heavily armed ships up to Muizenberg to bombard the Dutch encampment. 
                            The early Dutch defence at Muizenberg is still visible on the rocks near Bailey’s Cottage, whilst the later British defence can be found on the hillside across the road. Cannon balls from this historical battle are still being found in Muizenberg.
                            The Posthuys in Muizenberg  was one of the early Toll customs houses built by the Dutch in the late 18th Century.  Rhodes Cottage and the SA Police Museum can be found in a cluster of historic houses near the Natale Labia Museum.
                        </p>
                        <p>
                            A number of wine farms along the route showcase their produce by way of cellar tours and tasting rooms, where visitors can sample some of the finest wines produced in the Western Cape.
                            Numerous nature reserves throughout the bay allow guests to savour the natural and preserved beauty of the coast. In some instances camping facilities are additionally provided with tented camps or self-catering chalets where guests can relax in close proximity to the African stars by night and breathtakingly beautiful beaches and other indigenous floral landscapes by day.
                            Culture is kept alive through theatres and a variety of open-air venues celebrating local art and music. Upcoming events are locally advertised and all year round there is a fun- filled occasion for everyone to look forward to.
                            International sporting events such as the “Argus Cycle Tour” annually brings thousands of guests to the area, both in support of the sport and also in pursuit of exploring the richness in diversity of the resources offered by False Bay and its people.
                            What are you still waiting for?
                        </p>
                        <h2 class="h2">General Visitor Pursuits</h2>
                        <p>
                            As the biggest and only true bay and being one of Cape Town’s most popular spots, False Bay attracts visitors all year round and is a popular swimming, surfing, sunbathing and picnicking destination. 
                            Snorkelers and scuba divers a are also very active in False Bay. Activities such as recreational fishing and bait collection extend around the shores of the entire bay. Commercial fishing in False Bay dates back more than 300 years and includes trek-net fishing, line-fishing and rock lobster fishing. 
                        </p>
                        <p>
                            Eco-tourism enterprises like whale-watching, shark cage diving, hiking and boat trips, top popularity in False Bay and are seen by many as a positive step towards the sustainable use of the bay’s natural resources, generating alternative local livelihood options. 
                            Another recent development, the application for aquaculture ventures in False Bay such as the ranching of yellowtail fish, kelp farming and an experimental whelk fishery, contributes to the income of a new generation of fishing folk .
                            Restaurants and accommodation centres proliferate the area and have something to offer for all tastes in False Bay. Craft markets and curio vendors are dotted throughout the area – giving visitors the opportunity to support the local economy and acquire memorabilia to remind them of a fantastic visit. 
                        </p>
                    </section>
                </article>
            </div>
        </div>
    </div>
</section>

<!-- SECTION: About Header -->
<section class="page-header">
    <div class="container text-center">
        <h1>About Us</h1>
    </div>
</section>

<!-- SECTION: about-hero -->
<section class="page-content" id="about-content">
    <div class="container">

        <!-- SECTION: Columns -->
        <div class="row">
            <div class="col">

                <!-- TABS: Nav -->
                <ul class="nav nav-tabs" id="about-tabs" role="tablist">   
                    <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#calendars" role="tab" aria-controls="calendars">Calendar</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#meet-info" role="tab" aria-controls="meet-info">Meet Info</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#handbooks" role="tab" aria-controls="handbooks">Handbooks</a></li>
                </ul>

                <!-- TABS: Content -->
                

                    <!-- TAB: Academy -->
                    <div id="calendars" class="tab-pane active" role="tabpanel">

                        <div class="container">
                            <div class="text-center">
                                <div class="section-header text-center">
                                    <h2>Calendars</h2>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <p>
                            Archived (older) Western Province Aquatics Handbooks:
                        </p>
                        <hr>
                        <ol>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ol>
                    </div>

                    <!-- SECTION: section-about -->
                    <div id="meet-info" class="tab-pane" role="tabpanel">
                        <div class="container">
                            <div class="text-center">
                                <div class="section-header text-center">
                                    <h2 class="text-center" data-animation="bounceInUp">Meet Info</h2>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <p>
                            Archived (older) Western Province Aquatics Handbooks:
                        </p>
                        <hr>
                        <ol>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ol>


                        <!-- Row #1 -->
                        <div class="row align-center">
                        </div>
                    </div>

                    <!-- TAB: Values -->
                    <div id="handbooks" class="tab-pane" role="tabpanel">
                        <div class="container">
                            <div class="text-center">
                                <div class="section-header">
                                    <h2 class="text-center" data-animation="bounceInUp">Handbooks</h2>
                                    <hr>
                                </div>
                            </div>
                            <p>
                                Archived (older) Western Province Aquatics Handbooks:
                            </p>
                            <hr>
                            <ol>
                                <li></li>
                                <li></li>
                                <li></li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.section-inner -->
</section><!-- /#landing-hero -->

<!-- SECTION: section-about -->


<!--<section class="page-content" id="about-mission">
    <div class="container text-center">
        <div class="row text-center">
            <h2>We strive to help athlete's develop each athlete to their full potential.</h2>
        </div>
    </div>
</section>-->




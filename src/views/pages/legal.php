<section class="default-section" id="bg-neutral-1">
    <div class="container">
        <section class="activity-header">
            <div class="row">
                <h1 class="h1">False Bay Tours | </h1><h1 class="h1" style="margin-left:7px;font-weight:500;">Legal Information</h1>
            </div>
            <br>
        </section>

        <section class="container-fluid" id="bg-white">
            <div class="container">
                <div class="page-content">

                    <!-- BUSINESS OWNER -->
                    <p><b>Business Owner:</b> VentureCo (Pty) Ltd</p>

                    <!-- WEBSITE HOST -->
                    <p><b>Website Host:</b></p>

                    <p>Web Folders (Pty) Ltd<br>
                        7974, Fish Hoek<br>
                        Western Cape<br>
                        South Africa<br>
                    </p>
                    <hr>

                    <!-- MANAGEMENT -->
                    <h2 class="h2">Management</h2>

                    <p><b>Company</b>: VentureCo (Pty) Ltd</p>

                    <p><b>Director</b>: Deon van Biljon</p>

                    <p><b>Contact Information:</b><a href="mailto:info@falsebaytours.com">info@falsebaytours.com</a> | (+27) 073 792 0736</p>

                    <hr>

                    <p>
                        The Arbitration Foundation of South Africa (AFSA) provides a platform for online-related dispute resolution. http://www.arbitration.co.za/pages/default.aspx
                        VentureCo (Pty.) Ltd captures, process and use data as part of our service of offering tourist services online via the internet and mobile applications (the “Service”).
                        The protection of your personal data as a part of all the services we provide, is of high priority to us. Therefore we gather, pro-cess and use your personal data exclusively in accordance with the applicable legal regulations.
                        In this privacy statement we explain how we handle your personal data as part of your use of our website.
                        The party responsible for data protection VentureCo ( Pty) .Ltd,(“FBT”), as well as those companies affiliated with FBT, to whom you have given the appropriate consent for the use of your personal data.
                        We reserve the right to adjust the content of this privacy statement from time to time. It is therefore advisable to periodically review this privacy statement.
                    </p>

                    <ul>
                        <li>1. General Information<br>

                            We are committed to observe the applicable data protection laws, in particular, the provisions of the South African Law Reform Commission. http://www.justice.gov.za/salrc/dpapers/dp109.pdf

                            We collect, process and use your personal data only with your consent or when this processing or use is permissible by law. We only process or use the data which is required for using the site or the data which you provide to us.
                        </li>

                        <li>2. Personal Data</li>

                        Personal data is specific information about personal or factual circumstances regarding an identified or identifiable natural person. This might include information such as your name, your address and email address, or your phone number and banking details.

                    </ul>


                    <p>                            3. Automatic Collection and Processing of Data During the Use of this Website

                        3.1 Data Processing to Enable the Use of the Website
                        When you visit our website, we collect the data necessary to enable your use of this website (usage data). This includes your IP address as well as data about the beginning and end of sessions and the telemedia used, it may also include data used to identify you (e.g. your login credentials if you log in to a secure area of the website). These data serve the purposes of delivering the service and designing it in a needs-based manner. Generally, they are deleted when no longer needed. For information on the processing of data in the form of pseudonymous usage profiles, see section 3.3.
                        3.2 Cookies 
                        When you visit our website, information may be stored on your computer in the form of cookies. Cookies are small text files that are transferred between a webserver and your browser and are stored on your computer’s disk. This makes it possible to recognize you when you re-visit the website. This way, we can offer you better functionality of our website and, for example, avoid that you have to log in repeatedly, or allow us to carry out web analysis (see section 3.3). Most browsers are configured to automatically accept cookies.
                        By configuring your browser, you may prevent that cookies are stored, and you may also erase cookies at any time. Please note that without cookies, the use and experience of our services via this website may be impeded or impossible. In particular, without cookies, it may not be possible to book a guide, as cookies are required to verify your booking data. Via your browser settings, you can also prevent certain cookies (such as third-party-cookies) from being stored, e.g. if you want to prevent web tracking. You will find further information in your browser’s help section.
                        3.3 Pseudonymous Usage Profiles for Advertising and Market Research (Web Tracking and Analysis) 
                        For the purposes of advertising and market research and to optimize the user experience of our website, we use web tracking technology. Respective data regarding the use of our website is stored in pseudonymous usage profiles (your IP addresses is stored in anonymized form). This way, we are able to improve our website and to better adjust the content to your needs. Usage profiles are also used for so-called retargeting. This enables us to place ads with interesting offers also on other websites that you visit. Pseudonymous usage profiles will not be re-combined with personal data.
                        You can object to the building of pseudonymous usage profiles. To this end, you can configure your browser so that it does not accept cookies (see section 3.2). You can also use a browser plugin to protect your privacy – e.g. AdBlock, Ghostery or NoScript (before installing any plugins, consider the applicable privacy policy).
                        Some providers of tracking technology have joined advertising associations (see below for details), allowing users to centrally opt-out of receiving targeted online ads by any of the members of the respective association. 
                        3.3.1 Facebook Custom Audience; Facebook Pixel
                        Our website contains tracking technology by Facebook, 1601 S. California Avenue, Palo Alto, 94304 CA, USA („Facebook“). This may include cookies (see section 3.2). Facebook collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device. You will find further information on the data processing by Facebook in the privacy policy of Facebook https://www.facebook.com/privacy/explanation
                        3.3.2 Google Analytics
                        Our website uses Google Analytics, a web analytics service by Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA („Google“). Google Analytics uses cookies (see section 3.2) in order to analyse your use of this website. The information about your use of this website collected using cookies is usually transferred to and stored on servers of Google Inc. in the US. Prior to the transfer to the US, Google masks and thereby anonymizes your IP address within the territory of the EU or of the European Economic Area. Only in exceptional cases, the full IP address is sent to and masked by Google servers in the US. On behalf of the website provider Google will use this information to analyse your use of the website, compiling reports on website activity for website operators and providing other services relating to website activity and internet usage to the website provider. Google will not combine your IP address with any other data held by Google. 
                        You may refuse to have cookies stored on your device by appropriately configuring your browser (see section 3.2) or by using a privacy plugin (see section 3.3). Furthermore you can prevent Google’s collection and use of data (cookies and IP address) by downloading and installing the browser plug-in available here: https://tools.google.com/dlpage/gaoptout?hl=en-GB. Alternatively, you can prevent the tracking by Google Analytics by setting an opt-out-cookie on your device. 
                        3.3.4 Google Double-Click, Google AdWords Conversion, Google Dynamic Remarketing
                        We also use Google Analytics to evaluate data from the Google services AdWords and DoubleClick for statistical purposes. This way, in order to improve our services, we can analyse what happens after a user has clicked on one of our ads, e.g. whether a user has ordered a product or has viewed the ad from a mobile device. Furthermore, you will receive interest-based ads through these services. You can opt out of such interest-based ads via the Google Ads preferences pages: http://www.google.com/settings/ads/onweb/?hl=en.
                        DoubleClick places a cookie on your device to track your surf profile across webpages and to serve you interest-based ads. If you want to permanently opt out of this, you can download a plugin to deactivate this cookie under the following link: https://www.google.com/settings/u/0/ads/plugin?hl=en
                        3.3.5 Google Tag Manager
                        This website uses Google Tag Manager to manage website tags. A tag is a JavaScript snippet that can be used to send information from a website to a third party, in particular in the context of web tracking. The tool Google Tag Manager itself does not collect personal data. Rather, the tool triggers other tags that may collect data (such as the tag of Google Analytics). Google Tag Manager does not access such data. A deactivation on the domain level or cookie level will affect all tracking tags implemented via Google Tag Manager. This facilitates the implementation of an effective opt-out of web tracking.
                        3.3.6 Mixpanel
                        Our website contains tracking technology by Mixpanel, San Francisco 405 Howard Street, Floor 2. San Francisco, CA 94105(“Mixpanel”). This may include cookies (see section 3.2). Mixpanel collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Mixpanel in the privacy policy of Mixpanel (https://mixpanel.com/privacy/).
                        3.3.7 New Relic
                        Our website contains tracking technology by New Relic, 188 Spear St., Suite 1200 
                        San Francisco, CA USA 94105 (“New Relic”). This may include cookies (see section 3.2). New Relic collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by New Relic in the privacy policy of New Relic (https://newrelic.com/privacy).
                        3.3.8 Sift Science
                        Our website contains tracking technology by Sift Science, 123 Mission Street, 15th Floor San Francisco, CA 94105 . This may include cookies (see section 3.2). Sift Science collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Sift Science in the privacy policy of Sift Science.
                        3.3.9 Optimizely
                        Our website contains tracking technology by Optimizely, 631 Howard Street, Suite 100. San Francisco, CA 94105.This may include cookies (see section 3.2). Optimizely collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Facebook in the privacy policy of Optimizely (https://www.optimizely.com/privacy/).
                        3.3.10 Airpr
                        Our website contains tracking technology by Airpr, Maiden Lane, Suite 530
                        San Francisco, CA 94108.This may include cookies (see section 3.2). Airpr collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Airpr in the privacy policy of Airpr (https://airpr.com/privacy-policy/).
                        3.3.11 Hotjar
                        Our website contains tracking technology by Hotjar, St Julians, Saint Julian's, MT. This may include cookies (see section 3.2). Hotjar collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Hotjar in the privacy policy of Hotjar (https://www.hotjar.com/privacy).
                        3.3.12 Bing
                        Our website contains tracking technology by Bing, 555 110th Ave NE; Bellevue, Washington 98004, USA. This may include cookies (see section 3.2). Bing collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Bing in the privacy policy of Bing (https://advertise.bingads.microsoft.com/en-us/resources/policies/privacy-and-data-protection-policies ).
                        3.3.13 Booking.com
                        Our website contains tracking technology by Booking.com,Herengracht 597 1017 CE Amsterdam Netherlands. This may include cookies (see section 3.2). Booking.com collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Booking.com in the privacy policy of Booking.com (https://www.booking.com/content/privacy.en-gb.html)
                        3.3.14 Opodo 
                        Our website contains tracking technology by Opodo,Hammersmith Embankment, Chancellors Road, London, W6 9RU. This may include cookies (see section 3.2). Opodo collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Opodo in the privacy policy of Opodo (https://www.opodo.co.uk/privacy-policy/)
                        3.3.15 Spotefffects
                        Our website contains tracking technology by Spoteffects, Knorrstr. 69 80807 München. This may include cookies (see section 3.2). Opodo collects and stores usage data in pseudonymous profiles for the purposes of web analysis and interest-based advertising. You can opt-out of such data collection and storage at any time with effect for the future by setting an opt-out-cookie on your device via the website of the EDAA (see section 3.3). You will find further information on the data processing by Spoteffects in the privacy policy of Spoteffects (http://www.spoteffects.com/footer/imprint/#c920 )

                        4. Transfer of Personal Data

                        False Bay Tours will not share your personal data with third parties, unless you have previously given your express permission, or unless the transmission is required or permissible by law. We will in particular not sell or otherwise market your data to third parties. Transfers of data to government institutions and authorities, is done only within the scope of mandatory national legal requirements. Our employees and partners are obligated by us to maintain secrecy and to comply with legal da-ta protection regulations.


                        5. Use of Data for Marketing Purposes / Consent

                        5.1 Based on your declaration of consent, FBT processes and uses your personal data when applicable for marketing activities, such as for sending emails with general information or advertising information (newsletters).

                        6. About Data Collected From Mobile Applications or Devices

                        6.1 When you download data, use our mobile applications or access one of our webpages optimized for mobile devices, we may, as described above in this declaration, collect information about you and your mobile device. This many include data about your location. We use this information to provide location-based services such as search results and other personalized content for you, if approved by you and your device. On most mobile devices, you can control or disable location services in the settings menu. If you have questions about how to disable location services on your device, we recommend that you contact your mobile service provider or the manufacturer of your device.
                        6.2 If we collect additional personal data which is transmitted as a result of your use of our mobile applications or your accessing our website with a mobile device, we will obtain your express con-sent in advance.

                        7. Data Collected from Other Sources

                        We may collect additional information about you from third parties in order to supplement our account information. This information may be, among other things, demographic and navigation data, credit check information and information from credit reporting agencies, to the extent legally permissible.

                        8. Data Storage

                        Your personal data will be stored for only as long as necessary for the purposes stated in this privacy statement or for as long as we are required by law or legally obligated to store this information.



                        9. Right of Access, Rectification, Deletion

                        You have the right at any time to receive information free of charge about your personal data stored with us. If your data is stored incorrectly or wrongly by us, we will gladly correct, block or delete it. Please inform us of any changes to your data immediately.
                        Please direct requests for information, questions, complaints or suggestions to the following address:

                        VentureCo (Pty) Ltd 
                        4 Protea Ave
                        Fish Hoek
                        7974
                        Cape Town
                        South Africa
                        Tel: + 27 73 792 0736
                        or via email: admin@falsebaytours.com
                    </p>
                </div>
            </div>
        </section>
    </div>
</section>

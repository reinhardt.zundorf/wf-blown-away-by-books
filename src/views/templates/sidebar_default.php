 <!-- SIDEBARS -->
<div class="col-lg-3 col-md-3">
    <!-- <div class="sidebars">     -->

        <!-- FESTIVAL -->
        <article class="sidebar sidebar-about">
            <header class="sidebar-header">
                <h3 class="h3"><i class="sidebar-icon"></i><?php echo ucwords($view["title"]); ?></h3>
            </header>
            <section class="sidebar-body">
                <p class="p">
                    <?php if($view["title"] === "film") 
                    {
                        echo "The group meets the first and third Friday of every month (and the fifth Friday if there are five Fridays in that month) 
                        unless there is a public holiday. Admission is free to all library members.</p>";
                        echo "<p><i>Donations are welcome to purchase DVDs for the library collection.</i> ";
                    }
                    ?>                    
                </p>
            </section>
        </article>

        <!-- SUBSCRIBE -->
        <article class="sidebar sidebar-newsletter">
            <header class="sidebar-header">
                <h3 class="h3"><i class="sidebar-icon"></i>Keep me updated</h3>
            </header>
            <div class="sidebar-body">
                <div class="form-text">
                    <input class="input-group-text" id="subscribe-email" label="Email" placeholder="Please enter your email address" type="email"  />
                    <a href="index.php?action=subscribe" class="btn btn-block btn-success">Subscribe</a>
                </div>
            </div>
        </article>

        <!-- ABOUT US -->
        <article class="sidebar sidebar-about">
            <header class="sidebar-header">
                <h3 class="h3">About Us</h3>
            </header>
            <section class="sidebar-body">
                <h4 class="h4">Friends of the Fish Hoek Library</h4>
                <p class="p">
                    Blown Away By Books or 'Friends of the Fish Hoek Library' is a society for the appreciation of literature and film at the Fish Hoek Public Library.
                </p>
                <h4 class="h4">Our Mission</h4>
                <p class="p">
                    Book-lovers, literary celebrities, poets and performers are given the opportunity to interact. 
                    It also raises the profiles of libraries in the different communities, and creates opportunities for local businesses.
                </p>
            </section>
            <section class="sidebar-footer">
                <a class="btn btn-info" href="index.php?action=festival">Read more <i class="fa fa-right"></i></a>
            </section>
        </article>

        <!-- MAP -->
        <article class="sidebar sidebar-map">
            <header class="sidebar-header">
                <h3 class="h3">Find us</h3>
            </header>
            <section class="sidebar-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13209.506088086286!2d18.426808!3d-34.136708!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcef68b2b6fef69d8!2sFish+Hoek+Public+Library!5e0!3m2!1sen!2sza!4v1529064375526" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
            </section>
        </article>

    </div>
</div>

<!-- GALLERY -->
<div class="wrapper">
    <section class="page-content" id="gallery-container" >

        <div class="container">
            <!-- GALLERY: Tabs -->
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" >
                    <a class="nav-link active"  id="gallery1-tab" data-toggle="tab" href="#gallery1" role="tab" aria-controls="gallery1" aria-selected="true">
                        Archive
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active"  id="gallery2-tab" data-toggle="tab" href="#gallery2" role="tab" aria-controls="gallery2" aria-selected="true">
                        Festival 2015
                    </a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link active"  id="gallery3-tab" data-toggle="tab" href="#gallery3" role="tab" aria-controls="gallery3" aria-selected="true">
                        Festival 2016
                    </a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link active"  id="gallery4-tab" data-toggle="tab" href="#gallery4" role="tab" aria-controls="gallery4" aria-selected="true">
                        Festival 2017
                    </a>
                </li>
            </ul>

            <!-- GALLERY: Content -->
            <div class="tab-content">

                <!-- TYR 2017 -->
                <div class="tab-pane active" id="festival-2015" role="tabpanel" aria-labelledby="gallery1-tab">
                    <?php
                    echo "<div class='masonry'>";

                    for ($i = 0; $i < 19; $i++)
                    {
                        echo "<div class='item'>"
                        . "  <img style='width:250px' src='res/festival/2015/gallery/$i.jpg' alt='Festival 2015 img. $i'/>"
                        . "</div>";
                    }

                    echo "</div>";
                    ?>
                </div>

                <!-- DEMO -->
                <div class="tab-pane" id="festival-2017" role="tabpanel" aria-labelledby="gallery2-tab">
                    <?php
                    echo "<div class='masonry'>";

                    for ($i = 0; $i < 19; $i++)
                    {
                        echo "<div class='item'>"
                        . "  <img style='width:250px;' src='res/festival/2017/gallery/$i.jpg' alt='Festival 2017 img. $i'/>"
                        . "</div>";
                    }

                    echo "</div>";
                    ?>
                </div>


            </div>
        </div>
    </section>
</div>

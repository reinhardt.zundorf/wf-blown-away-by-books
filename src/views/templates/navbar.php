<?php
/* ============================================================ *
 * Web Folders CMS                                              *
 * Navbar File                v1.0                              *
 * ------------------------------------------------------------ *
 * Contains HTML markup for navbar.                             *
 * ------------------------------------------------------------ */
?>

<nav class="navbar nav">

</nav>


<!-- <div class=""> -->
<!-- <div class="container-fluid"> -->
<nav class="navbar-nav ml-auto">

    <a class="navbar-brand">
        <img src="res/brand/brand-logo-md.png"/>
    </a>

    <ul class="nav nav-tabs">

        <li class="nav-item">
            <a id="nav-home" class="nav-link" href="index.php?action=home">Home</a>
        </li>

        <li class="nav-item dropdown">
            <a id="nav-about" class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>About</a>
            <!-- <a id="nav-festival" class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Festival</a> -->
            <div class="dropdown-menu" aria-labelledby="nav-about">
                <a id="nav-about-us" href="index.php?action=about" class="dropdown-item">About us</a>
                <a id="nav-what-we-do" href="index.php?action=about&type=services" class="dropdown-item">What we do</a>
                <a id="nav-contact" href="index.php?action=contact" class="dropdown-item">Get in touch</a>
                <a id="nav-sponsors" href="index.php?action=about&type=sponsors" class="dropdown-item">Our sponsors</a>
            </div>
        </li>

        <li class="nav-item dropdown">
            <a id="nav-festival" class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Festival</a>
            <div class="dropdown-menu" aria-labelledby="nav-festival">
                <!-- <a id="nav-festival-gallery" href="index.php?action=festival&type=gallery" class="dropdown-item">Gallery</a> -->
                <a id="nav-festival-about" href="index.php?action=festival&type=about" class="dropdown-item">About</a>
                <a id="nav-festival-info" href="index.php?action=festival&type=info" class="dropdown-item">Info</a>
                <a id="nav-festival-calendar" href="index.php?action=festival&type=calendar" class="dropdown-item">Programme</a>
                <a id="nav-archive" href="index.php?action=festival&type=gallery" class="dropdown-item">Past Festivals</a>
            </div>
        </li>

        <li class="nav-item dropdown">
            <a id="nav-books" class="nav-link" href="" id="dropdownBooks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Books</a>
            <div class="dropdown-menu" aria-labelledby="dropdownBooks">
                <a id="nav-books-featured" href="index.php?action=books&type=about" class="dropdown-item">Latest Books</a>
                <a id="nav-books-authors" href="index.php?action=books&type=info" class="dropdown-item">Featured Authors</a>
                <a id="nav-books-events" href="index.php?action=books&type=calendar" class="dropdown-item">Events</a>
            </div>
        </li>
        <li class="nav-item"><a id="nav-film" class="nav-link" href="index.php?action=film">Film</a></li>
        <li class="nav-item"><a id="nav-blog" class="nav-link" href="index.php?action=blog">Blog</a></li>
        <li class="navbar-text ml-auto">
            Brought to you by <a href="">The Friends of the Fish Hoek Library</a>
        </li>
    </ul>
    
</nav>
<!-- </navbar> -->


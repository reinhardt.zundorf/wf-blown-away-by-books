<?php
/* ==================================================== *
 * Web Folders CMS                                      *
 * ---------------------------------------------------- *
 * Type:        View                                    *
 * Name:        Gallery                                 *
 * Date:        06/11/2017                              *
 * ---------------------------------------------------- */
require_once("./conf/init.php");
$view_title = "calendar";
?>

<!--<link href='../fullcalendar.print.min.css' rel='stylesheet' media='print' />-->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" /> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" /> -->
<!-- <link href='css/sections/calendar-1.0.css' rel='stylesheet' /> -->

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script> -->
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.1/fullcalendar.min.js"></script>  -->
<!-- <script src="/shared/js/bootstrapmodal.min.js"></script> -->

<!--<div style="padding-top:5px;" class="landing-message">
   <header class="container text-center">
    <h1 class="text-center">Calendar</h1>
    </header>
</div>-->

<br/>

<div class="container" style="background-color: rgba(255, 255, 255, 0.5);" id="calendar"></div>

<script>
    $(function ()
    {

        /* initialize the external events
         -----------------------------------------------------------------*/
        function init_events(ele)
        {
            ele.each(function ()
            {
                var eventObject = { title: $.trim($(this).text()) };

                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({ 
                    zIndex:                 1070,
                    revert:                 true, // will cause the event to go back to its
                    revertDuration:         0  //  original position after the drag
                });
            });
        };

        init_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear()
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'year, month'
            },
            buttonText: {
                today: 'today',
                month: 'month'
            },
            //Random default events
//            events: [
//                {
//                    title: 'CTMA Junior League 7',
//                    start: new Date(y, m, d, 18, 1, 18),
//                    backgroundColor: '#09148a', //red
//                    borderColor: '#09148a' //red
//                },
//                {
//                    title: 'CTMA Junior Invitational Champs (old Jack Currie)',
//                    start: new Date(y, m, d - 5),
//                    end: new Date(y, m, d - 2),
//                    backgroundColor: '#f39c12', //yellow
//                    borderColor: '#f39c12' //yellow
//                },
//                {
//                    title: 'CTMA Qualifying Gala III',
//                    start: new Date(y, m, d, 10, 30),
//                    allDay: false,
//                    backgroundColor: '#0073b7', //Blue
//                    borderColor: '#0073b7' //Blue
//                }
//            ],
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject')

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject)

                // assign it the date that was reported
                copiedEventObject.start = date
                copiedEventObject.allDay = allDay
                copiedEventObject.backgroundColor = $(this).css('background-color')
                copiedEventObject.borderColor = $(this).css('border-color')

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove()
                }

            }
        })

        /* ADDING EVENTS */
        var currColor = '#3c8dbc' //Red by default
        //Color chooser button
        var colorChooser = $('#color-chooser-btn')
        $('#color-chooser > li > a').click(function (e) {
            e.preventDefault()
            //Save color
            currColor = $(this).css('color')
            //Add color effect to button
            $('#add-new-event').css({'background-color': currColor, 'border-color': currColor})
        })
        $('#add-new-event').click(function (e) {
            e.preventDefault()
            //Get value and make sure it is not null
            var val = $('#new-event').val()
            if (val.length == 0) {
                return
            }

            //Create events
            var event = $('<div />')
            event.css({
                'background-color': currColor,
                'border-color': currColor,
                'color': '#fff'
            }).addClass('external-event')
            event.html(val)
            $('#external-events').prepend(event)

            //Add draggable funtionality
            init_events(event)

            //Remove event from text input
            $('#new-event').val('')
        })
    })

//    $(document).ready(function () {
//        $('#calendar').fullCalendar({
//            events: '/hackyjson/cal/',
//            header: {
//                left: '',
//                center: 'prev title next',
//                right: ''
//            },
//            eventClick: function (event, jsEvent, view) {
//                $('#modalTitle').html(event.title);
//                $('#modalBody').html(event.description);
//                $('#eventUrl').attr('href', event.url);
//                $('#fullCalModal').modal();
//            }
//        });
//    });
</script>



<!--
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary"><a id="eventUrl" target="_blank">Event Page</a></button>
            </div>
        </div>
    </div>
</div>-->

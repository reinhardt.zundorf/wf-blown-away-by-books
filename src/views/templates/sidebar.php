<!-- SIDEBARS -->
<div class="col-lg-3 col-md-10 col-sm-12">
    <!-- <div class="sidebars">     -->

    <!-- SEARCH -->
<!--    <article class="sidebar sidebar-search">
        <form class="form-inline">
            <input class="form-control" type="search" placeholder="..." aria-label="Search">
            <button class="btn btn-info" type="submit"><i class="fa fa-search"></i></button>
        </form> 
    </article>-->

    <!-- MAINTENANCE MESSAGE -->
    <article class="sidebar home-maintenance">
        <header class="sidebar-header">
            <h3 class="h3"><i class="fa fa-lg fa-warning"></i>Website Maintenance</b></h3>
        </header>
        <section class="sidebar-body">
             <p class="p">Please contact us at <a href="mailto:info@webfolders.co.za">info@webfolders.co.za</a> for any queries.</p>
        </section>
    </article>


    <!-- FESTIVAL -->
    <article class="sidebar sidebar-festival">
        <header class="sidebar-header">
            <h3 class="h3"><i class="sidebar-icon"></i>Read about the Festival</h3>
        </header>
        <section class="sidebar-body">
            <p class="p">Our annual <i>Blown Away By Books</i> Festival coincides with 
                <a href="index.php?action=festival&type=national_library_week">National Library Week</a> 
                each year and involves events at the libraries of Fish Hoek, Masiphumelele, Simon’s Town, 
                Kommetjie and Ocean View, allowing the programmes and audiences to be more diverse.</p>
            <a href="ndex.php?action=festival&type=info">Read more</a>
        </section>
    </article>

     <!-- BLOG POSTS --> 
    <article class="sidebar sidebar-subscribe">
        <header class="sidebar-header">
            <h3 class="h3"><i class="sidebar-icon"></i>Read our blog</h3>
        </header>
        <div class="sidebar-body">
            <?php 
                $posts = $view["articles"];
                $count = 0;

                foreach($posts["results"] as $post)
                {
                    echo "<section>
                            <span>
                                <h4><a href='index.php?action=blog&id={$post->id}'>{$post->title}</a></h4>
                                <small>{$post->publicationDate}</small>
                            </span>
                            <p>{$post->summary}</p>
                          </section>";

                    $count++;
                    if($count === 1)
                    {
                        break;
                    }
                }
            ?>
        </div>
    </article>

    <article class="sidebar sidebar-facebook">
        <header class="sidebar-header">
            <h3 class="h3"><i class="sidebar-icon"></i>News on Facebook</h3>
        </header>
        <!--<div class="sidebar-body">-->
        <!--<div class="form-text">-->
        <div class="fb-like-box" data-href="https://www.facebook.com/BlownAwayByBooks" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="true" data-show-border="false" data-height="200px" data-width="264"></div>
        <!--</div>-->
        <!--</div>-->
    </article>


    <!-- MAP -->
    <article class="sidebar sidebar-map">
        <header class="sidebar-header">
            <h3 class="h3">Where to find us</h3>
        </header>
        <section class="sidebar-body">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13209.506088086286!2d18.426808!3d-34.136708!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcef68b2b6fef69d8!2sFish+Hoek+Public+Library!5e0!3m2!1sen!2sza!4v1529064375526" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
            <p>We call the Fish Hoek Public Library 'home'. Come visit us next to the Fish Hoek Civic Centre, Central Circle Rd, Fish Hoek.</p>
        </section>
    </article>

</div>
</div>

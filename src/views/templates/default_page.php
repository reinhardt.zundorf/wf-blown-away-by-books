<main>

    <!-- DESTINATION TEMPLATE -->
    <div class="default-section">
        <div class="destination-hero" style="background-image: url(<?php echo $view["hero"]; ?>);">
            <div class="destination-hero-text">
                <div class="destination-hero-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="h1">Destination | <text style="font-weight:500;"><?php echo $view["title"] ?></text></h1>
                                <h2 style="font-weight:500;">Read more about <?php echo ucwords($view["heading"]); ?> in False Bay. <br>Explore activities and things to do in the area by viewing our online catalogue:</h2>
                                <a class="btn btn-success" href="index.php?action=catalogue&type=activity&name=<?php echo $view["name"]; ?>">Browse catalogue</a>
                            </div>
                            <div class="col-lg-4 offset-2">
                                <!--<img class="img-responsive" src="res/destinations/maps/map-jubilee-square.png" alt="Jubilee Square Map"/>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include "./views/templates/breadcrumbs.php"; ?>

    <section class="default-section">
        <div class="container">
            <div class="row">
                <div class="col-md-10">

                    <!-- DESTINATION: Content -->
                    <article class="activity-main">

                        <!-- DESTINATION: Header -->
    <!--                    <section class="activity-header">
                            <div class="row">
                                <h1 class="h1"><?php echo $view["title"] ?></h1>
                            </div>
                            <br>
                        </section>-->

                        <!-- DESTINATION: Content -->
                        <section class="page-content" id="bg-white">
                            <?php
                            if ($view["text"] != "")
                            {
//                                echo "<br>";
                                echo $view["text"];
                            }
                            else
                            {
//                        echo "<section class='page-content' id='bg-white'>";
                                echo "<h2 class='h2'>This page is currently undergoing maintenance. </h2>";
                                echo "<br><p>If you believe this is a mistake or would like to contact us anyway,<br> please"
                                . " send us an email to <a href='mailto:info@falsebaytours.com'>info@falsebaytours.com</a><br><br></p>";
                            }
                            ?>
                        </section>

                    </article>
                </div>

                <!-- DESTINATIONS MENU -->
                <div class="col-lg-2">
                    <aside class="destination-sidebar">
                        <div class="page-content" id="bg-white">
                            <h3>Explore destinations</h3>
                            <ul class="list-unstyled">
                                <?php
                                $dest_list = $view["destinations"];

                                if (isset($dest_list))
                                {
                                    foreach ($dest_list["results"] as $destination)
                                    {
                                        $lc_name = strtolower($destination->name);
                                        $temp    = str_replace(" ", "_", $lc_name);
                                        
                                        echo "<li class='list-item'><a href='index.php?action=destination&type=$temp'>$destination->name</a></li>";
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>


</main>

<script>
    $(document).ready(function ()
    {
        $("#home-destinations-more").hide();
    });

    $(document).ready(function ()
    {
        $("#button-destinations").click(function ()
        {
            $("#home-destinations-more").show();
            $("#button-destinations").hide();
        });
    });
</script>

<main id="bg-neutral-1">
    <div class="container-fluid page-content">
        <div class="container">
            <div class="card-columns">

                <a href="?action=destination&type=kalk_bay" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-kalk-bay"></div>
                        <div class="card-footer">
                            <h1 class="h1">Kalk Bay</h1>
                        </div>
                    </div>
                </a>

                <a href="?action=destination&type=jubilee_square" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-jubilee-square"></div>
                        <div class="card-footer">
                            <h1 class="h1">Jubilee Square</h1>
                        </div>
                    </div>
                </a>

                <a href="?action=destination&type=cape_point_ostrich_farm" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-ostrich-farm"></div>
                        <div class="card-footer">
                            <h1 class="h1">Cape Point Ostrich Farm</h1>
                        </div>
                    </div>
                </a>

                <a href="?action=destination&type=cape_point" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-cape-point"></div>
                        <div class="card-footer">
                            <h1 class="h1">Cape Point</h1>
                        </div>
                    </div>
                </a>

                <!--</div>-->


                <a href="?action=destination&type=millers_point" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-millers-point"></div>
                        <div class="card-footer">
                            <h1 class="h1">Miller's Point</h1>
                        </div>
                    </div>
                </a>

                <a href="?action=destination&type=boulders_beach" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-boulders-beach"></div>
                        <div class="card-footer">
                            <h1 class="h1">Boulders Beach</h1>
                        </div>
                    </div>
                </a>

            </div>

            <div class="card-columns" style="display:none;" id="home-destinations-more">

                <a href="?action=destination&type=scratch_patch_mineral_world" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-scratch-patch"></div>
                        <div class="card-footer"><h1 class="h1">Scratch Patch Mineral World</h1></div>
                    </div>
                </a>

                <a href="?action=destination&type=cheetah_outreach" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-cheetah-outreach"></div>
                        <div class="card-footer"><h1 class="h1">Cheetah Outreach</h1></div>
                    </div>
                </a>

                <a href="?action=destination&type=monkey_town" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-monkey-town"></div>
                        <div class="card-footer"><h1 class="h1">Monkey Town</h1></div>
                    </div>
                </a>

                <a href="?action=destination&type=kogelbaai" class="card-wrapper">
                    <div class="card card-activity">
                        <div class="card-body" id="home-destination-kogelbaai"></div>
                        <div class="card-footer"><h1 class="h1">Kogelbaai Nature Reserve</h1></div>
                    </div>
                </a>

            </div>

            <div class="row text-center justify-content-center">
                <button id="button-destinations" class="btn btn-lg btn-expand">Show more</button>
            </div>

            <?php
            /* ================================================= *
             * Get all destinations from the DB                  *
             * ------------------------------------------------- */

//            if($view["destinations"])
//            {
//                $list = $view["destinations"];
//
//                $count = 0;
//
////                foreach($view["destinations"] as $item)
//                while ($count != 5)
//                {
////                    $temp = ucwords($item);
//
//                    echo "<div class='card'>" .
//                    "  <div class='card-img'><img src='http://via.placeholder.com/350x225'/></div>" .
//                    "  <div class='card-footer'><h1>Destination $count</h1></div>" .
//                    "</div>";
//
//                    $count++;
//                }
//            }
            ?>
            <!--<br>-->
        </div>
    </div>
</div>

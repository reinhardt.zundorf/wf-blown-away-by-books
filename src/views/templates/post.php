<!DOCTYPE html>
<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->

<?php
/* ==================================================== *
 * Get post from view structure into $post              *
 * ---------------------------------------------------- */
$post = $view["post"];
?>


<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-9">

            <!-- Title -->
            <h1 class="mt-4"><?php echo $post->title; ?></h1>

            <!-- Author -->
            <p class="lead">
                <?php echo $post->posterId; ?>
                <a href="#"></a>
            </p>

            <hr>

            <!-- Date/Time -->
            <p><?php echo $post->publicationDate;?></p>

            <hr>

            <!-- Preview Image -->
            <img class="img-fluid rounded" src="res/blog/cover/<?php echo $post->id; ?>.jpg" alt="Cover image for post ID <?php echo $post->id; ?>">

            <hr>

            <!-- Post Content -->
            <p class="lead"><?php echo $post->contentLead ;?></p>

            <?php echo $post->contentBody; ?>
            
            
<!--            <blockquote class="blockquote">
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                <footer class="blockquote-footer">Someone famous in
                    <cite title="Source Title">Source Title</cite>
                </footer>
            </blockquote>-->

            <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>-->

            <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>-->

            <hr>

            <!-- Comments Form -->
            <div class="card my-4">
                <h5 class="card-header">Leave a Comment:</h5>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>

            <!-- Single Comment -->
            <div class="media mb-4">
                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                    <h5 class="mt-0">Commenter Name</h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
            </div>

            <!-- Comment with nested comments -->
            <div class="media mb-4">
                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                    <h5 class="mt-0">Commenter Name</h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.

                    <div class="media mt-4">
                        <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Commenter Name</h5>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        </div>
                    </div>

                    <div class="media mt-4">
                        <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Commenter Name</h5>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-3">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <!-- Categories Widget -->
            <div class="card  my-4">
                <h5 class="card-header">Tagged in</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="post-tags list-unstyled mb-0">
                                <?php
                                    $tags = explode(", ", $post->tags);
                                    $count = count($tags);
                                    
                                    for($x = 0; $x <= $count; $x++) 
                                    {
                                        echo "<li><a href='index.php?action=blog&tag=$x'>$tags[$x]</a></li>";
                                    } 
                                ?>
                            </ul>
                        </div>
<!--                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#">CSS</a>
                                </li>
                                <li>
                                    <a href="#">Tutorials</a>
                                </li>
                            </ul>
                        </div>-->
                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Side Widget</h5>
                <div class="card-body">
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!--
<article class="main-article post tag-polygon">
    <header>
        <div class="featured-image cover" style="background: url('res/blog/posts/covers/<?php echo $post->id; ?>.jpg');">
            <div class="info">
                <h1 class="title large white"><?php echo ucwords($post->title); ?></h1>
            </div>
            <div class="tags">
                <a href=""><?php echo "Books"; ?></a>
            </div>
        </div>
        <div class="meta">
            <div class="left grid-60 grid-parent">
                <time datetime="2018-04-20"><i class="fa fa-clock-o"></i>Apr 20, 2018</time>
                <a href="#comments" class="comments tooltip" title="Go to comments"><i class="fa fa-comment"></i> <span class="disqus-comment-count" data-disqus-url="http://ghost.estudiopatagon.com/reco/reco/various-nature-polygons/" data-disqus-identifier="5ad9325716f0910d1452213a">0</span> Comments</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="share hide-on-desktop hide-on-tablet">
            <a class="whatsapp hide-on-tablet hide-on-desktop" href="whatsapp://send?text=http://ghost.estudiopatagon.com/reco/various-nature-polygons/" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i></a>
            <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=http://ghost.estudiopatagon.com/reco/various-nature-polygons/"
               onclick="window.open(this.href, 'facebook-share', 'width=580,height=296');return false;"><i class="fa fa-facebook"></i></a>
            <a class="twitter" href="http://twitter.com/share?text=Deer%20head%20polygon&url=http://ghost.estudiopatagon.com/reco/various-nature-polygons/"
               onclick="window.open(this.href, 'twitter-share', 'width=550,height=235');return false;"><i class="fa fa-twitter"></i></a>
            <a class="googleplus" href="https://plus.google.com/share?url=http://ghost.estudiopatagon.com/reco/various-nature-polygons/"
               onclick="window.open(this.href, 'google-plus-share', 'width=490,height=530');return false;"><i class="fa fa-google-plus"></i></a>
        </div>
    </header>
    <section class="post-content">
        <div class="text">
            <div class="kg-card-markdown">
                <h2 id="producttitledeerhead">Product Title: DEER HEAD</h2>
                <p>You receive pre-cut and pre-folded paper sections that you just have to stick together.</p>
                <p>Glue in the kit.<br>
                    Instruction in English in the kit.</p>
                <p>Tired of sad pictures on the walls? Then this &quot;DEER&quot; is just the right thing for you.<br>
                    The &quot;DEER&quot; is made from high quality environmentally friendly 250g/m² paper.</p>
                <p>The assembly of the model does not require special qualification. You just have to glue the numbered pieces with the same numbers. It's easy enough!</p>
                <p>The number of color solutions is large enough. Soon, I'll add all the matching colors.</p>
                <p>
                    Recommended age: 12+<br>
                    Difficulty: medium-hard<br>
                    Paper: luxury Italian paper 290g/m²</p>
                <h3 id="designedbypaperraz">Designed by Paperraz</h3>
                <p>
                    Height / Width / Depth<br>
                    55cm / 30cm / 40cm<br>
                    33&quot; /12&quot; /16&quot;
                </p>
            </div>
        </div>
    </section>
</article>

</div>-->

<!-- CONTACT FORM -->
<form id="contact-form" method="post" action="index.php?action=email" role="form">
    <div class="messages"></div>
    <div class="controls">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name *</label>
                    <input id="name" type="text" name="name" autocomplete="name" class="form-control" placeholder="Please enter your first name *" required="required" data-error="Firstname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="surname">Surname </label>
                    <input id="surname" type="text" name="surname" autocomplete="family-name" class="form-control" placeholder="Please enter your last name *" data-error="Lastname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email *</label>
                    <input id="email" type="email" name="email" autocomplete="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input id="phone" type="tel" name="phone" class="form-control" autocomplete="tel-national" placeholder="Please enter your phone number">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="form_message">Message *</label>
                    <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Send message"><?php
                if (isset($responseArray))
                {
                    if ($responseArray["type"] === "succcess")
                    {
                        echo "Your message has successfully been sent.";
                    }
                    else if ($responseArray["type"] === "danger")
                    {
                        echo "Your message has not been sent. We apologize for the inconvenience.";
                    }

                    if ($responseArray["message"])
                    {
                        echo $responseArray["message"];
                    }
                }
                ?>
            </div>
        </div>
    </div>
</form>

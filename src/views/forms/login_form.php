<!-- LOGIN -->
<main style="">
    <div class="login-box" >

        <div class="login-box-header" id="bg-neutral-">
            <h1 class="h2">Login</h1>
        </div>

        <div class="login-box-body" id="bg-white">
            <form action="index.php?action=login" class="form-horizontal" method="post">
                <input type="hidden" name="login" value="true"/>
                <div class="input-group has-feedback">
                    <input class="form-control" type="text" name="username" id="username" aria-label="Text input" placeholder="Username" required maxlength="50" autocomplete='username' />
                </div>
                <div class="input-group has-feedback">
                    <input class="form-control" type="text" name="password" id="password" aria-label="Text input" placeholder="Password" required maxlength="50" autocomplete='password' />
                </div>
                <div class="container">
                    <button class="btn btn-block btn-login" type="submit" name="profile">Login</button> 
                </div>
            </form>
        </div>

        <div class="login-box-footer">
            <p>I have <a href="index.php?action=forgotten_password">forgotten</a> my password.</p>
            <p><a href="index.php?action=register">Register</a> one now.</p>
        </div>

        <?php
        if (isset($results['errorMessage']))
        {
            echo "<div class='login-box-msg-error'>{$results['errorMessage']}</div>";
        }
        ?>

    </div>

    <br>

</main>

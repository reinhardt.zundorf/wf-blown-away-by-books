<!DOCTYPE html>
<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->

<!-- CONTACT PERSON FORM -->
<div class="form-register">
    <div class="login">
        <p class="login-box-text">Welcome! <br>Please register a False Bay Tours Account to proceed with Partner Subscription</p>

        <!-- FORM -->
        <form action="index.php?action=register_profile" class="form-horizontal" method="post">
            <input type="hidden" name="login" value="true" />
            <div class="container-fluid">

                <!-- EMAIL & CONTACT -->
                <div class="row">
                    <div class="form-group has-feedback">
                        <input class="form-control" type="text" name="email" id="email" placeholder="Your email address" required autofocus maxlength="50"/>
                    </div>
                    <div class="form-group has-feedback">
                        <input class="form-control" type="text" name="contact" id="contact" placeholder="Your contact number" required autofocus maxlength="11"/>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group has-feedback">
                        <input class="form-control" type="password" name="password" id="password" placeholder="Your password" required maxlength="20"/>
                    </div>
                    <div class="form-group has-feedback">
                        <input class="form-control" type="password" name="password_repeat" id="password_repeat" placeholder="Repeat your password" required maxlength="20"/>
                    </div>
                </div>

                <div class="row">
                    <div class="buttons">
                        <button class="btn btn-info" type="submit" name="">Next <i class="fa fa-arrow-right"></i></button> 
                        <button class="btn btn-success" type="submit" name="register_profile">Next <i class="fa fa-arrow-right"></i></button> 
                    </div>
                </div>

                <div class="row">
                    <div class="text-center">
                        Already have an account? Login <a href="#">here</a> | <a href="#">Contact Support</a> 
                    </div>
                </div>

            </div>

        </form>

        <?php
        if (isset($results['errorMessage']))
        {
            echo "<div class='login-box-msg-error'>{$results['errorMessage']}</div>";
        }
        ?>
    </div>
</div>




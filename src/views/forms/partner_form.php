<!-- REGISTER -->
<div class="register-box">

    <div class="register-box-header" id="bg-base-1">
        <img class="register-box-brand" src="res/brand/brand-text-box.png" alt="False Bay Tours"/>
    </div>

    <div class="register-box-body">

        <!-- FORM HEADER -->
        <div class="register-box-text">
            <h2>Please enter your details to register a False Bay Tours profile:</h2>
        </div>

        <!-- FORM -->
        <form action="index.php?action=register&type=member" class="form-horizontal" method="post">
            <input type="hidden" name="login" value="true" />

            <div class="row">
                <div class="input-group has-feedback">
                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                    <input class="form-control" type="text" name="name" id="member_firstname" aria-label="Text input" placeholder="First name" required maxlength="50" />
                </div>
                <div class="input-group has-feedback">
                    <span class="input-group-addon"><i class="fa fa-group"></i></span>
                    <input class="form-control" type="text" name="lastname" id="member_lastname" aria-label="Text input" placeholder="Last name" required maxlength="50" />
                </div>
            </div>



            <div class="input-group has-feedback">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input class="form-control" type="text" name="email" id="member_email" aria-label="Text input" placeholder="Email address" required autofocus maxlength="200" />
            </div>

            <div class="input-group has-feedback">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input class="form-control" type="text" name="cellphone" id="member_cellphone" aria-label="Text input" placeholder="Cellphone number" autofocus maxlength="13" />
            </div>

            <div class="input-group has-feedback">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                <input class="form-control" type="password" name="password" id="member_password" aria-label="Text input for password" placeholder="Password" required autofocus maxlength="50"/>
            </div>

            <div class="form-group">
                <label class="checkbox" for="checkbox1">
                    <input type="checkbox" data-toggle="checkbox" value="" id="checkbox1" required>
                    I have read and agree to the Terms of Service
                </label>
            </div>

            <div class="buttons text-center">
                <button class="btn btn-lg btn-success" type="submit" name="profile">Proceed</button> 
            </div>
        </form>
    </div>

    <!-- REGISTER: Footer -->
    <div class="register-box-footer">
        <div class="text-center">
            <p class="p">False Bay Tours will never exchange your personal information with a 3rd party, please read our Privacy Policy for more information.</p>
            <p class="p">Already have an account? Click here to login</p>
        </div>
    </div>

</div>




<?php
if (isset($results['errorMessage']))
{
    echo "<div class='login-box-msg-error'>{$results['errorMessage']}</div>";
}
?>


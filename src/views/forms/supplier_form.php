<!DOCTYPE html>
<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->

<main>

    <!-- ABOUT US -->
    <section class="default-section" style="margin-top:0px;margin-bottom:0px;">
        <section class="activity-header" style="margin-bottom:15px;">
            <div class="row">
                <div class="container">
                    <h1 class="h1">Partners | <text class="h1" style="margin-left:7px;font-weight:500;">Subscribe</text></h1>
                </div>
            </div>
        </section>
        <section class="page-content" id="bg-white">
            <div class="row">
                <div class="container">

                    <!-- FORM -->
                    <!--<form action="index.php?action=register_profile" class="form-horizontal" method="post">-->
                    <form class="form"  method="post" name="supplier_form" id="supplier_form" accept-charset="utf-8" action="index.php?action=subscribe">
                        <input type="hidden" name="formID" value="72321201323942" />
                        <div class="form-all">
                            <ul class="form-section page-section">
                                <li id="cid_1" class="form-input-wide" data-type="control_head">
                                    <div class="form-header-group ">
                                        <div class="header-text httal htvam">
                                            <h2 id="header_1" class="form-header" data-component="header">
                                                Partner Subscription
                                            </h2>
                                            <div id="subHeader_1" class="form-subHeader">
                                                Subscribe to add your Activities &amp; Destinations to our Catalogue.
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_fullname" id="id_6">
                                    <label class="form-label form-label-top form-label-auto" id="label_6" for="first_6"> Your Name </label>
                                    <div id="cid_6" class="form-input-wide">
                                        <div data-wrapper-react="true">
                                            <span class="form-sub-label-container" style="vertical-align:top">
                                                <input type="text" id="first_6" name="q6_yourName[first]" class="form-textbox" size="10" value="" data-component="first" />
                                                <label class="form-sub-label" for="first_6" id="sublabel_first" style="min-height:13px"> Business Name </label>
                                            </span>
                                            <span class="form-sub-label-container" style="vertical-align:top">
                                                <input type="text" id="last_6" name="q6_yourName[last]" class="form-textbox" size="15" value="" data-component="last" />
                                                <label class="form-sub-label" for="last_6" id="sublabel_last" style="min-height:13px"> Website</label>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_chargify" id="id_5">
                                    <label class="form-label form-label-top form-label-auto" id="label_5" for="input_5"> My Products </label>
                                    <div id="cid_5" class="form-input-wide">
                                        <div data-wrapper-react="true">
                                            <div data-wrapper-react="true">
                                                <input type="hidden" name="simple_fpc" data-payment_type="chargify" data-component="payment1" value="5" />
                                                <input type="hidden" name="payment_total_checksum" id="payment_total_checksum" data-component="payment2" />
                                                <div data-wrapper-react="true">
                                                    <input type="hidden" id="payment_enable_lightbox" />
                                                    <span class="form-product-item hover-product-item">
                                                        <div data-wrapper-react="true" class="form-product-item-detail">
                                                            <input type="radio" class="form-radio " id="input_5_1000" name="q5_myProducts[][id]" value="1000" />
                                                            <label for="input_5_1000" class="form-product-container">
                                                                <span data-wrapper-react="true">
                                                                    <span class="form-product-name" id="product-name-input_5_1000">
                                                                        Partner Subscription Monthly
                                                                    </span>
                                                                    <span class="form-product-details">
                                                                        <span data-wrapper-react="true">
                                                                            (
                                                                            <b>
                                                                                <span data-wrapper-react="true">
                                                                                    R
                                                                                    <span id="input_5_1000_price">
                                                                                        450.00
                                                                                    </span>
                                                                                </span>
                                                                            </b>
                                                                            <span data-wrapper-react="true">
                                                                                for each
                                                                                <u>
                                                                                    month</u>.)</span>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </span>
                                                    <br/>
                                                    <span class="form-product-item hover-product-item">
                                                        <div data-wrapper-react="true" class="form-product-item-detail">
                                                            <input type="radio" class="form-radio " id="input_5_1001" name="q5_myProducts[][id]" value="1001" />
                                                            <label for="input_5_1001" class="form-product-container">
                                                                <span data-wrapper-react="true">
                                                                    <span class="form-product-name" id="product-name-input_5_1001">
                                                                        Partner Subscription 12 Months (annually renewed)
                                                                    </span>
                                                                    <span class="form-product-details">
                                                                        <span data-wrapper-react="true">
                                                                            (
                                                                            <b>
                                                                                <span data-wrapper-react="true">
                                                                                    R
                                                                                    <span id="input_5_1001_price">
                                                                                        4650.00
                                                                                    </span>
                                                                                </span>
                                                                            </b>
                                                                            <span data-wrapper-react="true">
                                                                                for each
                                                                                <u>
                                                                                    year</u>.)</span>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </span>
                                                    <br/>
                                                    <span class="form-payment-total">
                                                        <b>
                                                            <span id="total-text">
                                                                Total
                                                            </span>

                                                            <span class="form-payment-price">
                                                                <span data-wrapper-react="true">
                                                                    R
                                                                    <span id="payment_total">
                                                                        0.00
                                                                    </span>
                                                                </span>
                                                            </span>
                                                        </b>
                                                    </span>
                                                </div>
                                            </div>
                                            <div>
                                                <table class="form-address-table payment-form-table" style="border:0" cellPadding="0" cellSpacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_email" id="sublabel_cc_email" style="min-height:13px;margin:0 0 3px 0"> E-mail </label>
                                                                    <input type="email" id="input_5_cc_email" name="q5_myProducts[cc_email]" class="form-textbox cc_email" size="20" value="" data-component="cc_email" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="form-address-table payment-form-table" style="border:0" cellPadding="0" cellSpacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <th colSpan="2" style="text-align:left" id="ccTitle5">
                                                                Credit Card
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_firstName" id="sublabel_cc_firstName" style="min-height:13px;margin:0 0 3px 0"> First Name </label>
                                                                    <input type="text" id="input_5_cc_firstName" name="q5_myProducts[cc_firstName]" class="form-textbox cc_firstName" size="20" value="" data-component="cc_firstName" />
                                                                </span>
                                                            </td>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_lastName" id="sublabel_cc_lastName" style="min-height:13px;margin:0 0 3px 0"> Last Name </label>
                                                                    <input type="text" id="input_5_cc_lastName" name="q5_myProducts[cc_lastName]" class="form-textbox cc_lastName" size="20" value="" data-component="cc_lastName" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_number" id="sublabel_cc_number" style="min-height:13px;margin:0 0 3px 0"> Credit Card Number </label>
                                                                    <input type="number" id="input_5_cc_number" name="q5_myProducts[cc_number]" class="form-textbox cc_number" autoComplete="off" size="20" value="" data-component="cc_number" />
                                                                </span>
                                                            </td>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_ccv" id="sublabel_cc_ccv" style="min-height:13px;margin:0 0 3px 0"> Security Code </label>
                                                                    <input type="number" id="input_5_cc_ccv" name="q5_myProducts[cc_ccv]" class="form-textbox cc_ccv" autoComplete="off" style="width:52px" value="" data-component="cc_ccv" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_exp_month" id="sublabel_cc_exp_month" style="min-height:13px;margin:0 0 3px 0"> Expiration Month </label>
                                                                    <select class="form-dropdown cc_exp_month" name="q5_myProducts[cc_exp_month]" id="input_5_cc_exp_month" data-component="cc_exp_month">
                                                                        <option>  </option>
                                                                        <option value="January"> January </option>
                                                                        <option value="February"> February </option>
                                                                        <option value="March"> March </option>
                                                                        <option value="April"> April </option>
                                                                        <option value="May"> May </option>
                                                                        <option value="June"> June </option>
                                                                        <option value="July"> July </option>
                                                                        <option value="August"> August </option>
                                                                        <option value="September"> September </option>
                                                                        <option value="October"> October </option>
                                                                        <option value="November"> November </option>
                                                                        <option value="December"> December </option>
                                                                    </select>
                                                                </span>
                                                            </td>
                                                            <td width="50%">
                                                                <span class="form-sub-label-container" style="vertical-align:top">
                                                                    <label class="form-sub-label" for="input_5_cc_exp_year" id="sublabel_cc_exp_year" style="min-height:13px;margin:0 0 3px 0"> Expiration Year </label>
                                                                    <select class="form-dropdown cc_exp_year" name="q5_myProducts[cc_exp_year]" id="input_5_cc_exp_year" data-component="cc_exp_year">
                                                                        <option>  </option>
                                                                        <option value="2018"> 2018 </option>
                                                                        <option value="2019"> 2019 </option>
                                                                        <option value="2020"> 2020 </option>
                                                                        <option value="2021"> 2021 </option>
                                                                        <option value="2022"> 2022 </option>
                                                                        <option value="2023"> 2023 </option>
                                                                        <option value="2024"> 2024 </option>
                                                                        <option value="2025"> 2025 </option>
                                                                        <option value="2026"> 2026 </option>
                                                                        <option value="2027"> 2027 </option>
                                                                    </select>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_textbox" id="id_4">
                                    <label class="form-label form-label-top form-label-auto" id="label_4" for="input_4"> Enter your affiliate link: </label>
                                    <div id="cid_4" class="form-input-wide">
                                        <span class="form-sub-label-container" style="vertical-align:top">
                                            <input type="text" id="input_4" name="q4_enterYour" data-type="input-textbox" class="form-textbox" size="20" value="" data-component="textbox" />
                                            <label class="form-sub-label" for="input_4" style="min-height:13px"> Must be in http:// format </label>
                                        </span>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_button" id="id_2">
                                    <div id="cid_2" class="form-input-wide">
                                        <div style="margin-left:156px" class="form-buttons-wrapper">
                                            <button id="input_2" type="submit" class="form-submit-button" data-component="button">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </li>
                                <li style="display:none">
                                    Should be Empty:
                                    <input type="text" name="website" value="" />
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </section>
</main>

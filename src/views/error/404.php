<!DOCTYPE html>
<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<script>
    /* ================================================== *
     * FullCalendar: JSON feed from PHP script            *
     * -------------------------------------------------- */
    /* initialize the external events
         -----------------------------------------------------------------*/
     $(function ()
    {
    
    function init_events(ele)
    {
            ele.each(function ()
            {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                }

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject)

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                })

            })
        }

//        init_events($('#external-events div.external-event'))

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear()
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'year, month'
            },
            buttonText: {
                today: 'today'
//                month: 'month'
            },

            defaultView: 'month',
            
//             events: [ 
//                 { 
//                     title: 'Friends Literary Tea',
//                     start: new Date(y, m, d, 17, 7, 28),
//                     end: new Date(y, m, d, 17, 7, 28),
//                     backgroundColor: '#09148a', //red
//                     borderColor: '#09148a' //red
//                 },
//                 {
//                     title: 'Friends Literary Tea',
//                     start: new Date(y, m, d - 5),
//                     end: new Date(y, m, d - 2),
//                     backgroundColor: '#f39c12', //yellow
//                     borderColor: '#f39c12' //yellow
//                 },
//                 {
//                     title: 'CTMA Qualifying Gala III',
// //                    start: new Date(y, m, d, 10, 30),
// //                    allDay: false,
//                     backgroundColor: '#0073b7', //Blue
//                     borderColor: '#0073b7' //Blue
//                 }
//             ],

            events:
            {
                url: '/feed.php',
                cache: true
            },

            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject')

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject)

                // assign it the date that was reported
                copiedEventObject.start = date
                copiedEventObject.allDay = allDay
                copiedEventObject.backgroundColor = $(this).css('background-color')
                copiedEventObject.borderColor = $(this).css('border-color')

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove()
                }

            }
        })

        /* ADDING EVENTS */
        var currColor = '#3c8dbc' //Red by default
        //Color chooser button
        var colorChooser = $('#color-chooser-btn')
        $('#color-chooser > li > a').click(function (e) {
            e.preventDefault()
            //Save color
            currColor = $(this).css('color')
            //Add color effect to button
            $('#add-new-event').css({'background-color': currColor, 'border-color': currColor})
        })
        $('#add-new-event').click(function (e) {
            e.preventDefault()
            //Get value and make sure it is not null
            var val = $('#new-event').val()
            if (val.length == 0) {
                return
            }

            //Create events
            var event = $('<div />')
            event.css({
                'background-color': currColor,
                'border-color': currColor,
                'color': '#fff'
            }).addClass('external-event')
            event.html(val)
            $('#external-events').prepend(event)

            //Add draggable funtionality
            init_events(event)

            //Remove event from text input
            $('#new-event').val('')
        })
    })
</script>

<!-- 404 ERROR TEMPLATE -->
<section class="default-section">
    <div class="container">
        <div class="row ">
            <div class="col-md-10">

                <!-- ACTIVITY: Content -->
                <article class="activity-main">

                    <!-- ACTIVITY: Header -->
                    <section class="activity-header">
                        <div class="row">
                            <h1 class="h1" >Error | </h1> 
                            <h1 class="h1" style="font-weight:500">&nbsp;404</h1>
                        </div>
                        <br>
                    </section>

                    <section class="page-content content-error" id="bg-white">
                        <div >
                            <h2 class="h2">Sorry! The page you are trying to open cannot be found.</h2>
                            <p>
                                For assistance please complete this <a href="#">form</a> or contact our 
                                <a href="mailto:support@falsebaytours.com">Support Team</a>.
                                We apologize for the any inconvenience caused!
                            </p>
                            <a class="btn btn-primary " href="index.php?action=home">Back</a>
                        </div>
                    </section>

            </div>
        </div>
    </div>
</section>








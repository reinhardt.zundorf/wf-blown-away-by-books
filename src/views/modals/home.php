<?php
?>
<!--
 Button to Open the Modal 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Open modal
</button>-->

<!-- SERVICES: Restaurants -->
<div class="modal fade" id="mod_restaurants">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="bg-restaurants">
                <h4 class="modal-title">Restaurants</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <div class="row">



                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!-- SERVICES: Accomodation -->
<div class="modal fade" id="mod_accomodation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="bg-accomodation">
                <h4 class="modal-title">Accomodation</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <br>
                <br>
                <br>
                <br>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- SERVICES: Tour Guides -->
<div class="modal fade" id="mod_tour_guides">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="bg-tour-guides">
                <h4 class="modal-title">Tour Guides</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <br>
                <br>
                <br>
                <br>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
/* The MIT License
  Copyright 2018 Web Folders (Pty) Ltd.

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE. */

/* ============================================================ *
 * Header File                v1.0                              *
 * Web Folders CMS                                              *
 * ------------------------------------------------------------ *
 * Contains HTML markup for <head> tag.                         *
 * ------------------------------------------------------------ */

/* ==================================================== *
 * Put $view structure into local variables (faster)    *
 * ---------------------------------------------------- */
$headerSize = "M";

if (isset($view["headerSize"]))
{
    $headerSize = $view["headerSize"];
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
    <!--<![endif]-->

    <!-- HEAD -->
    <head>

        <!-- META -->
        <meta content="text/html; charset=utf-8" http-equiv="Content-type">
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <!--<meta name=viewport content="width=device-width, initial-scale=1">-->
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="Web Folders (Pty) Ltd" name="author">
        <meta content="Blown Away By Books, Fish Hoek Public Library, Library, Books, Festival, Fish Hoek, Janusz Skarzynski" name="keywords">
        <meta content="index, follow" name="robots">

        <!-- TITLE -->
        <title>Blown Away By Books <?php if (isset($view["title"])) { $title = ucfirst($view["title"]); echo " | " . $title; }?></title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">

        <!-- SECTIONS -->
        <link href="css/sections/header.css"        rel="stylesheet">
        <link href="css/sections/navbar_tabs.css"   rel="stylesheet">
        <link href="css/sections/footer.css"        rel="stylesheet">
        <link href="css/sections/social-media.css"  rel="stylesheet">
        <link href="css/sections/buttons.css"       rel="stylesheet">
        <link href="css/sections/typography.css"    rel="stylesheet">
        <link href="css/sections/backgrounds.css"   rel="stylesheet">
        <link href="css/sections/sidebar.css"       rel="stylesheet">
        <link href="css/sections/tiles.css"         rel="stylesheet">
        <link href="css/main.css"                   rel="stylesheet">

        <?php
        /* ==================================================== *
         * Check for calendar view type                         *  
         * ---------------------------------------------------- */
        if (isset($view["type"]))
        {
            if ($view["type"] === "calendar")
            {
                // echo "<link rel='stylesheet' href='css/views/calendar.css'";
                // echo "<link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.1.0/fullcalendar.css' rel='stylesheet'/>";
            }
        }

        /* ==================================================== *
         * View CSS files                                       *  
         * ---------------------------------------------------- */
        if (isset($view["styles"]))
        {
            $styles = $view["styles"];

            foreach ($styles as $stylesheet)
            {
                echo "<link href='css/views/$stylesheet' rel='stylesheet'>\n\t\t";
            }
        }
        ?>

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
          <![endif]-->

        <!-- FAVICON -->
        <link href="favicon.png" type="image/png" rel="shortcut icon">

        <!-- JQUERY -->
        <script src="js/moment.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>

        <script src="js/fullcalendar.js"></script>
        <!--<script src="js/bootstrap.bundle.js"></script>-->



    </head>

    <!-- BODY -->
    <body class="body-<?php if (isset($view['body'])) echo $view['body']; ?>" itemscope itemtype="http://schema.org/WebApplication">

        <div id="fb-root"></div>

        <script>
            (function (d, s, id)
            {
                var js, fjs = d.getElementsByTagName(s)[0];
                if(d.getElementById(id))
                {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=2299278180299326&version=v2.0";

                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- BUTTON: Back to top -->    
        <a class="btn btn-top" onclick="topFunction()" id="btnTop" title="Go to top"><i class="fa fa-2x fa-arrow-up"></i></a>

        <script>
            window.onscroll = function ()
            {
                scrollFunction();
            };

            /* =============================================== *
             * If more than 20 display button                  *
             * ----------------------------------------------- */
            function scrollFunction()
            {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20)
                {
                    document.getElementById("btnTop").style.display = "block";
                } else
                {
                    document.getElementById("btnTop").style.display = "none";
                }
            }

            /* =============================================== *
             * Scroll to top on click                          *
             * ----------------------------------------------- */
            function topFunction()
            {
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            }
        </script>

        <!-- MAIN -->
        <main class="container" style="background-color:rgb(255, 255, 255); padding:0px;">


            <!-- HEADER -->
            <header class="main-header">
                
                <div class="topbar">
                    <div class="icon-bar">
                        <a href="#" class="facebook"><i class="fa fa-2x fa-facebook"></i></a> 
                        <a href="#" class="twitter"><i class="fa fa-2x fa-twitter"></i></a> 
                        <a href="#" class="google"><i class="fa fa-2x fa-google"></i></a> 
                        <!--<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>-->
                        <!--<a href="#" class="youtube"><i class="fa fa-youtube"></i></a>--> 
                    </div>
                </div>

                <nav class="navbar nav">
                    <a class="navbar-brand">
                        <img src="res/brand/brand-logo-md.png" alt="Blown Away By Books logo"/>
                    </a>
                    <!-- The social media icon bar -->

                </nav>

                <!-- NAVBAR -->
                <nav class="navbar-nav ml-auto">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a id="nav-home" class="nav-link" href="index.php?action=home">Home</a></li>
                        <!-- <li class="nav-item"><a id="nav-about" class="nav-link" href="index.php?action=about"></i>About</a></li> -->
                        <li class="nav-item dropdown">
                            <a id="nav-about" data-toggle="dropdown" class="nav-link" href="#" aria-haspopup="true" aria-expanded="false"></i>About</a>
                            <!-- <a id="nav-festival" class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Festival</a> -->
                            <div class="dropdown-menu" aria-labelledby="nav-about">
                                <a id="nav-about-us" href="index.php?action=about" class="dropdown-item">About us</a>
                                <a id="nav-what-we-do" href="index.php?action=about&type=services" class="dropdown-item">What we do</a>
                                <a id="nav-contact" href="index.php?action=contact" class="dropdown-item">Get in touch</a>
                                <a id="nav-sponsors" href="index.php?action=about&type=sponsors" class="dropdown-item">Our sponsors</a>
                            </div>
                        </li>
                        <!-- <li class="nav-item"><a id="nav-contact" class="nav-link" href="index.php?action=contact"></i>Contact</a></li> -->
                        <li class="nav-item dropdown">
                            <a id="nav-festival" class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Festival</a>
                            <div class="dropdown-menu" aria-labelledby="nav-festival">
                                <!-- <a id="nav-festival-gallery" href="index.php?action=festival&type=gallery" class="dropdown-item">Gallery</a> -->
                                <a id="nav-festival-about" href="index.php?action=festival&type=about" class="dropdown-item">About</a>
                                <a id="nav-festival-info" href="index.php?action=festival&type=authors" class="dropdown-item"></a>
                                <a id="nav-festival-calendar" href="index.php?action=festival&type=calendar" class="dropdown-item">Programme</a>
                                <a id="nav-archive" href="index.php?action=festival&type=gallery" class="dropdown-item">Gallery</a>
                                <a id="nav-archive" href="index.php?action=festival&type=archive" class="dropdown-item">Past Festivals</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a id="nav-programme" class="nav-link" href="index.php?action=programme">Programme</a>
                        </li>

                        <!-- <li class="nav-item"></li> -->
                        <li class="nav-item dropdown">
                            <a id="nav-books" class="nav-link" href="" id="dropdownBooks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Books</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownBooks">
                                <a id="nav-books-featured" href="index.php?action=books&type=latest" class="dropdown-item">Latest Books</a>
                                <a id="nav-books-authors" href="index.php?action=books&type=news" class="dropdown-item">News</a>
                                <a id="nav-books-events" href="index.php?action=books&type=authors" class="dropdown-item">Authors</a>
                                <a id="nav-books-events" href="index.php?action=books&type=calendar" class="dropdown-item">Events</a>
                            </div>
                        </li>
                        <li class="nav-item"><a id="nav-film" class="nav-link" href="index.php?action=film">Film</a></li>
                        <li class="nav-item"><a id="nav-blog" class="nav-link" href="index.php?action=blog">Blog</a></li>
                        <li class="navbar-text ml-auto">
                            Brought to you by <a href="" style="color:#777;">The Friends of the Fish Hoek Library</a>
                        </li>
                    </ul>
                </nav>
            </header>

            <!--<div class="breadcrumbs-wrapper">-->
            <div class="container">
                <div class="breadcrumbs">
                    <?php
                    /* ======================================================== *
                     * Set variables for Breadcrumbs                            *
                     * -------------------------------------------------------- */
                    $title  = $view["heading"];
                    $link   = $view["title"];
                    $uTitle = ucwords($title);

                    if ($title === "home")
                        $uTitle = "Welcome!";

                    /* ======================================================== *
                     * Output Breadcrumbs                                       *
                     * -------------------------------------------------------- */
                    echo "<a class='breadcrumb-item' href='index.php'><i class='fa fa-home'></i></a>";
                    echo "<a class='breadcrumb-item' href='index.php' aria-current='page'>Home</a>";
                    echo "<a class='breadcrumb-item' href='index.php?action={$link}' aria-current='page'>$uTitle</a>";
                    ?>
                </div>
            </div>
            <!--</div>-->

<?php

/*
 * The MIT License
 *
 * Copyright 2018 Web Folders (Pty) Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * View class
 * ---
 * Gets 
 */
class View
{
    public $id              = null;
    public $title           = null;
    public $desc            = null;
    public $subtitles       = [];
    public $content         = [];
    public $date_added      = null;
    public $date_modified   = null;

    /**
     * Sets the object's properties using the values in the supplied array
     *
     * @param assoc The property values
     */
    public function __construct($data = array())
    {
        if(isset($data['id']))
        {
            $this->id = (int) $data['id'];
        }
        if(isset($data['title']))
        {
            $this->title = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['title']);
        }
        if(isset($data['date_added']))
        {
            $this->date_added = (int) $data['date_added'];
        }
        if(isset($data['date_modified']))
        {
            $this->date_modified = (int) $data['date_modified'];
        }
        if(isset($data['summary']))
        {
            $this->summary = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['summary']);
        }
        if(isset($data['content']))
        {
            $this->content = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['content']);;
        }
        if(isset($data['posterId']))
        {
            $this->posterId = (int) $data['posterId'];
        }
    }

    /**
     * Sets the object's properties using the edit form post values in the supplied array
     *
     * @param assoc The form post values
     */
    public function storeFormValues($params)
    {

        // Store all the parameters
        $this->__construct($params);

        // Parse and store the publication date
        if(isset($params['publicationDate']))
        {
            $publicationDate = explode('-', $params['publicationDate']);

            if (count($publicationDate) == 3)
            {
                list($y, $m, $d) = $publicationDate;
                $this->publicationDate = mktime(0, 0, 0, $m, $d, $y);
            }
        }
    }

    /**
     * Returns an View object matching the given View ID
     *
     * @param int The View ID
     * @return View|false The View object, or false if the record was not found or there was a problem
     */
    public static function getById($id)
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM View WHERE id = :id";
        $st  = $conn->prepare($sql);

        $st->bindValue(":id", $id, PDO::PARAM_INT);
        $st->execute();

        $row = $st->fetch();

        $conn = null;

        if($row)
        {
            return new View($row);
        }
    }

    /**
     * Returns all (or a range of) View objects in the DB
     *
     * @param int Optional The number of rows to return (default=all)
     * @param string Optional column by which to order the Views (default="publicationDate DESC")
     * @return Array|false A two-element array : results => array, a list of View objects; totalRows => Total number of Views
     */
    public static function getList($numRows = 1000000, $order = "publicationDate DESC")
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT SQL_CALC_FOUND_ROWS *, 
                 UNIX_TIMESTAMP(publicationDate) 
                 AS publicationDate 
                 FROM View
                 ORDER BY " . $order . " LIMIT :numRows";

        $st = $conn->prepare($sql);

        $st->bindValue(":numRows", $numRows, PDO::PARAM_INT);
        $st->execute();

        $list = array();
        $count = 0;

        while ($row = $st->fetch())
        {
            $View = new View($row);
            $list[]  = $View;
            $count++;
        }

        $sql       = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $conn->query($sql)->fetch();

        $conn = null;
        
        /* ============================================================== *
         * Return array with two items, "results" (View object array)  *
         * and "totalRows" (single numerical value).                      *
         * -------------------------------------------------------------- */
        return (["results" => $list, "totalRows" => $totalRows[0]]);
    }

    /**
     * Inserts the current View object into the database, and sets its ID property.
     */
    public function insert()
    {
        if (!is_null($this->id))
        {
            trigger_error("View::insert(): Attempt to insert an View object that already has its ID property set (to $this->id).", E_USER_ERROR);
        }

        $conn     = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql      = "INSERT INTO Views ( publicationDate, title, summary, content ) VALUES ( FROM_UNIXTIME(:publicationDate), :title, :summary, :content )";
        $st       = $conn->prepare($sql);
        $st->bindValue(":publicationDate", $this->publicationDate, PDO::PARAM_INT);
        $st->bindValue(":title", $this->title, PDO::PARAM_STR);
        $st->bindValue(":summary", $this->summary, PDO::PARAM_STR);
        $st->bindValue(":content", $this->content, PDO::PARAM_STR);
        $st->execute();
        $this->id = $conn->lastInsertId();
        $conn     = null;
    }

    /**
     * Updates the current View object in the database.
     */
    public function update()
    {
        if (is_null($this->id))
        {
            trigger_error("View::update(): Attempt to update an View object that does not have its ID property set.", E_USER_ERROR);
        }
        
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        
        $sql  = "UPDATE Views SET publicationDate=FROM_UNIXTIME(:publicationDate), title=:title, summary=:summary, content=:content WHERE id = :id";
        $st   = $conn->prepare($sql);
        $st->bindValue(":publicationDate", $this->publicationDate, PDO::PARAM_INT);
        $st->bindValue(":title", $this->title, PDO::PARAM_STR);
        $st->bindValue(":summary", $this->summary, PDO::PARAM_STR);
        $st->bindValue(":content", $this->content, PDO::PARAM_STR);
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();
        $conn = null;
    }

    /**
     * Deletes the current View object from the database.
     */
    public function delete()
    {
        if (is_null($this->id))
        {
            trigger_error("View::delete(): Attempt to delete an View object that does not have its ID property set.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $st = $conn->prepare("DELETE FROM Views WHERE id = :id LIMIT 1");
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();

        $conn = null;
    }

}

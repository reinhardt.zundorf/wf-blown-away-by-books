<?php

/*
 * The MIT License
 *
 * Copyright 2018 Web Folders (Pty) Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Class Book
 */
class Book
{

    public $id            = null;
    public $title         = null;
    public $altTitle      = null;     /* Alternative title */
    public $authorId      = null;     /* Author */
    public $publisherId   = null;     /* FK to Publisher class/table */
    public $datePublished = null;
    public $text          = null;     /* Book text */
    public $genreId       = null;
    public $notes         = null;     /* Any further information or notes on the book */
    public $coverUrl      = null;     /* Boolean value whether the Book is to be featured on the landing */
    public $featured      = null;     /* Boolean value whether the Book is to be featured on the landing */
    public $dateAdded     = null;
    public $dateModified  = null;     /* Date the entry was last updated in the database */

    /**
     * Sets the object's properties using the values in the supplied array
     *
     * @param assoc The property values
     */
    public function __construct($data = array())
    {
        if(isset($data['id']))
        {
            $this->id = (int) $data['id'];
        }
        
        if(isset($data['title']))
        {
            $this->title = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['title']);
        }
        
        if(isset($data['altTitle']))
        {
            $this->altTitle = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['altTitle']);
        }
        
        if(isset($data['authorId']))
        {
            $this->authorId = (int) $data['authorId'];
        }
        
        
        if(isset($data['text']))
        {
            $this->text = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['text']);
        }
        
        if(isset($data['coverUrl']))
        {
            $this->coverUrl = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['coverUrl']);
        }
        
        if(isset($data['genreId']))
        {
            $this->genreId = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['genreId']);
        }
        
        
        if(isset($data['datePublished']))
        {
            $this->datePublished = (int) $data['datePublished'];
        }
        
        if(isset($data['dateAdded']))
        {
            $this->dateAdded = (int) $data['dateAdded'];
        }
        
        if(isset($data['dateModified']))
        {
            $this->dateModified = (int) $data['dateModified'];
        }
        
        /*if(isset($data['blurb']))
        {
            $this->blurb = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['blurb']);
        }
        
        if(isset($data['featured']))
        {
            $this->featured = (int) $data['featured'];
        }*/
    }

    /**
     * Sets the object's properties using the edit form post values in the supplied array
     *
     * @param assoc The form post values
     */
    public function storeFormValues($params)
    {

        // Store all the parameters
        $this->__construct($params);

        // Parse and store the publication date
//        if (isset($params['publicationDate']))
//        {
//            $publicationDate = explode('-', $params['publicationDate']);
//
//            if (count($publicationDate) == 3)
//            {
//                list($y, $m, $d) = $publicationDate;
//                $this->publicationDate = mktime(0, 0, 0, $m, $d, $y);
//            }
//        }
    }

    /**
     * Returns an Book object matching the given book ID
     *
     * @param int The book ID
     * @return Book|false The book object, or false if the record was not found or there was a problem
     */
    public static function getById($id)
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT * "
             . "FROM book "
             . "WHERE id = :id";
        
        $st  = $conn->prepare($sql);

        $st->bindValue(":id", $id, PDO::PARAM_INT);
        $st->execute();

        $row = $st->fetch();

        $conn = null;

        if ($row)
        {
            return new Book($row);
        }
    }

    /**
     * Returns all (or a range of) Book objects in the DB
     *
     * @param int Optional The number of rows to return (default=all)
     * @param string Optional column by which to order the books (default="publicationDate DESC")
     * @return Array|false A two-element array : results => array, a list of Book objects; totalRows => Total number of books
     */
    public static function getList($numRows = 1000000, $order = "publicationDate DESC")
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT SQL_CALC_FOUND_ROWS *, 
                 UNIX_TIMESTAMP(publicationDate) 
                 AS publicationDate 
                 FROM book
                 ORDER BY " . $order . " LIMIT :numRows";

        $st = $conn->prepare($sql);

        $st->bindValue(":numRows", $numRows, PDO::PARAM_INT);
        $st->execute();

        $list  = array();
        $count = 0;

        while ($row = $st->fetch())
        {
            $book = new Book($row);
            $list[]  = $book;
            $count++;
        }

        $sql       = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $conn->query($sql)->fetch();

        $conn = null;

        /* ============================================================== *
         * Return array with two items, "results" (Book object array)  *
         * and "totalRows" (single numerical value).                      *
         * -------------------------------------------------------------- */
        return (["results" => $list, "totalRows" => $totalRows[0]]);
    }

    /**
     * Inserts the current Book object into the database, and sets its ID property.
     */
    public function insert()
    {
        if (!is_null($this->id))
        {
            trigger_error("Book::insert(): Attempt to insert an Book object that already has its ID property set (to $this->id).", E_USER_ERROR);
        }

        $conn     = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql      = "INSERT INTO books ( publicationDate, title, text, content ) VALUES ( FROM_UNIXTIME(:publicationDate), :title, :text, :content )";
        $st       = $conn->prepare($sql);
        $st->bindValue(":publicationDate", $this->publicationDate, PDO::PARAM_INT);
        $st->bindValue(":title", $this->title, PDO::PARAM_STR);
        $st->bindValue(":text", $this->text, PDO::PARAM_STR);
        $st->bindValue(":content", $this->content, PDO::PARAM_STR);
        $st->execute();
        $this->id = $conn->lastInsertId();
        $conn     = null;
    }

    /**
     * Updates the current Book object in the database.
     */
    public function update()
    {
        if (is_null($this->id))
        {
            trigger_error("Book::update(): Attempt to update an Book object that does not have its ID property set.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql  = "UPDATE books SET publicationDate=FROM_UNIXTIME(:publicationDate), title=:title, text=:text, content=:content WHERE id = :id";
        
        $st   = $conn->prepare($sql);
        $st->bindValue(":publicationDate", $this->publicationDate, PDO::PARAM_INT);
        $st->bindValue(":title", $this->title, PDO::PARAM_STR);
        $st->bindValue(":text", $this->text, PDO::PARAM_STR);
        $st->bindValue(":content", $this->content, PDO::PARAM_STR);
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();
        $conn = null;
    }

    /**
     * Deletes the current Book object from the database.
     */
    public function delete()
    {
        if (is_null($this->id))
        {
            trigger_error("BOOK - delete() Attempt to delete an Book object that does not have its ID property set.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $st = $conn->prepare("UPDATE book "
                           . "SET dateRemoved=CURRENT_TIMESTAMP, "
                           . "dateModified=CURRENT_TIMESTAMP "
                           . "WHERE id = :id LIMIT 1;");            /* set column `dateRemoved` to current time for the ID */
        
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();

        $conn = null;
    }

}

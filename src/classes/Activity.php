<?php

/*
 * The MIT License
 *
 * Copyright 2018 Web Folders (Pty) Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Class to handle activities
 */
class Activity
{

    /**
     * Class Attributes
     */
    public $id        = null;
    public $name      = null;
    public $text      = null;
    public $dateadded = null;

    /**
     * Sets the object's properties using the values in the supplied array
     *
     * @param assoc The property values
     */
    public function __construct($data = array())
    {
        if (isset($data['id']))
        {
            $this->id = (int) $data['id'];
        }
        if (isset($data['name']))
        {
            $this->name = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['name']);
        }
        if (isset($data['text']))
        {
            $this->text = $data['text'];
        }
        if (isset($data['dateadded']))
        {
            $this->dateadded = $data["dateadded"];
        }
    }

    /**
     * Sets the object's properties using the edit form post values in the supplied array
     *
     * @param assoc The form post values
     */
    public function storeFormValues($params)
    {
        $this->__construct($params);
    }

    /** =================================================================== *
     * Returns an Activity object matching the given activity ID            *
     * -------------------------------------------------------------------- *
     * @param int The activity ID                                           *
     * @return Activity | false                                             *
     * The activity object, or false if the record was not found or there   *
     * was a problem                                                        *
     * -------------------------------------------------------------------- */
    public static function getById($id)
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT * FROM activity_category "
                . "WHERE id = :id "
                . "AND dateremoved IS NULL;";

        $st = $conn->prepare($sql);

        $st->bindValue(":id", $id, PDO::PARAM_INT);
        $st->execute();

        $row = $st->fetch();

        $conn = null;

        if ($row)
        {
            return new Activity($row);
        }
    }

    /**
     * Returns all (or a range of) Activity objects in the DB
     *
     * @param int Optional The number of rows to return (default=all)
     * @param string Optional column by which to order the activities (default="publicationDate DESC")
     * @return Array|false A two-element array : results => array, a list of Activity objects; totalRows => Total number of activities
     */
    public static function getList($numRows = 1000000, $order = "id ASC")
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

//        $sql = "SELECT *
//                FROM activity_category
//                ORDER BY " . $order . " LIMIT :numRows;
//                WHERE dateremoved IS NULL;";

//        error_log("ACTIVITY: getList() - SQL statement prepared.");

        $sql = "SELECT * FROM activity_category WHERE dateremoved IS NULL;";
        
        $st = $conn->prepare($sql);

        $st->bindValue(":numRows", $numRows, PDO::PARAM_INT);
        $st->execute();

        $list  = array();
        $count = 0;


        while($row = $st->fetch())
        {
            $activity = new Activity($row);
            $list[]   = $activity;
            $count++;
        }

        error_log("ACTIVITY: getList() - Added Activity objects to array with DB data.");

        $sql       = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $conn->query($sql)->fetch();

        error_log("ACTIVITY: getList() - Added number of rows to 'totalRows'.");


        $conn = null;

        /* ============================================================== *
         * Return array with two items, "results" (Activity object array)  *
         * and "totalRows" (single numerical value).                      *
         * -------------------------------------------------------------- */
        return (["results" => $list, "totalRows" => $totalRows[0]]);
    }

    /**
     * Inserts the current Activity object into the database, and sets its ID property.
     */
    public function insert()
    {
        if (!is_null($this->id))
        {
            trigger_error("Activity::insert(): Attempt to insert an Activity object that already has its ID property set (to $this->id).", E_USER_ERROR);
        }

        $conn     = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql      = "INSERT INTO activity_category ( name, text, dateadded ) VALUES ( :name, :text, :dateadded )";
        $st       = $conn->prepare($sql);
        
        $st->bindValue(":name", $this->name, PDO::PARAM_STR);
        $st->bindValue(":text", $this->text, PDO::PARAM_STR);
        $st->bindValue(":dateadded", "now()", PDO::PARAM_STR);
        $st->execute();
        
        $this->id = $conn->lastInsertId();
        
        $conn     = null;
    }

    /**
     * Updates the current Activity object in the database.
     */
    public function update()
    {

        // Does the Activity object have an ID?
        if (is_null($this->id))
            trigger_error("Activity::update(): Attempt to update an Activity object that does not have its ID property set.", E_USER_ERROR);

        // Update the Activity
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "UPDATE activity "
                . "SET name=:name, text=:text WHERE id = :id";

        $st = $conn->prepare($sql);

        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->bindValue(":name", $this->name, PDO::PARAM_STR);
        $st->bindValue(":text", $this->text, PDO::PARAM_STR);
//        $st->bindValue(":dateadded", "now()", PDO::PARAM_STR);

        $st->execute();
        $conn = null;
    }

    /**
     * Deletes the current Activity object from the database.
     */
    public function delete()
    {
        if (is_null($this->id))
        {
            trigger_error("Activity::delete(): Attempt to delete an Activity object that does not have its ID property set.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $st = $conn->prepare("DELETE FROM activity_category WHERE id = :id LIMIT 1");
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();

        $conn = null;
    }

}

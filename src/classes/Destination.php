<?php

/*
 * The MIT License
 *
 * Copyright 2018 Web Folders (Pty) Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Class to handle destinations
 */
class Destination
{
    // Properties

    /**
     * @var int The destination ID from the database
     */
    public $id = null;

    /**
     * @var string Full title of the destination
     */
    public $name = null;

    /**
     * @var string The HTML content of the destination
     */
    public $text = null;


    /**
     * Sets the object's properties using the values in the supplied array
     *
     * @param assoc The property values
     */
    public function __construct($data = array())
    {
        if (isset($data['id']))
        {
            $this->id = (int) $data['id'];
        }
        if (isset($data['name']))
        {
            $this->name = preg_replace("/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9()]/", "", $data['name']);
        }
        if (isset($data['text']))
        {
            $this->text = $data['text'];
        }
        if (isset($data['dateadded']))
        {
            $this->dateadded = (int) $data['dateadded'];
        }
    }

    /**
     * Sets the object's properties using the edit form post values in the supplied array
     *
     * @param assoc The form post values
     */
    public function storeFormValues($params)
    {

        // Store all the parameters
        $this->__construct($params);

        // Parse and store the publication date
//        if(isset($params['publicationDate']))
//        {
//            $publicationDate = explode('-', $params['publicationDate']);
//
//            if (count($publicationDate) == 3)
//            {
//                list($y, $m, $d) = $publicationDate;
//                $this->publicationDate = mktime(0, 0, 0, $m, $d, $y);
//            }
//        }
    }

    /**
     * Returns an Destination object matching the given destination ID
     *
     * @param int The destination ID
     * @return Destination|false The destination object, or false if the record was not found or there was a problem
     */
    public static function getById($id)
    {
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT * FROM destination WHERE id = :id";
        $st  = $conn->prepare($sql);

        $st->bindValue(":id", $id, PDO::PARAM_INT);
        $st->execute();

        $row = $st->fetch();

        $conn = null;

        if ($row)
        {
            return new Destination($row);
        }
    }

    /**
     * Returns all (or a range of) Destination objects in the DB
     *
     * @param int Optional The number of rows to return (default=all)
     * @param string Optional column by which to order the destinations (default="publicationDate DESC")
     * @return Array|false A two-element array : results => array, a list of Destination objects; totalRows => Total number of destinations
     */
    public static function getList($numRows = 1000000, $order = "id DESC")
    {
        error_log("Called getList() from Destination class.");
        
        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $sql = "SELECT * FROM destination WHERE dateremoved IS NULL;";
        $st = $conn->prepare($sql);

//        $st->bindValue(":numRows", $numRows, PDO::PARAM_INT);
        $st->execute();

        error_log("SQL SELECT statement for all rows in `destination` tables was successful.");
        
        $list = array();
        $count = 0;

        while($row = $st->fetch())
        {
            $destination = new Destination($row);
            $list[]  = $destination;
            $count++;
        }

        $sql       = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $conn->query($sql)->fetch();

        $conn = null;
        
        error_log("getList(): returning the list container of all destinations in the DB to the Controller.");
        
        return (["results" => $list]);
    }

    /**
     * Inserts the current Destination object into the database, and sets its ID property.
     */
    public function insert()
    {
        if (!is_null($this->id))
        {
            trigger_error("Destination::insert(): Attempt to insert an Destination object that already has its ID property set (to $this->id).", E_USER_ERROR);
        }

        $conn     = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        $sql      = "INSERT INTO destination ( publicationDate, title, summary, content ) VALUES ( FROM_UNIXTIME(:publicationDate), :title, :summary, :content )";
        $st       = $conn->prepare($sql);
        
        $st->bindValue(":publicationDate", $this->publicationDate, PDO::PARAM_INT);
        $st->bindValue(":title", $this->title, PDO::PARAM_STR);
        $st->bindValue(":summary", $this->summary, PDO::PARAM_STR);
        $st->bindValue(":content", $this->content, PDO::PARAM_STR);

        $st->execute();
        $this->id = $conn->lastInsertId();
        $conn     = null;
    }

    /**
     * Updates the current Destination object in the database.
     */
    public function update()
    {
        if(is_null($this->id))
        {
            trigger_error("Destination::update(): Attempt to update an Destination object that does not have its ID property set.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
        
        $sql  = "UPDATE destination SET dateadded=now(), title=:title, summary=:summary, content=:content WHERE id = :id";
        $st   = $conn->prepare($sql);
        
        $st->bindValue(":dateadded", "now()", PDO::PARAM_INT);
        $st->bindValue(":name",      $this->name, PDO::PARAM_STR);
        $st->bindValue(":text",      $this->text, PDO::PARAM_STR);
        $st->bindValue(":id",        $this->id, PDO::PARAM_INT);
        
        $st->execute();
        
        $conn = null;
    }

    /**
     * Deletes the current Destination object from the database.
     */
    public function delete()
    {

        // Does the Destination object have an ID?
        if (is_null($this->id))
        {
            trigger_error("Destination::delete(): Attempt to delete an Destination object that does not have its ID property set.", E_USER_ERROR);
        }

        $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        $st = $conn->prepare("UPDATE destination SET dateremoved=now() WHERE id = :id LIMIT 1");
        $st->bindValue(":id", $this->id, PDO::PARAM_INT);
        $st->execute();

        $conn = null;
    }

}

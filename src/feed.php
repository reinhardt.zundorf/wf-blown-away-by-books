<?php

include "config/config.php";

try
	{

		/* =================================================== *
		 * Connect to DB                                       *
		 * --------------------------------------------------- */
        $connection = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

		/* =================================================== *
         * Prepare & Execute Query                             *
         * --------------------------------------------------- */
		$query = "SELECT * FROM event;";
		$sth   = $connection->prepare($query);
		$sth->execute();

        error_log("CONTROLLER: Executing SQL statement (feed)");

		/* =================================================== *
         * Return array                                        *
         * --------------------------------------------------- */
		$events = array();

		/* =================================================== *
         * Fetch results                                       *
         * --------------------------------------------------- */
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) 
        {
			$e           = array();
			$e['id']     = $row['id'];
			$e['title']  = "Lorem Ipsum";
			$e['start']  = $row['start'];
			$e['end']    = $row['end'];
			$e['allDay'] = false;

    	   /* =================================================== *
            * Merge the two arrays                                *
            * --------------------------------------------------- */
			array_push($events, $e);
		}

        error_log("CONTROLLER: Merging arrays & handling result set. (feed)");

		/* =================================================== *
		 * Output event data as JSON                           *
		 * --------------------------------------------------- */
		echo json_encode($events);

        error_log("CONTROLLER: Outputting JSON event data. (feed)");

		exit();

	}
	catch (PDOException $e) 
    {
		echo $e->getMessage();
	}
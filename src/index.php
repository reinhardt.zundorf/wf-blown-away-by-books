<?php

/* ============================================================ *
 * Index Controller File      v1.5                              *
 * Web Folders CMS                                              *
 * ------------------------------------------------------------ *
 * This file controls the back-end navigation for the main      *
 * navbar on the website. Views are 'pages' in the traditional  *
 * website sense and are the sum of:                            *
 * 1. Header             (header.php)                           *
 * 2. 'content'          ([page].php)                           *
 * 3. Footer             (footer.php)                           *
 * ------------------------------------------------------------ *
 * To create a view call displayView($page) or to display the   *
 * construction page call displayCons($page).                   *
 * ------------------------------------------------------------ */

/* ============================================================ *
 * Configuration Script                                         *
 * ------------------------------------------------------------ */
require "./config/config.php";

/* ============================================================ *
 * Set variables                                                *
 * ------------------------------------------------------------ */
$view["type"] = "view";
$action       = filter_input(INPUT_GET, "action");
//$action       = filter_input(INPUT_GET, "action");

if(isset($action))
{
    switch($action)
    {
        case "home":
            home();
            break;

        case "about":
            about();
            break;

        case "books":
            books();
            break;

        case "events":
            events();
            break;

        case "film":
            film();
            break;

        case "contact":
            contact();
            break;

        case "blog":
            blog();
            break;

        case "festival":
            festival();
            break;

        case "programme":       /* opens up calendar in list mode for all the days */
            programme();
            break;

        case "feed":            /* FullCalendar PHP feed - output in JSON */
            feed();
            break;

        case "login":           /* Admin Portal login */
            login();
            break;

        case "register":        /* FUTURE */
            register();
            break;

        case "email":           /* Sends an email */
            email();
            break;

        case "events":
            events();
            break;

        case "temp":
            temp();
            break;

        case "error":
            error();
            break;

        default:
            break;
    }
}
else
{
    home();
}

/* ============================================================ *
 * Home                                                         *
 * ------------------------------------------------------------ */
function home()
{
    $view["title"]      = "home";
    $view["heading"]    = "home";
    $view["body"]       = "home";
    $view["styles"]     = ["home.css", "carousel.css", "post.css"];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    /* ============================================================ *
     * Get posts from database                                      *
     * ------------------------------------------------------------ */
    $db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    
    $data["results"] = Article::getList();
    $view["articles"] = $data["results"];

//    error_log("CONTROLLER: Displaying home page to user.");

    include "header.php";
    include "views/pages/home.php";
    include "footer.php";
}

/* ============================================================ *
 * About                                                        *
 * ------------------------------------------------------------ */
function about()
{
    $view["title"]      = "about";
    $view["heading"]    = "About Us";
    $view["body"]       = "about";
    $view["styles"]     = ["about.css"];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    error_log("CONTROLLER: Displaying about page to user.");

    include "./header.php";
    include "./views/pages/about.php";
    include "./footer.php";
}

/* ============================================================ *
 * Contact                                                      *
 * ------------------------------------------------------------ */

function contact()
{
    $view["title"]      = "contact";
    $view["body"]       = "contact";
    $view["heading"]    = "Contact";
    $view["styles"]     = ["contact.css"];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    include "./header.php";
    include "./views/pages/contact.php";
    include "./footer.php";
}

/* ============================================================ *
 * Register                                                     *
 * ------------------------------------------------------------ *
 * Take the user to the registration process to register a new  *
 * profile                                                      *
 * ------------------------------------------------------------ */
function register()
{
    $view["title"]      = "register";
    $view["body"]       = "register";
    $view["styles"]     = ["login-form.css"];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    include "./header.php";
    include "./views/pages/register.php";
    include "./footer.php";
}

/* ============================================================ *
 * Register                                                     *
 * ------------------------------------------------------------ */
function books()
{
    $view["title"]      = "books";
    $view["body"]       = "books";
    $view["heading"]    = "Books";
    $view["stylea"]    = [""];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    include "./header.php";
    include "./views/pages/books.php";
    include "./footer.php";
}

/* ============================================================ *
 * Register                                                     *
 * ------------------------------------------------------------ *
 * Take the user to the registration process to register a new  *
 * False Bay Tours profile                                      *
 * ------------------------------------------------------------ *
 * 1. Registration of False Bay Tours Member Account(form)      *
 * ------------------------------------------------------------ */
function film()
{
    $view["title"]      = "film";
    $view["body"]       = "film";
    $view["heading"]    = "Film";
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    error_log("CONTROLLER: Displaying film page to user.");

    include "./header.php";
    include "./views/pages/film.php";
    include "./footer.php";
}

function events()
{
    $view["type"]   = "calendar";
    $view["styles"] = ["calendar.css"];

    include "header.php";
    include "./views/pages/calendar.php";
    include "footer.php";
}

/* ============================================================ *
 * Login                                                        *
 * ------------------------------------------------------------ *
 * Start a new session and log the user in if there has been a  *
 * form submission (POST). Else, display the login modal        *
 * ------------------------------------------------------------ */
function login()
{
    $view["title"]      = "login";
    $view["body"]       = "login";
    $view["styles"]     = ["login-form.css"];
    $view["headerSize"] = "M";

    $filter_input = filter_input(INPUT_POST, "username");

    if (isset($filter_input))
    {
        $username = filter_input(INPUT_POST, "username");
        $password = filter_input(INPUT_POST, "password");
    }

    include "./header.php";
    include "./views/forms/login.php";
    include "./footer.php";
}

/* ============================================================ *
 * Under Construction                                           *
 * ------------------------------------------------------------ */

function temp()
{
    $view["title"]      = "Maintenance";
    $view["body"]       = "construction";
    $view["styles"]     = ["construction.css"];
    $view["headerSize"] = "S";
    $view["nav"]        = 1;

    include "./header.php";
    include "./views/pages/under_construction.php";
}

/* =================================================== *
 * Programme                                           *
 * --------------------------------------------------- */
function programme()
{
    $view["title"]      = "Programme";
    $view["type"]       = "view";
    $view["heading"]    = "Programme";
    $view["body"]       = "festival";
    $view["styles"]     = ["festival.css"];
    $view["headerSize"] = "S";
    $view["nav"]        = 1;
    
    include "header.php";
    include "views/pages/programme.php";
    include "footer.php";
}

/* =================================================== *
 * Festival                                            *
 * --------------------------------------------------- */
function festival()
{
    $view["title"]      = "festival";
    $view["type"]       = "view";
    $view["heading"]    = "Festival";
    $view["body"]       = "festival";
    $view["styles"]     = ["festival.css"];
    $view["headerSize"] = "S";
    $view["nav"]        = 1;

    $type = filter_input(INPUT_GET, "type");

    switch ($type)
    {
        /* =================================================== *
         * Info pages 		                               *
         * --------------------------------------------------- */
        case "list":

            $view["type"]   = "view";
            $view["styles"] = ["event_list.css"];

            include "header.php";
            include "./views/pages/event_list.php";
            include "footer.php";

            break;

        /* =================================================== *
         * Calendar 		                               *
         * --------------------------------------------------- */
        case "calendar":

            $view["type"]   = "calendar";
            $view["styles"] = ["calendar.css"];

            include "header.php";
            include "./views/pages/calendar.php";
            include "footer.php";

            break;

        /* =================================================== *
         * Event       		                               *
         * --------------------------------------------------- */
        case "event":

            $view["type"]   = "event";
            $view["styles"] = ["event.css"];

            include "header.php";
            include "./views/pages/view_event.php";
            include "footer.php";

            break;

        /* =================================================== *
         * Past Festivals	                               *
         * --------------------------------------------------- */
        case "gallery":

            $view["styles"] = ["event.css"];

            include "header.php";
            include "./views/templates/gallery.php";
            include "footer.php";

            break;

        default:

            $view["styles"] = ["calendar.css"];

            include "header.php";
            include "./views/pages/calendar.php";
            include "footer.php";

            break;
    }
}

/* =================================================== *
 * JSON Feed                                           *
 * --------------------------------------------------- */
function feed()
{
    try
    {

        /* =================================================== *
         * Connect to DB                                       *
         * --------------------------------------------------- */
        $connection = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

        /* =================================================== *
         * Prepare & Execute Query                             *
         * --------------------------------------------------- */
        $query = "SELECT * FROM event;";
        $sth   = $connection->prepare($query);
        $sth->execute();

        error_log("CONTROLLER: Executing SQL statement (feed)");

        /* =================================================== *
         * Return array                                        *
         * --------------------------------------------------- */
        $events = array();

        /* =================================================== *
         * Fetch results                                       *
         * --------------------------------------------------- */
        while ($row = $sth->fetch(PDO::FETCH_ASSOC))
        {
            $e           = array();
            $e['id']     = $row['id'];
            $e['title']  = "Lorem Ipsum";
            $e['start']  = $row['start'];
            $e['end']    = $row['end'];
            $e['allDay'] = false;

            /* =================================================== *
             * Merge the two arrays                                *
             * --------------------------------------------------- */
            array_push($events, $e);
        }

        error_log("CONTROLLER: Merging arrays & handling result set. (feed)");

        /* =================================================== *
         * Output event data as JSON                           *
         * --------------------------------------------------- */
        echo json_encode($events);

        error_log("CONTROLLER: Outputting JSON event data. (feed)");

        exit();
    }
    catch (PDOException $e)
    {
        echo $e->getMessage();
    }
}

/* =================================================== *
 * Blog                                                *
 * --------------------------------------------------- */
function blog()
{

    /* ===================================================== *
     * Configure the view and stylesheets to be attached.    *
     * ----------------------------------------------------- */
    $view["title"]      = "blog";
    $view["body"]       = "blog";
    $view["heading"]    = "Blog";
    $view["styles"]     = ["blog.css"];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    /* ================================================================ *
     * Create PDO object and prepare the SELECT statement to get all    *
     * the posts from the 'post' table in the 'fbt' database.           *
     * ---------------------------------------------------------------- */
    $db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

    /* ============================================================ *
     * Check post id variable from GET URL parameter                *
     * ------------------------------------------------------------ */
    $id = filter_input(INPUT_GET, "id");

    if ($id != "")
    {
        /* ============================================== *
         * Get post data from DB for the ID               *
         * ---------------------------------------------- */
        $view["post"] = Article::getById($id);

        /* ============================================== *
         * Display the post                               *
         * ---------------------------------------------- */
        require "./header.php";
        require "./views/templates/post.php";
        require "./footer.php";
    }
    else
    {

        /* ============================================================ *
         * Get posts from database                                      *
         * ------------------------------------------------------------ */
        $data["results"] = Article::getList();

        /* ===================================================================== *
         * Put the result set into an array as part of the $view[] structure.    *
         * --------------------------------------------------------------------- */
        $view["articles"] = $data["results"];

        /* ============================================================ *
         * Display on front end                                         *
         * ------------------------------------------------------------ */
        require "./header.php";
        require "./views/pages/blog.php";
        require "./footer.php";
    }
}

/* ============================================================ *
 * Function - Author View                                       *
 * ------------------------------------------------------------ *
 * Generates the front end output and includes the header and   *
 * footer templates. Form handling is also done here.           *
 * ------------------------------------------------------------ */

function view_author($id)
{

    /* ============================================ *
     * Get URL parameter for 'destination'          *
     * -------------------------------------------- */
    $action_d = filter_input(INPUT_GET, "type");

    /* ============================================ *
     * Set view configuration                       *
     * -------------------------------------------- */
    $temp = str_replace("_", " ", $action_d);

    /* ============================================ *
     * Set view configuration                       *
     * -------------------------------------------- */
    $view["title"]      = ucwords($temp);
    $view["heading"]    = $temp;
    $view["hero"]       = "'res/destinations/heros/{$view['name']}.jpg'";
    $view["body"]       = "destination";
    $view["styles"]     = ["destination.css"];
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    /* ========================================================= *
     * Pull content for the Destination from the `destinations`  *
     * table in the DB.                                          *
     * --------------------------------------------------------- */
    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);

    $sql = "SELECT * "
            . "FROM author "
            . "WHERE id = {$id};";

    /* ========================================================= *
     * Get all the destinations names from the database          *
     * --------------------------------------------------------- */
    $st     = $conn->prepare($sql);
    $st->execute();
    $result = $st->fetch();

    /* ========================================================= *
     * Get all the destinations names from the database          *
     * --------------------------------------------------------- */
    $st_sidebar  = $conn->prepare($sql_sidebar);
    $st_sidebar->execute();
    $res_sidebar = $st->fetch();

    $destinations = Destination::getList();

    $conn = null;

    $view["destinations"] = $destinations;
    $view["text"]         = $result[0];

    include "./header.php";
    include "./views/templates/destination.php";
    include "./footer.php";
}

/* ============================================================ *
 * Error: 404 (Page Not Found)                                  *
 * ------------------------------------------------------------ */

function error_404()
{
    $view["body"]       = "error";
    $view["headerSize"] = "S";

    require "./header.php";
    include "./views/error/404.php";
    require "./footer.php";
}

/* ============================================================ *
 * Helper Function - displayView                                *
 * Creates a new page based on the parameter given (action)     *
 * paired with layout files header.php and footer.php           *
 * ------------------------------------------------------------ */

function displayView($page)
{
    require "./header.php";
    include "./views/pages/$page.php";
    require "./footer.php";

    error_log("View for page: $page is being displayed to the user.", LOG_INFO);
}

/* ============================================================ *
 * Function - displayCons($page)                                *
 * Wraps the Under Construction message (content/body) with the *
 * Header and Footer files.                                     *
 * ------------------------------------------------------------ */

function displayCons($page)
{
    include "./header.php";

    $pageFormatted = ucfirst($page);

    echo "<section class='page-header'>
            <div class='container text-center'>
                <h1>$pageFormatted</h1>
            </div>
            </section>
          </header>
          <main>
            <section class='page-content'>
                <div class='text-center'>
                    <h1>Page Maintenance</h1>
                </div>
            </section>
            <section class='page-content' id='about-content'>
              <div class='container text-center'>
                <p>The $pageFormatted page is currently under going maintenance and has been temporarily taken down. Please come back soon!</p>
                <p>For more information please send us an email at <a href='mailto:info@webfolders.co.za'>info@webfolders.co.za</a></p>
              </div>
            </section>";

    include "./footer.php";
}

/* ============================================================ *
 * Function - Legal                                             *
 * ------------------------------------------------------------ *
 * Generates the front end output and includes the header and   *
 * footer templates. Form handling is also done here.           *
 * ------------------------------------------------------------ */

function legal()
{
    $view["action"]     = ucwords($action);
    $view["title"]      = "legal";
    $view["body"]       = "legal";
    $view["heading"]    = "Legal";
    $view["headerSize"] = "M";
    $view["nav"]        = 1;

    require "header.php";
    require "./views/pages/legal.php";
    require "footer.php";
}

/* ============================================================ *
 * Function - Email                                             *
 * ------------------------------------------------------------ *
 * Generates the front end output and includes the header and   *
 * footer templates. Form handling is also done here.           *
 * ------------------------------------------------------------ */

function email()
{

    /* ====================================== *
     * Configure email                        *
     * -------------------------------------- */
    $from    = 'noreply@blownawaybybooks.co.za';
    $sendTo  = 'info@webfolders.co.za';
    $subject = 'Blown Away By Books Online - Contact Form';

    /* ====================================== *
     * Fields                                 *
     * -------------------------------------- */
    $fields = array("name"    => "Name",
        "surname" => "Surname",
        "phone"   => "Phone",
        "email"   => "Email",
        "message" => "Message");

    /* ====================================== *
     * Status messages                        *
     * -------------------------------------- */
    $okMessage    = "Contact form successfully submitted.";
    $errorMessage = "There was an error while submitting the form. Please try again later";

    error_reporting(E_ALL & ~E_NOTICE);

    /* =============================================== *
     * Attempt to get message contents from POST       *
     * ----------------------------------------------- */
    try
    {
        if (count($_POST) == 0)
        {
            throw new \Exception('Form is empty');
        }

        $emailText = "You have a new message from your contact form\n=============================\n";

        /* ============================================================================ *
         * Loop through POST and output contents of each iteration into the emailText   *
         * ---------------------------------------------------------------------------- */
        foreach ($_POST as $key => $value)
        {
            if (isset($fields[$key]))
            {
                $emailText .= "$fields[$key]: $value\n";
            }
        }

        /* =============================================== *
         * Populate headers of the email                   *
         * ----------------------------------------------- */
        $headers = array('Content-Type: text/plain; charset="UTF-8";',
            'From: ' . $from,
            'Reply-To: ' . $from,
            'Return-Path: ' . $from);

        /* =============================================== *
         * Finally, send the email with the contents       *
         * ----------------------------------------------- */
        mail($sendTo, $subject, $emailText, implode("\n", $headers));
        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
    catch (Exception $e)
    {
        $responseArray = array('type' => 'danger', 'message' => $errorMessage);
    }

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    {
        $encoded = json_encode($responseArray);
        header('Content-Type: application/json');
        //         header('Location: index.php?action=contact');
        echo $encoded;
    }
    else
    {
        echo $responseArray['message'];
    }

    //    header("Location: index.php?action=home");
    //     contact();
}

function error()
{
    $error_type = filter_input(INPUT_GET, "type");

    if ($error_type === "404")
    {
        require "./header.php";
        require "./views/error/404.php";
        require "./footer.php";
    }
}

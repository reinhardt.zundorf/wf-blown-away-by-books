            <?php
            /* =========================================== *
             * Include login modal                         *
             * ------------------------------------------- */
            if ($headerSize != "S")
            {
                ?>

                <!-- FOOTER: Graphic -->
                <div class="footer-pre text-center" >
                    <img src="res/footer/footer-master-5.png" alt="Fish Hoek Library"/>
                </div>

                <!-- FOOTER -->
                <footer class="footer">

                    <div class="container">
                        <div class="footer-content" style="background-color:#a67894;">
                            <div class="row">

                                <!-- FOOTER: Business Card -->
                                <article class="col-lg-4">
                                    <header>
                                        <h5 class="h5">Friends Of The Fish Hoek Library</h5>
                                    </header>
                                    <div itemscope="" itemtype="http://schema.org/Organization">
                                        <address itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                            <span itemprop="streetAddress">We are based at the Fish Hoek Public Library: </span><br/>
                                            <span itemprop="streetAddress">Civic Centre, Central Circle, </span><span itemprop="addressLocality">Fish Hoek</span><br>
                                            <span itemprop="addressRegion">Cape Town</span><br>
                                            <span itemprop="addressCode">7975</span><br>
                                        </address>

                                        <!-- CARD: Contact Information -->
                                        <ul class="footer-details list-unstyled">
                                            <div class="container">
                                                <div class="row">
                                                    <i class="fa fa-1x fa-phone"></i>
                                                    <span itemprop="telephone">(+27) 021 784 2030</span>
                                                </div>
                                            </div>
                                            <li><i class="fa fa-1x fa-envelope"></i></li>
                                            <li><i class="fa fa-1x fa-globe"></i></li>
                                        </ul>

                                        <ul class="footer-details list-unstyled">
                                            <li></li>
                                            <li><span itemprop="email"><a href="mailto:fishhoek.library@capetown.gov.za">info@blownawaybybooks.co.za</a></span></li>
                                            <li><span itemprop="url"><a href="www.blownawaybybooks.co.za" itemprop="url">www.blownawaybybooks.co.za</a></span></li>
                                        </ul>

                                    </div><!-- /.row -->
                                </article>

                                <!-- FOOTER: Sitemap -->
                                <article class="col-lg-2 text-center">
                                    <header>
                                        <h5 class="h5">Sitemap</h5>
                                    </header>
                                    <section class="footer-nav text-left">
                                        <a href="?action=home">Home</a>
                                        <a href="?action=about">About Us</a>
                                        <a href="?action=members">Members</a>
                                        <a href="?action=film">Film</a>
                                        <a href="?action=books">Books</a>
                                        <a href="?action=festival">Festival</a>
                                        <a href="?action=contact">Contact</a>
                                    </section>
                                </article>

                                <!-- FOOTER: Sitemap -->
                                <article class="col-lg-2 text-center">
                                    <header>
                                        <h5 class="h5">Links to read</h5>
                                    </header>
                                    <section class="footer-nav text-left">
                                        <a href="#">Online Public Access Catalogue</a>
                                        <a href="#">City of Cape Town</a>
                                        <a href="#">Fish Hoek Public Library</a>
                                    </section>
                                </article>

                                <!-- FOOTER: Blog -->
                                <!--            <article class="col">
                                                <div>
                                                    <h5>Recommended Links</h5>
                                                    <hr>
                                                </div>
                                                <ul class="footer-nav list-unstyled">
                                                    <li><a href="http://www.swimlabaquaticacademy.co.za/"><img style="height:40px; margin-top:8px;margin-right:10px;" src="res/icons/icon-academy.png" alt=""/>Aquatic Academy</a></li>
                                                    <li><a href="http://www.swimlabswimschool.co.za/"><img style="height:40px; margin-top:8px;margin-right:10px;" src="res/icons/icon-swim-school.png" alt=""/>Swim School</a></li>
                                                    <li><a href="http://www.swimlabswimshop.co.za/"><img style="height:40px; margin-top:8px;margin-right:12px;" src="res/icons/icon-swim-store.png" alt=""/>Swim Shop</a></li>
                                                </ul>
                                            </article>-->

                                <!-- FOOTER: Facebook -->
                                <article class="col-lg-2 ">
                                    <div class="text-center">
                                        <h5>Connect</h5>
                                    </div>
                                    <div class="text-center">
                                        <ul class="footer-nav list-unstyled">
                                            <li><a target="_blank" href="https://www.facebook.com/"><i class="fa fa-3x fa-facebook"></i></a></li>
                                            <li><a target="_blank" href="https://twitter.com/"><i class="fa fa-3x fa-twitter"></i></a></li>
                                            <li><a target="_blank" href="https://www.instagram.com/"><i class="fa fa-3x fa-instagram"></i></a></li>
                                        </ul>
                                    </div> 
                                </article>

                                <!-- FOOTER: Boring Stuff -->
                                <article class="col-lg-3">

                                </article>

                            </div>

                            <div class="container footer-copy">
                                <p class="text-center">Copyright &copy; <?php echo date("Y"); ?> Web Folders (Pty) Ltd.  | All rights reserved | <a href="http://webfolders.co.za/">Webmaster</a></p>
                            </div>
                        </div>
                </footer>
            </main>
                <!--</div>-->


            <?php } ?>


            <!-- JS Libraries -->
            <script async src="js/popper.js"></script>
            <script async src="js/bootstrap.js"></script>
            <script async src="js/bootstrap.bundle.js"></script>

            <?php
            if (isset($view["scripts"]))
            {
                foreach ($view["scripts"] as $script)
                {
                    echo "<script src='js/sections/$script'></script>";
                }
            }

            if ($view["title"] === "contact")
            {
                echo "<script async src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCg5alOdxSu9NnTIEow4z2tyX2SZfrv5QA&callback=myMap'></script>";
            }


            if (isset($view["nav"]) && ($view["nav"] === 1))
            {
                $page_title = $view["title"];
                ?>
                <script>
                    $(document).ready(function ()
                    {
                        $("a .nav-link").removeClass("active");
                        $("#nav-<?php echo $page_title; ?>").addClass("active");
                    });
                </script>
                <?php
            }
            ?>

    </body>
</html>

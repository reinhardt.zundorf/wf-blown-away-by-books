<?php
/* ============================================================ *
 * Web Folders CMS                                              *
 * Index Controller File v1.2                                   *
 * ------------------------------------------------------------ *
 * This file controls the back-end navigation for the main      *
 * navbar on the website. Views are 'pages' in the traditional  *
 * website sense and are the sum of:                            *
 * ------------------------------------------------------------ *
 * 1. Header             (header.php)                           *
 * 2. Page content       (page.php)                             *
 * 3. Footer             (footer.php)                           *
 * ------------------------------------------------------------ *
 * To create a view call displayView($page_name);               *
 * or to display the construction page call displayCons($page)  *
 * ------------------------------------------------------------ */
include "config.php";
include "connection.php";

/* ============================================================ *
 * Check session variables                                      *
 * ------------------------------------------------------------ */
session_start();

$action   = isset($_GET['action'])       ? $_GET['action'] : "";
$username = isset($_SESSION['username']) ? $_SESSION['username'] : "";

if($action != "login" && $action != "logout" && !$username)
{
    login();
    exit;
}

/* ============================================================ *
 * Check action variable and navigate                           *
 * ------------------------------------------------------------ */
if(isset($action))
{
    switch($action)
    {
        /* ============================== *
         * Home                           *
         * ------------------------------ */
        case "home" :
            home();
            break;

        /* ============================== *
         * Login                          *
         * ------------------------------ */
        case "login" :
            login();
            break;

        /* ============================== *
         * Default                        *
         * ------------------------------ */
        default :
            home();
            break;
    }
}
else
{
    home();
}

/* ============================================================ *
 * Helper Function - displayView                                *
 * Creates a new page based on the parameter given (action)     *
 * paired with layout files header.php and footer.php           *
 * ------------------------------------------------------------ */
function displayView($page) 
{
    require "./core/includes/header.php";
    require "./core/pages/$page.php";
    require "./core/includes/footer.php";

    error_log("View for page: $action is being displayed to the user.", LOG_INFO);
}

/* ============================================================ *
 * Helper Function - displayView                                *
 * Creates a new page based on the parameter given (action)     *
 * paired with layout files header.php and footer.php           *
 * ------------------------------------------------------------ */
function home() 
{
    require "./core/includes/header.php";
    require "./core/pages/home.php";
    require "./core/includes/footer.php";

    error_log("View for page: $action is being displayed to the user.", LOG_INFO);
}

/* ============================================================ *
 * Function - login                                             *
 * Attempts to log the user in and starts a new session.        *
 * ------------------------------------------------------------ */
function login()
{
    include "connection.php";

    error_log("CONTROLLER: Start of login function.");

    if(isset($_POST['submit']))
    {
        error_log("CONTROLLER: Post 'submit' successful.");

        $user = mysqli_real_escape_string($mysqli, $_POST['username']);
        $pass = mysqli_real_escape_string($mysqli, $_POST['password']);

        if($user == "" || $pass == "") 
        {
            error_log("LOGIN: Could not complete login process. Either username or password is blank.");
            echo "Either username or password field is empty.";
        } 
        else 
        {
            $result = mysqli_query($mysqli, 
                                  "SELECT * "
                                . "FROM login "
                                . "WHERE username='$user' "
                                . "AND password=md5('$pass')") or die("Could not execute the select query.");
            
            error_log("LOGIN: Username: $user \t\tPassword: $pass");
            error_log("LOGIN: Fetch users from the DB.");

            $row = mysqli_fetch_assoc($result);
            
            if(is_array($row) && !empty($row)) 
            {
                var_dump($row);
                $validuser          = $row['username'];
                $_SESSION['valid']  = $validuser;
                $_SESSION['name']   = $row['name'];
                $_SESSION['id']     = $row['id'];
            } 
            else 
            {
                error_log("LOGIN: Invalid username or password. Username entered is: $user.");
                echo "Invalid username or password.";
                echo "<br/>";
                echo "<a href='login.php'>Go back</a>";
            }

            if(isset($_SESSION['valid'])) 
            {
                error_log("LOGIN: Session valid. Sending header to controller.");
                header('Location: index.php?action=home');          
            }
        }
    } 
    else
    {
        require "core/forms/login.php";
    }
    // require "./core/forms/login.php";
    // require "views/includes/footer";
}

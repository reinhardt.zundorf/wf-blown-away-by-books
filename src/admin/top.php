<!-- DASHBOARD TOP NAVBAR TEMPLATE -->
<div class="navbar navbar-inverse navbar-cpanel navbar-fixed-top" role="navigation">
    <div class="navbar-header">

        <!-- SIDEBAR TOGGLE -->
        <button type="button" id="btnSideBarToggle" class="cp-nav-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="cp-icon-bar"></span>
            <span class="cp-icon-bar"></span>
            <span class="cp-icon-bar"></span>
        </button>

        <!-- NAVBAR BRAND -->
        <a id="lnkHeaderHome" class="navbar-brand navbar-brand-img" target="_top" href="../index.html" uib-tooltip="Home" role="link" aria-label="Home" tooltip-placement="bottom">
            <img id="imgLogo" class="navbar-brand-logo" src="/brand/logo.png?1530299517" alt="MWEB Connect">
        </a>
        
        <!-- <script type="text/ng-template" id="customTemplate.html">
          <a href="../{{ match.model.url }}" ng-attr-target="{{match.model.target}}" ng-bind-html="match.model.name">
          </a>
        </script> -->

        <div class="navbar-preferences form-inline">
            <span class="quickFindDiv">
                <input type="text" id="txtQuickFind" class="form-control hidden-xs input-sm input-quick-find ng-pristine ng-untouched ng-valid" placeholder="Search ( / )" ng-model="quickFindSelected" typeahead-template-url="customTemplate.html" ng-keyup="clearQuickFind($event)" uib-typeahead="app as app.name for app in applicationList | filter:{searchText:$viewValue}" typeahead-input-formatter="formatAppName(quickFindSelected)" typeahead-on-select="openApplication($item, $model, $label)" aria-label="Search" aria-autocomplete="list" aria-expanded="false" aria-owns="typeahead-25-2382">
                <ul class="dropdown-menu ng-isolate-scope ng-hide" ng-show="isOpen() &amp;&amp; !moveInProgress" ng-style="{top: position().top+'px', left: position().left+'px'}" role="listbox" aria-hidden="true" uib-typeahead-popup="" id="typeahead-25-2382" matches="matches" active="activeIdx" select="select(activeIdx, evt)" move-in-progress="moveInProgress" query="query" position="position" assign-is-open="assignIsOpen(isOpen)" debounce="debounceUpdate" template-url="customTemplate.html">
                    <!-- ngRepeat: match in matches track by $index -->
                </ul>
            </span>

            <div class="btn-group dropdown" uib-dropdown="">
                
                <button id="btnUserPref" uib-dropdown-toggle="" class="btn dropdown-toggle user-preferences-btn" uib-tooltip="User Preferences" tooltip-placement="bottom" aria-haspopup="true" aria-expanded="false">
                    <span id="userImg" class="glyphicon glyphicon-user"></span>
                    <span id="lblUserNameTxt" class="hidden-inline-xs">f3894401</span>
                    <span id="caretImg" class="caret"></span>
                </button>
                
                <ul uib-dropdown-menu="" class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a id="lnkUserPrefResetInterface" href="javascript:void(0)" onclick="reset_all_interface_settings('/cpsess7644091877')">
                            Reset Page Settings
                        </a>
                    </li>
                </ul>
            </div>

            <!-- NOTIFICATIONS ICON -->
            <a id="lnkHeaderNotifications" href="../home/notifications.html.tt" uib-tooltip="Notifications (0)" class="btn link-buttons btn-notifications" tooltip-placement="bottom" role="link" aria-label="Notifications (0)">
                <i class="fas fa-lg fa-bell" id="imgNotification"></i>
                <span id="lblHeaderNotificationCountMobile" class="badge badge-danger visible-xs-inline notification-badge ng-hide" ng-show="notificationsExist">!</span>
                <span id="lblHeaderNotificationCount" class="badge badge-danger hidden-xs notification-badge ng-binding ng-hide" ng-show="notificationsExist">0</span>
            </a>

            <!-- LOGOUT -->
            <a id="lnkHeaderLogout" target="_top" href="/logout/?locale=en" class="btn link-buttons" uib-tooltip="Logout" tooltip-placement="bottom" role="link" aria-label="Logout">
                <span id="logoutImg" class="glyphicon glyphicon-log-out"></span>
                <span id="lblLogout" class="hidden-inline-xs">Logout</span>
            </a>
        </div>
    </div>
</div>
<!doctype html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>Login</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<style>
			body
			{
				/*background-color:		#999;*/
			}

			.login-logo,
			.register-logo 
			{
				font-size: 				35px;
				text-align: 			center;
				margin-bottom: 			20px;
				font-weight: 			300;
			}

			.login-logo a,
			.register-logo a 
			{
				color: 					#444;
			}

			.login-page,
			.register-page 
			{
				background: 			linear-gradient(rgb(173, 216, 230), rgb(173, 188, 230), rgb(153, 207, 224), rgb(134, 197, 218));
				background-size: 		cover;
				/*background-repeat:		no-repeat;*/
			}

			.login-box,
			.register-box 
			{
				width: 					400px;
				margin: 				7% auto;
				/*margin-top:				;*/
				/* padding:				45px; */
			}

			@media (max-width: 768px) 
			{
				.login-box,
				.register-box 
				{
					width: 90%;
					margin-top: 20px
				}
			}

			.login-box-body,
			.register-box-body 
			{
				background: rgba(255, 255, 255, 0.8);
				padding: 20px;
				border-top: 0;
				color: #666
			}

			.login-box-body .form-control-feedback,
			.register-box-body .form-control-feedback 
			{
				color: #777
			}

			.login-box-msg,
			.register-box-msg {
				margin: 0;
				text-align: center;
				padding: 0 20px 20px 20px
			}

			.social-auth-links {
				margin: 10px 0
			}
		</style>

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	</head>

	<body class="hold-transition login-page">
		<main class="container">
			<div class="login-box">
				<div class="login-box-body">
					<div class="login-logo text-center">
						<img src="res/wf/brand-logo-sm.png" style="width:140px;" alt="Web Folders" />
					</div>
					<p class="login-box-msg">
						<b>Welcome</b><br>
						Please sign in below to access your Website Dashboard.
					</p>
					<form id="frmLogin" action="index.php?action=login" method="post" name="frmLogin" class="form-horizontal">
						<div class="container">
							<!-- <div class="form-group has-feedback">
								<input  type="email" class="form-control" placeholder="Email" required autofocus maxlength="60">
							</div> -->
							
							<div class="input-group mb-3">
								<div class="input-group-prepend">
							  		<span class="input-group-text" id="basic-addon1"><i class="fa fa-at"></i></span>
							  	</div>
								<input type="email" name="username" class="form-control" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" required maxlength="50">
							</div>

							<div class="input-group mb-3">
							  	<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
							  	</div>
								<input id="password" name="password" type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon2" required maxlength="50">
							</div>
<!-- 							<div class="form-group has-feedback">
							</div>
 -->						<div class="col-xs-8">
								<div class="checkbox icheck">
									<label>
										<input type="checkbox"> Remember Me
									</label>
								</div>
							</div>
							<div class="row" style="margin-bottom:10px;">
								<div class="col-sm">
									<input type="submit" name="submit" value="Login" class="btn btn-success btn-block btn-flat">
								</div>
							</div>
							<!-- <div class="row"> -->
								<!-- <a class="col" href="index.php?action=forgot_password">I can't sign in</a> -->
								<!-- <a class="col" href="index.php?action=activate">Activate profile</a> -->
							<!-- </div> -->
						</div>
					</form>
				</div>
			</div>
		</main>

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" sintegrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	</body>
</html>
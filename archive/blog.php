<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-8">

            <h1 class="my-4">News Blog<br>
                <small>Read news about <i>False Bay Tours</i> and Tourism</small>
            </h1>

            <!-- Blog Post -->
            <div class="card mb-4">
                <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    Posted on January 1, 2017 by
                    <a href="#">Web Folders</a>
                </div>
            </div>

            <!-- Blog Post -->
            <div class="card mb-4">
                <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    Posted on January 1, 2017 by
                    <a href="#">Web Folders</a>
                </div>
            </div>

            <!-- Blog Post -->
            <div class="card mb-4">
                <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    Posted on January 1, 2017 by
                    <a href="#">Web Folders</a>
                </div>
            </div>

            <!-- Pagination -->
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item">
                    <a class="page-link" href="#">&larr; Older</a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#">Newer &rarr;</a>
                </li>
            </ul>

        </div>

        <!-- Sidebar Widgets Column -->
        <!--<div class="col-md-4">-->

            <!-- Search Widget -->


            <!-- CARD -->
            <!--<div class="card my-4">-->

            <!-- 
            <h5 class="card-header">Search</h5>
            <div class="card-body">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Latest Posts</h5>
                <div class="card-body">
                    <ul class="rp">
                        <li class="rp-item">
                            <div class="rp-thumb"><a href=""><img src="http://www.travelstart.co.za/blog/wp-content/uploads/2018/02/image-6.jpg" class="attachment-wp_rp_thumbnail size-wp_rp_thumbnail wp-post-image" alt="zanzibar-island-getaway" width="150" height="113"></a></div>
                            <div class="rp-title"><a href="" rel="bookmark">Best Time To Visit Zanzibar</a></div>
                            <div class="rp-excerpt">The best time to visit Zanzibar is between… </div>
                            <div class="rp-more"><a href="">read more &gt;</a></div>
                        </li>      
                        <li class="rp-item">
                            <div class="rp-thumb"><a href=""><img src="http://www.travelstart.co.za/blog/wp-content/uploads/2018/02/visas.jpg" class="attachment-wp_rp_thumbnail size-wp_rp_thumbnail wp-post-image" alt="types of visa" width="150" height="113"></a></div>
                            <div class="rp-title"><a href="" rel="bookmark">An easy guide to the different types of visas</a></div>
                            <div class="rp-excerpt">Planning a holiday and unsure of which… </div>
                            <div class="rp-more"><a href="">read more &gt;</a></div>		         
                        </li>      
                        <li class="rp-item">
                            <div class="rp-thumb"><a href=""><img src="http://www.travelstart.co.za/blog/wp-content/uploads/2018/01/Discount-Airline-Tickets-Main-image.jpg" class="attachment-wp_rp_thumbnail size-wp_rp_thumbnail wp-post-image" alt="Discount Airline Tickets Main image" width="150" height="113"></a></div>
                            <div class="rp-title"><a href="" rel="bookmark">Get Discounts on Your Next Airline Ticket!</a></div>
                            <div class="rp-excerpt">If you love to travel the world, you’d… </div>
                            <div class="rp-more"><a href="">read more &gt;</a></div>
                        </li>      
                    </ul>
                </div>
            </div>

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Get Our Latest Posts by Email</h5>
                <div class="card-body">
                    <form method="post" action="" onSubmit="window.location.reload()" pageId="7464161" siteId="318579" parentPageId="7464159">
                        <div style="position: relative; overflow: hidden; width:100%;margin-top: 16px;" id="container_COLUMN19">
                            <div class="fieldLabel" style="margin-bottom: 8px;">First Name</div>
                            <input type="text" name="First Name" id="control_COLUMN19" label="First Name" class="textInput defaultText" style="width: 100%">
                        </div>
                        <div style="position: relative; overflow: hidden; width:100%;margin-top: 16px;" id="container_EMAIL">
                            <div class="fieldLabel" style="margin-bottom: 8px;">Email</div>
                            <input type="text" name="Email" id="control_EMAIL" label="Email" class="textInput defaultText" style="width: 100%">
                        </div>
                    </form>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Side Widget</h5>
                <div class="card-body"></div>
            </div>

        </div>
    </div>
<!--</div>-->



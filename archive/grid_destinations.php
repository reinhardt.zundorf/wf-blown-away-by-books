<!--
The MIT License

Copyright 2018 Web Folders (Pty) Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->

<main id="bg-neutral-1">

    <section class="container-fluid" id="bg-neutral-1">
        <div class="page-header" id="bg-neutral-1">
            <div class="text-center">
                <h1><?php echo ucwords($view["title"]); ?></h1>
            </div>
        </div>
    </section>

    <section class="container-fluid">
        <div class="container text-center">
            <p class="text-center">Browse our activities on offer by category. To read about and book activities for your ideal holiday itinerary, please visit our <a href="index.php?action=catalogue">Catalogue</a></p>
        </div>
    </section>

    <section class="container-fluid" id="bg-white">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-6">
                    <img src="http://via.placeholder.com/350x225"/><h1>Kalk Bay</h1>
                </div>
                <div class="col-lg-6">
                    <img src="http://via.placeholder.com/350x225"/><h1>Kalk Bay</h1>
                </div>
            </div>
        </div>
    </section>

</main>

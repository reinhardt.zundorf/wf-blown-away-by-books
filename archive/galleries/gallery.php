<!-- SECTION: About Header -->
<section class="page-header">
    <div class="container text-center">
        <h1>Academy Gallery</h1>
    </div>
</section>


<section class="page-content">
    <div class="container">
        <?php include 'views/pages/galleries/gallery_cards.php?gallery=tyr&year=2017'; ?>
    </div>
</section>



<!-- SECTION: Gallery Header -->
<!-- <section class="page-content" id="gallery-header">
    <div class="container text-center">
        <h1>Year Galleries</h1><br/>
        <p>Please select the gallery you would like to view below: (TIP: you can enlarge images by clicking on them in the gallery)</p>
    </div>
</section>-->

<!-- SECTION: Gallery Preview -->
<!--<section class="gallery-container" >
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">

                    <div class="card-image">
                        <img class="img-fluid" src="res/gallery/cards/card_2017.jpg" alt="General Gallery (2017)"/>
                        <span class="card-title">2017</span>
                    </div>

                    <div class="card-action text-center">
                        <a href="#" target="">View Gallery</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">    
                <div class="card">

                    <div class="card-image">
                        <img class="img-responsive" src="res/gallery/cards/card_2016.jpg" alt="General Gallery (2016)"/>
                        <span class="card-title">2016</span>
                    </div>

                    <div class="card-action text-center">
                        <a href="#" target="new_blank">View Gallery</a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>-->

<!-- SECTION: About Header -->
<!--<section class="page-content" id="gallery-header">
    <div class="container text-center">
        <h1>Events & Galas Galleries</h1>
    </div>
</section>-->

<!-- SECTION: Gallery Preview -->
<!--<section class="gallery-container" >
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card">

                    <div class="card-image">
                        <img class="img-fluid" src="res/gallery/cards/card_2017.jpg" alt="General Gallery (2017)"/>
                        <span class="card-title">Short Course Champs '17</span>
                    </div>

                    <div class="card-action text-center">
                        <a href="#" target="new_blank">View Gallery</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--
<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.7
 */

/**
 * Database `f3894401_babb`
 */

/* `f3894401_babb`.`author` */
$author = array(
  array('id' => '1','name' => 'Andrew','surname' => 'Brown','text' => 'Andrew Brown practises as an advocate in Cape Town, and is a reservist sergeant in the South African Police Service. He has published five novels, the most recent of which, Devil’s Harvest, came out in 2014. Coldsleep Lullaby won the 2006 Sunday Times Literary Prize and Refuge was shortlisted for the 2009 Commonwealth Writer’s Prize for the Africa region. Street Blues, an account of his experiences as a police reservist was shortlisted for the 2009 Alan Paton Award. Good Cop Bad Cop was published in 2016.','profilePic' => 'res/authors/andrew-brown/profile.jpg','dateAdded' => '2018-07-25 20:27:11','dateModified' => '2018-07-25 20:27:11','dateRemoved' => NULL),
  array('id' => '2','name' => 'Diane ','surname' => 'Awerbuck','text' => 'Diane Awerbuck wrote Gardening at Night (2003), which was awarded the Commonwealth Best First Book Award (Africa and the Caribbean) and was shortlisted for the International Dublin IMPAC Award. She also reviews fiction for the South African Sunday Times, and writes for Mail &amp; Guardian’s thought leader. 

Awerbuck’s latest collection of short stories is Cabin Fever; her last novel was Home Remedies. She also writes as Frank Owen (with Alex Latimer), and their latest book is South. They are currently writing the sequel, North. Visit http://southvsnorth.com/.','profilePic' => 'res/authors/diane-awerbuck/profile.jpg','dateAdded' => '2018-07-25 20:28:10','dateModified' => '2018-07-25 20:28:10','dateRemoved' => NULL),
  array('id' => '3','name' => 'Helen','surname' => 'Zille','text' => 'Helen Zille serves as Premier of the Western Cape Province in South Africa. She was also the Leader of the Democratic Alliance until May 2015. Zille previously served as Mayor of Cape Town. She was also the Director of Communications at the University of Cape Town.','profilePic' => 'res/authors/helen-zille/profile.jpg','dateAdded' => '2018-07-25 20:21:21','dateModified' => NULL,'dateRemoved' => NULL),
  array('id' => '4','name' => 'Don','surname' => 'Pinnock','text' => 'Don Pinnock is an associate of Southern Write, a group of top travel and natural history writers and photographers in Africa. He’ s a former editor of Getaway magazine in Cape Town, South Africa He has been an electronic engineer, lecturer in journalism and criminology, consultant to the Mandela government, a professional yachtsman, explorer, travel writer, photographer and a cable-car operator on the Rock of Gibraltar. His present passion is the impact of humans on planetary processes.','profilePic' => 'res/authors/don-pinnock/profile.jpg','dateAdded' => '2018-07-25 20:22:17','dateModified' => NULL,'dateRemoved' => NULL),
  array('id' => '5','name' => 'John','surname' => 'Fredericks','text' => 'I was born in 1945 in Kewtown, Athlone one of the many townships scattered across the Cape Flats of the Western Cape. My Father was a Dustman and my Mother worked in the abattoirs.

I have this distant memory of my Dad coming home from work with comic books, magazines and novels that he had salvaged from other people’s rubbish bins. We were poor and one of my adventures then was to ride upfront with my dad on the horse cart that were used to haul the refuse.

I would scavenge with the other human scavengers on the dump looking for book to read and anything with resale value. I hold a vivid recollection of horse carts, huge piles of rubbish and people enveloped in dust. The squawk of seagulls, mangy dogs and the banter of dustmen over the noise of the wagons.

At night my buddies and I would huddle around a fire brazier for warmth. We would talk about our adventures of the day which always led to me telling a story that I had read.

Three of those buddies later died on the gallows and many others died violent deaths before they reached manhood. In our township there were no heroes. Our idols were old street fighters who settled their scores in bloody bare fisted brawls on the streets of our town. The dynamics were crime and gangsterism and a flickering knife became part of everyday life. We felt privileged when these old gangsters sent us to the Merchant to go buy their marijuana, it meant that we could hang around them and listen to their stories of gangs and prison life.

Many of us aspired to become like them. Some succeeded to rise above these childhood fantasies, for others it became their destiny. Short lived and violent, leaving behind a legacy of heartache and shattered dreams. Our make believe world of gangsters became our rites of passage.

I changed my life around and took my story telling abilities to a higher level. I persevered and honed my craft as a writer and filmmaker, running on the perimeter of the film industry for more than twenty years. I do not have much education but I do have revelation. I have worked with people from all cultures and strived to reach my goal. For many years I was told that I was a good writer but I should not give up my day job. At the age of fifty and tired of doing nothing, I gave up my day job to write full time. To date I have numerous documentaries, film scripts and published stories to my credit. I have written stage and radio plays. I also wrote the screen play of the epic local film Noem My Skollie.

I am currently writing the book of Skollie and this will be my first major book. I dream on because I believe that dreams never die.
“I grow in the shadows, I glow in the dark. I am the storyteller. The weaver of dreams”.','profilePic' => 'res/authors/john-fredericks/profile.jpg','dateAdded' => '2018-07-25 20:25:57','dateModified' => NULL,'dateRemoved' => NULL),
  array('id' => '6','name' => 'Jolyn','surname' => 'Phillips','text' => 'Jolyn Phillips works as a word-vagrant in Cape Town. She sometimes stays in Gansbaai where she was born and sometimes in Bellville and Cape Town with a big backpack with stuff in. Her family calls her a professional student because she has not left university after graduating so many times. They also believe she doesn’t ‘really’ work but mostly they are proud that she is now a millionaire because she published a book called Tjieng Tjang Tjerries and other stories published by Modjaji Books. If she is not scratching around for words she performs as a jazz vocalist, theater performer, educator and poet.','profilePic' => 'res/authors/jolyn-phillips/profile.jpg','dateAdded' => '2018-07-25 20:33:46','dateModified' => NULL,'dateRemoved' => NULL)
);
